#version 330

in vec3 position;
in vec3 color;

uniform mat4 debugModel;
uniform mat4 debugView;
uniform mat4 debugProj;

out vec3 line_color;

void main(){
	line_color = color;
	gl_Position = debugProj * debugView * debugModel * vec4(position,1.0);
}