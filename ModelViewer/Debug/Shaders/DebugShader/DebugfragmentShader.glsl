#version 330

in vec3 line_color;

out vec4 outColor;

void main(){
	outColor = vec4(line_color,1.0);
}