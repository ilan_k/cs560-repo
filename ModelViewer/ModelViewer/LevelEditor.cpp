#include "Precompiled.h"
#include "LevelEditor.h"
#include "WindowsSystem.h"
#include "ModelViewerCore.h"

#include <stddef.h>

bool showGround = true;
bool showPath = true;
bool showModel = true;
bool showSkeleton = false;
bool fillPolygon = true;
bool bindPose = false;
bool pauseAnimation = false;
bool showAxis = true;

float modelSpeed = 20.0f;
bool animationBlending = true;
int restartButton = 0;

glm::vec3 goalOffset;

void TW_CALL SetVec3CallBack(const void *value, void *clientData)
{
	glm::vec3* clientVec3Data = (glm::vec3*) clientData;
	glm::vec3* valueVec3 = (glm::vec3*)value;

	if(*valueVec3 == *clientVec3Data)
	{
		return;
	}

	*clientVec3Data = *valueVec3;
}

void TW_CALL GetVec3CallBack(void *value, void *clientData)
{
	glm::vec3* clientVec3Data = (glm::vec3*) clientData;
	glm::vec3 *ptr = (glm::vec3*)value;
	*ptr = *clientVec3Data;
}

void TW_CALL SetBoolCallBack(const void *value, void *clientData)
{
	bool* clientVec2Data = (bool*) clientData;
	bool* valueVec2 = (bool*)value;

	if(*valueVec2 == *clientVec2Data)
	{
		return;
	}

	*clientVec2Data = *valueVec2;
}

void TW_CALL GetBoolCallBack(void *value, void *clientData)
{
	bool* clientVec2Data = (bool*) clientData;
	bool *ptr = (bool*)value;
	*ptr = *clientVec2Data;
}

void TW_CALL SetFloatCallBack(const void *value, void *clientData)
{
	float* clientFloatData = (float*) clientData;
	float* valueFloat = (float*)value;

	if(*valueFloat == *clientFloatData)
	{
		return;
	}

	*clientFloatData = *valueFloat;
}

void TW_CALL GetFloatCallBack(void *value, void *clientData)
{
	float* clientFloatData = (float*) clientData;
	float* ptr = (float*)value;
	*ptr = *clientFloatData;
}

void TW_CALL ButtonCallBack(void *clientData)
{
	MessageRestartAnimation mra;
	ModelViewerCore::Get()->Broadcast(&mra);
}

LevelEditor *editor;

LevelEditor::LevelEditor()
{
	editor = this;
}

LevelEditor::~LevelEditor()
{
	TwTerminate();
}

void LevelEditor::Initialize()
{
	TwInit(TW_OPENGL_CORE, NULL);

	int width = WindowsSystem::Get()->GetWidth();
	int height = WindowsSystem::Get()->GetHeight();

	TwWindowSize(width, height);

	InitializeBar();
}

void LevelEditor::Refresh() const
{
	TwRefreshBar(bar);
}

void LevelEditor::InitializeBar()
{
	bar = TwNewBar("bar");

	TwAddVarCB(bar, "showGround", TW_TYPE_BOOL32, SetBoolCallBack, GetBoolCallBack, &showGround, "label='Show Ground' key=g");
	TwAddVarCB(bar, "showModel", TW_TYPE_BOOL32, SetBoolCallBack, GetBoolCallBack, &showModel, "label='Show Model' key=m");
	TwAddVarCB(bar, "showSkeleton", TW_TYPE_BOOL32, SetBoolCallBack, GetBoolCallBack, &showSkeleton, "label='Show Skeleton' key=s");
	TwAddVarCB(bar, "fillPolygon", TW_TYPE_BOOL32, SetBoolCallBack, GetBoolCallBack, &fillPolygon, "label='Fill Polygon' key=f");
	TwAddVarCB(bar, "bindPose", TW_TYPE_BOOL32, SetBoolCallBack, GetBoolCallBack, &bindPose, "label='Bind Pose' key=b");
	TwAddVarCB(bar, "pauseAnimation", TW_TYPE_BOOL32, SetBoolCallBack, GetBoolCallBack, &pauseAnimation, "label='Pause Animation' key=p");

	TwAddSeparator(bar, "Separator1", "");

	TwAddVarCB(bar, "showPath", TW_TYPE_BOOL32, SetBoolCallBack, GetBoolCallBack, &showPath, "label='Show Path' key=a");
	TwAddButton(bar, "restart", ButtonCallBack, &restartButton, "label='Restart' key=r");
	TwAddVarCB(bar, "modelSpeed", TW_TYPE_FLOAT, SetFloatCallBack, GetFloatCallBack, &modelSpeed, "label='Model Speed' keyincr=. keydecr=, step=0.5 min=1.0 max=60.0");
	TwAddVarCB(bar, "animationBlending", TW_TYPE_BOOL32, SetBoolCallBack, GetBoolCallBack, &goalOffset, "label='Animation Blending' key=l");

	TwAddSeparator(bar, "Separator2", "");


    TwStructMember pointMembers[] = { 
        { "X", TW_TYPE_FLOAT, offsetof(glm::vec3, x), " Min=-5 Max=5 Step=0.05 " },
        { "Y", TW_TYPE_FLOAT, offsetof(glm::vec3, y), " Min=-5 Max=5 Step=0.05 " }, 
		{ "Z", TW_TYPE_FLOAT, offsetof(glm::vec3, z), " Min=-5 Max=5 Step=0.05 " }
	};

	TwType pointType = TwDefineStruct("POINT", pointMembers, 3, sizeof(glm::vec3), NULL, NULL);

	TwAddVarCB(bar, "goalOffset", pointType, SetVec3CallBack, GetVec3CallBack, &goalOffset, "label='Goal Offset'");
}

void LevelEditor::Draw()
{
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	TwDraw();
}

LevelEditor const* LevelEditor::Get()
{
	return editor;
}