﻿/* Start Header -------------------------------------------------------
Copyright (C) 2014 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written consent of DigiPen Institute of Technology is prohibited.
File Name:		GLSLshader class for creating / binding / using shaders
Purpose:		Gives some functionality of File / Folders
Language:		C++ and compiled in VS2012
Platform:		Visual Studio 2012, Windows 8
Project:		CS560
Author:			Ilan Keshet
Creation date:	16/9/2014
- End Header --------------------------------------------------------*/

#include "Precompiled.h"
#include "GLSLshader.h"

#define CHECKERROR {int err = glGetError(); if (err) { fprintf(stderr, "OpenGL error %d (at line %d): %s\n", err, __LINE__, glewGetErrorString(err)); } }


GLSLshader::GLSLshader(void)
{
}

GLSLshader::~GLSLshader(void)
{
	glDeleteProgram(shaderProgram);
}

void GLSLshader::createShaderProgram(const string& vertSrc,const string&  fragSrc,const string&  geoSrc)
{
	string vertexStr(readShaderFile(vertSrc));
	string fragStr(readShaderFile(fragSrc));
	string geoStr(readShaderFile(geoSrc));

	const GLchar* vertexShader   = vertexStr.c_str();
	const GLchar* fragmentShader = fragStr.c_str();
	const GLchar* geometryShader = geoStr.c_str();

	/*            This line of Code doesn't Work! Why?
	const GLchar* vertexShader   = readShaderFile(vertSrc).c_str();
	*/

	
	shaders[0] = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(shaders[0],1,&vertexShader,NULL);
	glCompileShader(shaders[0]);

	CHECKERROR;

	glGetShaderiv(shaders[0],GL_COMPILE_STATUS,&shaderStatus);

	CHECKERROR;

	if(shaderStatus != GL_TRUE)
	{
		printf("Vertex Shader Compile Failed\n");
	}

	//Compiling Fragment Shader
	shaders[1] = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(shaders[1],1,&fragmentShader,NULL);
	glCompileShader(shaders[1]);

	CHECKERROR;

	glGetShaderiv(shaders[1],GL_COMPILE_STATUS,&shaderStatus);

	CHECKERROR;

	if(shaderStatus != GL_TRUE)
	{
		printf("Fragment Shader Compile Failed\n");
	}

	//Compiling Geometry Shader
	shaders[2] = glCreateShader(GL_GEOMETRY_SHADER);
	glShaderSource(shaders[2],1,&geometryShader,NULL);
	//glShaderSource(shaders[2],1,&readShaderFile(geoSrc).c_str(),NULL);
	glCompileShader(shaders[2]);

	CHECKERROR;

	glGetShaderiv(shaders[2],GL_COMPILE_STATUS,&shaderStatus);

	CHECKERROR;

	if(shaderStatus != GL_TRUE)
	{
		printf("Geometry Shader Compile Failed\n");
	}

	//Binding each different state of shader
	shaderProgram = glCreateProgram();
	glAttachShader(shaderProgram,shaders[0]);
	glAttachShader(shaderProgram,shaders[1]);
	glAttachShader(shaderProgram,shaders[2]);
	
	CHECKERROR;

	glBindFragDataLocation(shaderProgram,0,"outColor");
	glLinkProgram(shaderProgram);

	CHECKERROR;

	glDeleteShader(shaders[0]);
	glDeleteShader(shaders[1]);
	glDeleteShader(shaders[2]);

	CHECKERROR;
}

void GLSLshader::ShaderCompileErrorLog(GLuint shader)
{
	int length;
	glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &length);
	char* buffer = new char[length];
	glGetShaderInfoLog(shader, length, NULL, buffer);
	printf("Compile log %s\n", buffer);
	delete buffer;
}

void GLSLshader::createShaderProgram(const string& vertSrc,const string&  fragSrc)
{
	string vertexStr(readShaderFile(vertSrc));
	string fragStr(readShaderFile(fragSrc));

	const GLchar* vertexShader   = vertexStr.c_str();
	const GLchar* fragmentShader = fragStr.c_str();

	/*            This line of Code doesn't Work! Why?
	const GLchar* vertexShader   = readShaderFile(vertSrc).c_str();
	*/

	
	shaders[0] = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(shaders[0],1,&vertexShader,NULL);
	glCompileShader(shaders[0]);

	glGetShaderiv(shaders[0],GL_COMPILE_STATUS,&shaderStatus);

	if(shaderStatus != GL_TRUE)
	{
		cout << "Vertex Shader Compile Failed\n";
		ShaderCompileErrorLog(shaders[0]);
	}

	//Compiling Fragment Shader
	shaders[1] = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(shaders[1],1,&fragmentShader,NULL);
	glCompileShader(shaders[1]);

	glGetShaderiv(shaders[1],GL_COMPILE_STATUS,&shaderStatus);

	if(shaderStatus != GL_TRUE)
	{
		cout << "Fragment Shader Compile Failed\n";
		ShaderCompileErrorLog(shaders[1]);
	}

	//Binding each different state of shader
	shaderProgram = glCreateProgram();
	glAttachShader(shaderProgram,shaders[0]);
	glAttachShader(shaderProgram,shaders[1]);
	
	glBindFragDataLocation(shaderProgram,0,"outColor");
	glLinkProgram(shaderProgram);


	glDeleteShader(shaders[0]);
	glDeleteShader(shaders[1]);
}

void GLSLshader::addAttribute(const string& Attribute)
{
	attributeMap[Attribute] = glGetAttribLocation(shaderProgram, Attribute.c_str());
}

void GLSLshader::addUniform(const string& Uniform)
{
	uniformMap[Uniform] = glGetUniformLocation(shaderProgram,Uniform.c_str()); 
}

void GLSLshader::BindFragData(const string& Frag)
{

}

void GLSLshader::Bind()
{
	glUseProgram(shaderProgram);
}

void GLSLshader::unBind()
{
	glUseProgram(NULL);
}

GLuint GLSLshader::operator[](const string& Attribute) const 
{
	return attributeMap.find(Attribute)->second;
}

GLuint GLSLshader::operator()(const string& Uniform) const
{
	return uniformMap.find(Uniform)->second;
}

GLuint GLSLshader::getShaderProgram()
{
	return shaderProgram;
}

string GLSLshader::readShaderFile(const string& fileName)
{
	string filetext;
	ifstream stream;
	stream.open(fileName);


	if(stream.fail()) 
	{
		cerr << "Failed to load file:" << fileName << endl;
		return "";
	}

	while( stream.good() )
	{
		string line;
		getline( stream, line );
		filetext.append(line + "\n");
	}

	stream.close();
	return filetext;
}