/* Start Header -------------------------------------------------------
Copyright (C) 2014 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written consent of DigiPen Institute of Technology is prohibited.
File Name:		Skeleton.h
Purpose:		Class for so
Language:		C++ and compiled in VS2012
Platform:		Visual Studio 2012, Windows 8
Project:		CS560
Author:			Ilan Keshet
Creation date:	16/9/2014
- End Header --------------------------------------------------------*/

#pragma once

#include "BinAsset.h"

#include "Math.h"

class Animation;
struct JointPose;
struct IKJointPose;

struct Joint
{
	string name;

	int index;
	int parentIndex;

	//Bind Pose Position / Rotation of bone
	glm::vec3 BindPosition;

	//glm::mat4 BindTransform;

	//Transform from Model to BoneSpace
	glm::mat4 invBindTransform;


	//The following are used for debugging purposes
	//All that is actually needed is the invBindTransform
	//which is created from below
	//Used for Model To Bone Space Translation
	//glm::vec3 invBindPosition;

	//Used for Model To Bone Space Rotation
	//glm::quat invBindRotation;

	//glm::vec3 invBindScale;

	vector<Joint*> children;
};

class LineDrawer;

class Skeleton : public BinAsset
{
public:
	//Reading the skeleton from a binary file in either Chris Peters format or mine
	void Read(BinFileDeseralizer &deser, bool oldFileVersion);

	//Initialization of the Skeleton -- to link up all parents with children
	void Initialize();
	void DrawBindPose(LineDrawer *lDrawer) const;

	void Draw(LineDrawer *lDrawer, vector<glm::mat4> const &jointMatrixBuffer) const;

	//Setting the joint Matrix buffer based on the currentTime and the current and next Animation poses
	//the joint matrix buffer should be called prior to the draw call
	void ComputeJointMatrixBuffer(Animation &animation, vector<glm::mat4> &jointMatrixbuffer, float currentTime);

	void ComputeBlendJointMatrixBuffer(Animation &anim1, Animation &anim2, float b, vector<glm::mat4> &jointMatrixbuffer, float currentTime);
	
	void ComputeIkJointMatrixBuffer(Animation &animation, vector<glm::mat4> &jointMatrixbuffer, float currentTime, 
			float k,std::map<int, JointPose> const & ikJointPoseMap);

	void InterpolateKeyFrame(Animation& anim, int boneId, float currentTime, Quaternion &interpolatedQuat, glm::vec3 &interpolatedTrans, glm::vec3 &interpolatedScale);

	unsigned int GetNumberOfJoints() const { return numberOfJoints; }

	vector<Joint> const& GetJoints() const { return this->joints; }

	bool GetJointChain(std::string const &handle, std::string const &root, vector<Joint*> &jointsOut);

	JointPose GetJointPose(int joint, Animation &animation, float currentTime);

private:
	unsigned int numberOfJoints;
	vector<Joint> joints;
	vector<Joint*> rootJoints;
};