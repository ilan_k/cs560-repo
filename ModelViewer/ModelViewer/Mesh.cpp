#include "Precompiled.h"
#include "Mesh.h"
#include "Model.h"

/******************************************************************************/
/*!
	Reads the Mesh from the binary file
*/
/******************************************************************************/
void Mesh::Read(BinFileDeseralizer& file, bool oldFileVersion)
{
	file.Read(vertexType);

	//Parse indices
	file.Read(numIndices);
	indices = new unsigned int[numIndices];
	file.ReadBytes(indices, sizeof(unsigned int) * numIndices);

	// Parse vertices
	file.Read(numVertices);

	int size = sizeof(StaticModelVertex);

	int offset = 0;

	if(vertexType == Skinned)
	{
		size = sizeof(SkinnedModelVertex);

		vertexBufferData = new SkinnedModelVertex[numVertices * size];
	}
	else
	{
		vertexBufferData = new StaticModelVertex[numVertices * size];
	}

	if(vertexType == Static)
	{
		file.ReadVertices((StaticModelVertex*)vertexBufferData, numVertices);

		file.ComputeTangents((StaticModelVertex*)vertexBufferData, numVertices, (unsigned int*)indices, numIndices);
	}
	else
	{
		file.ReadVertices((SkinnedModelVertex*)vertexBufferData, numVertices);

		file.ComputeTangents((SkinnedModelVertex*)vertexBufferData, numVertices, (unsigned int*)indices, numIndices);
	}

	numTriangles = numIndices / 3;
}