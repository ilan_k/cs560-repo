/* Start Header ------------------------------------------------------- 
Copyright (C) 2013 DigiPen Institute of Technology. 
Reproduction or disclosure of this file or its contents without the prior 
written consent of DigiPen Institute of Technology is prohibited. 
 
File Name: GEInputHandler.h
Purpose: Class that handles polling events and calling callback functions
		If a certain key was pressed
Language: Microsoft Visual C++, Compiler: MSV 2010
Platform: Has been tested on Windows 7 in DigiPen Lab Computers
Project: CS529_ilan.k_Game_Engine
Author: Ilan Keshet, ilan.k, 60001813
Creation date: December 13th 2013
- End Header --------------------------------------------------------*/ 

#pragma once

#include "System.h"

class InputSystem;

extern InputSystem* INPUTHANDLER;

enum{
	IS_UPARROW = 0,
	IS_DOWNARROW,
	IS_LEFTARROW,
	IS_RIGHTARROW,
	IS_DELETE,
	IS_ESC,
	IS_TAB
};

enum KeyEventDescription{
	KEYDOWN = 0,   //Key is currently down
	KEYUP,         //Key just got released
	KEYTRIGGERED   //Key just got pressed
};

class InputSystem : public System{
///System is a pure virtual base class (which is to say, an interface) that is
///the base class for all systems used by the game. 
public:
	void SendMsg(Message* message) {}

	void Update(float timeslice);

	string GetName(){ return "Input Handler"; }

	bool inputReceived(int key, KeyEventDescription desc);

	//Returns back a unique ID, which can be used to later unbind a key
	//Returns -1 if could not successfully bind key, and also prints error message
	int bindKey(int key, void (*callBack)(), KeyEventDescription desc);

	//Returns 0 on success on removing key-
	//Returns -1 if invalid key is given
	//Returns -2 if callBackID could not be found with given key
	//int unbindKey(int key, int callBackID, KeyEventDescription desc);

	//Unbinds all callBacks with a given key
	void unbindKey(int key);

	//Unbinds a callback with given callBack ID
	//Returns 0 on success, -1 if could not find CallBack ID
	int unbindCallBack(int callBackID);

	//Unbinds all callbacks
	void unbindAll();

	void Initialize();
		
	~InputSystem(){}		
private:
	int callBackID;

	typedef unordered_map<int,void(*)()> CallBackMap;

	typedef struct GEKeyEvent{
		bool down;
		CallBackMap downCallBack;

		bool triggered;
		CallBackMap triggeredCallBack;

		bool up;
		CallBackMap upCallBack;
	};

	void InitializeKey(GEKeyEvent &keyEvent);

	unordered_map<int, pair<CallBackMap*, CallBackMap::iterator> > globalCallBackMap;

	void keyPressed(GEKeyEvent &key);
	void keyReleased(GEKeyEvent &key);

	void keyPressedCallBack(GEKeyEvent &key);
	void keyReleasedCallBack(GEKeyEvent &key);

	void resetKeyEvent(GEKeyEvent &keyEvent);
	void beginFrame();

	void unbindCallBackMap(CallBackMap* callBackMapPtr);
	void unbindKey(GEKeyEvent &keyEvent);

	void pollRegisteredCallBacks();
	void pollRegisteredCallBacks(GEKeyEvent &keyEvent);

	bool getBool(GEKeyEvent &keyEvent, KeyEventDescription desc);
	CallBackMap* getCallBack(GEKeyEvent &keyEvent, KeyEventDescription desc);

	//unordered_map<int, pair<unordered_map<int,void(*)()>*, unordered_map<int,void(*)()>::iterator> >;

	#define SPACE_ASCII_CODE 32
	#define AT_SYMBOL_ASCII_CODE 64

	#define LEFTBRACKET_ASCII_CODE 91
	#define Z_ASCII_CODE 122

	//Events for keys '@' to ' ' being pressed
	GEKeyEvent keyEventSet1[AT_SYMBOL_ASCII_CODE - SPACE_ASCII_CODE + 1];

	//Events for keys 'z' to '['
	GEKeyEvent keyEventSet2[Z_ASCII_CODE - LEFTBRACKET_ASCII_CODE + 1];

	//Escape Key Event
	GEKeyEvent escapeEvent;

	//Delete key pressed (backspace>
	GEKeyEvent deleteEvent;
	
	//Tab key pressed
	GEKeyEvent tabEvent;

	//Up,Down,Left,Right arrow keys pressed
	GEKeyEvent upArrowEvent;
	GEKeyEvent downArrowEvent;
	GEKeyEvent leftArrowEvent;
	GEKeyEvent rightArrowEvent;
};
