/* Start Header ------------------------------------------------------- 
Copyright (C) 2013 DigiPen Institute of Technology. 
Reproduction or disclosure of this file or its contents without the prior 
written consent of DigiPen Institute of Technology is prohibited. 
 
File Name: GEInputHandler.cpp
Purpose: Class that handles polling events and calling callback functions
		If a certain key was pressed
Language: Microsoft Visual C++, Compiler: MSV 2010
Platform: Has been tested on Windows 7 in DigiPen Lab Computers
Project: CS529_ilan.k_Game_Engine
Author: Ilan Keshet, ilan.k, 60001813
Creation date: December 13th 2013
- End Header --------------------------------------------------------*/ 

#include "Precompiled.h"
#include "InputSystem.h"
#include "ModelViewerCore.h"
#include "Message.h"
#include "LevelEditor.h"

InputSystem* INPUTHANDLER;

void InputSystem::InitializeKey(GEKeyEvent &keyEvent){
	keyEvent.down = false;
	keyEvent.triggered = false;
	keyEvent.up = false;
}

void InputSystem::Initialize(){
	callBackID = 0;

	INPUTHANDLER = this;

	InitializeKey(deleteEvent);
	InitializeKey(escapeEvent);
	InitializeKey(tabEvent);

	for(unsigned int i = SPACE_ASCII_CODE; i <= AT_SYMBOL_ASCII_CODE; i++){
		InitializeKey(keyEventSet1[AT_SYMBOL_ASCII_CODE - i]);
	}

	for(unsigned int i = LEFTBRACKET_ASCII_CODE; i <= Z_ASCII_CODE; i++){
		InitializeKey(keyEventSet2[Z_ASCII_CODE - i]);
	}

	InitializeKey(upArrowEvent);
	InitializeKey(downArrowEvent);
	InitializeKey(leftArrowEvent);
	InitializeKey(rightArrowEvent);
}


bool InputSystem::getBool(GEKeyEvent &keyEvent, KeyEventDescription desc){
	if(desc == KEYDOWN){
		return keyEvent.down;
	}
	else if(desc == KEYUP){
		return keyEvent.up;
	}
	else if(desc == KEYTRIGGERED){
		return keyEvent.triggered;
	}
	else{
		return 0;
	}
}

bool InputSystem::inputReceived(int key, KeyEventDescription desc){
	key = tolower(key);

	if(key >= SPACE_ASCII_CODE && key <= AT_SYMBOL_ASCII_CODE){
		return getBool(keyEventSet1[AT_SYMBOL_ASCII_CODE - key], desc);
	}
	else if(key >= LEFTBRACKET_ASCII_CODE && key <= Z_ASCII_CODE){
		return getBool(keyEventSet2[Z_ASCII_CODE - key], desc);
	}
	else if(key == IS_UPARROW){
		return getBool(upArrowEvent, desc);
	}
	else if(key == IS_DOWNARROW){
		return getBool(downArrowEvent, desc);
	}
	else if(key == IS_LEFTARROW){
		return getBool(leftArrowEvent, desc);
	}
	else if(key == IS_RIGHTARROW){
		return getBool(rightArrowEvent, desc);
	}
	else if(key == IS_DELETE){
		return getBool(deleteEvent, desc);
	}
	else if(key == IS_ESC){
		return getBool(escapeEvent, desc);
	}
	else if(key == IS_TAB){
		return getBool(tabEvent, desc);
	}
	else{
		cout << "Invalid key: " << key << endl;
		return false;
	}		
}

unordered_map<int,void(*)()>* InputSystem::getCallBack(GEKeyEvent &keyEvent, KeyEventDescription desc){
	if(desc == KEYDOWN){
		return &(keyEvent.downCallBack);
	}
	else if(desc == KEYUP){
		return &(keyEvent.upCallBack);
	}
	else if(desc == KEYTRIGGERED){
		return &(keyEvent.triggeredCallBack);
	}
	else{
		return 0;
	}
}

int InputSystem::unbindCallBack(int callBackID){
	auto it = globalCallBackMap.find(callBackID);

	if(it != globalCallBackMap.end()){

		//Erase the CallBack function in the CallBack Map
		it->second.first->erase(it->second.second);

		//Erase the record of it in the global call back map
		globalCallBackMap.erase(it);

		return 0;
	}
	else{
		cout << "Error: CallbackID: " << callBackID << " could not be found " << endl;
		return -1;
	}
}

void InputSystem::unbindCallBackMap(CallBackMap* callBackMapPtr){
	while(callBackMapPtr->size()){
		unbindCallBack(callBackMapPtr->begin()->first);
	}
}

void InputSystem::unbindKey(GEKeyEvent &keyEvent){
	//keyEvent.downCallBack.clear();
	//keyEvent.triggeredCallBack.clear();
	//keyEvent.upCallBack.clear();

	unbindCallBackMap(&keyEvent.downCallBack);
	unbindCallBackMap(&keyEvent.triggeredCallBack);
	unbindCallBackMap(&keyEvent.upCallBack);
}

void InputSystem::unbindAll(){
	for(unsigned int i = SPACE_ASCII_CODE; i <= AT_SYMBOL_ASCII_CODE; i++){
		unbindKey(keyEventSet1[AT_SYMBOL_ASCII_CODE - i]);
	}

	for(unsigned int i = LEFTBRACKET_ASCII_CODE; i <= Z_ASCII_CODE; i++){
		unbindKey(keyEventSet2[Z_ASCII_CODE - i]);
	}

	unbindKey(upArrowEvent);
	unbindKey(downArrowEvent);
	unbindKey(leftArrowEvent);
	unbindKey(rightArrowEvent);
	unbindKey(deleteEvent);
	unbindKey(escapeEvent);
	unbindKey(tabEvent);
		
	//Ensure that all records in global call back map have been removed
	globalCallBackMap.clear();
}

//Unbinds all callBacks with a given key
void InputSystem::unbindKey(int key){
	key = tolower(key);

	if(key >= SPACE_ASCII_CODE && key <= AT_SYMBOL_ASCII_CODE){
		unbindKey(keyEventSet1[AT_SYMBOL_ASCII_CODE - key]);
	}
	else if(key >= LEFTBRACKET_ASCII_CODE && key <= Z_ASCII_CODE){
		unbindKey(keyEventSet2[Z_ASCII_CODE - key]);
	}
	else if(key == IS_UPARROW){
		unbindKey(upArrowEvent);
	}
	else if(key == IS_DOWNARROW){
		unbindKey(downArrowEvent);
	}
	else if(key == IS_LEFTARROW){
		unbindKey(leftArrowEvent);
	}
	else if(key == IS_RIGHTARROW){
		unbindKey(rightArrowEvent);
	}
	else if(key == IS_DELETE){
		unbindKey(deleteEvent);
	}
	else if(key == IS_ESC){
		unbindKey(escapeEvent);
	}
	else if(key == IS_TAB){
		unbindKey(tabEvent);
	}
	else{
		cout << "Error key: " << key << " could not be unbinded" << endl;
	}
}

int InputSystem::bindKey(int key, void (*callBack)(), KeyEventDescription desc){
	key = tolower(key);
	CallBackMap *callBackMapPtr;

	pair<CallBackMap::iterator, bool> insertRet;

	if(key >= SPACE_ASCII_CODE && key <= AT_SYMBOL_ASCII_CODE){
		insertRet = (callBackMapPtr = getCallBack(keyEventSet1[AT_SYMBOL_ASCII_CODE - key], desc))->insert(make_pair(callBackID, callBack));
	}
	else if(key >= LEFTBRACKET_ASCII_CODE && key <= Z_ASCII_CODE){
		insertRet = (callBackMapPtr = getCallBack(keyEventSet2[Z_ASCII_CODE - key], desc))->insert(make_pair(callBackID, callBack));
	}
	else if(key == IS_UPARROW){
		insertRet = (callBackMapPtr = getCallBack(upArrowEvent, desc))->insert(make_pair(callBackID, callBack));
	}
	else if(key == IS_DOWNARROW){
		insertRet = (callBackMapPtr = getCallBack(downArrowEvent, desc))->insert(make_pair(callBackID, callBack));
	}
	else if(key == IS_LEFTARROW){
		insertRet = (callBackMapPtr = getCallBack(leftArrowEvent, desc))->insert(make_pair(callBackID, callBack));
	}
	else if(key == IS_RIGHTARROW){
		insertRet = (callBackMapPtr = getCallBack(rightArrowEvent, desc))->insert(make_pair(callBackID, callBack));
	}
	else if(key == IS_DELETE){
		insertRet = (callBackMapPtr = getCallBack(deleteEvent, desc))->insert(make_pair(callBackID, callBack));
	}
	else if(key == IS_ESC){
		insertRet = (callBackMapPtr = getCallBack(escapeEvent, desc))->insert(make_pair(callBackID, callBack));
	}
	else if(key == IS_TAB){
		insertRet = (callBackMapPtr = getCallBack(tabEvent, desc))->insert(make_pair(callBackID, callBack));
	}
	else{
		cout << "Error key: " << key << " could not be binded" << endl;
		return -1;
	}

	//insertion into the key's callback map was successful
	if(insertRet.second){
		globalCallBackMap.insert(make_pair(callBackID, make_pair(callBackMapPtr, insertRet.first)));

		int returnID = callBackID;

		callBackID++;

		return returnID;
	}
	else{
		cout << "Error key: " << key << " could not be inserted into the key's callBack with callBackID: " << callBackID << endl;
		return -1;
	}
}

void InputSystem::keyPressedCallBack(GEKeyEvent &key){
	if(key.down){
		for(auto iter = key.downCallBack.begin(); iter != key.downCallBack.end(); ++iter){
			(*(iter->second))();
		}

		//If the event was not repeated, set triggered to true, and call back triggered functions
		if(key.triggered){
			for(auto iter = key.triggeredCallBack.begin(); iter != key.triggeredCallBack.end(); ++iter){
				(*(iter->second))();
			}
		}
	}
}

void InputSystem::keyReleasedCallBack(GEKeyEvent &key){
	if(key.up){
		for(auto iter = key.upCallBack.begin(); iter != key.upCallBack.end(); ++iter){
			(*(iter->second))();
		}
	}
}

void InputSystem::keyPressed(GEKeyEvent &key){
	key.triggered = true;
	key.down = true;
}

void InputSystem::keyReleased(GEKeyEvent &key){
	key.up = true;
	key.down = false;
	key.triggered = false;
}

void InputSystem::resetKeyEvent(GEKeyEvent &keyEvent){
		
	//Key can only be triggered once if it was set last frame
	keyEvent.triggered = false;

	//Key can only be up once if it was set last frame
	keyEvent.up = false;
}

void InputSystem::beginFrame(){
	resetKeyEvent(escapeEvent);
	resetKeyEvent(deleteEvent);
	resetKeyEvent(tabEvent);

	for(unsigned int i = 0; i < AT_SYMBOL_ASCII_CODE - SPACE_ASCII_CODE + 1; i++){
		resetKeyEvent(keyEventSet1[i]);
	}

	for(unsigned int i = 0; i < Z_ASCII_CODE - LEFTBRACKET_ASCII_CODE + 1; i++){
		resetKeyEvent(keyEventSet2[i]);
	}

	resetKeyEvent(upArrowEvent);
	resetKeyEvent(downArrowEvent);
	resetKeyEvent(rightArrowEvent);
	resetKeyEvent(leftArrowEvent);
}

void InputSystem::pollRegisteredCallBacks(GEKeyEvent &keyEvent){
	keyReleasedCallBack(keyEvent);

	keyPressedCallBack(keyEvent);
}

void InputSystem::pollRegisteredCallBacks(){
	pollRegisteredCallBacks(escapeEvent);
	pollRegisteredCallBacks(deleteEvent);
	pollRegisteredCallBacks(tabEvent);

	for(unsigned int i = 0; i < AT_SYMBOL_ASCII_CODE - SPACE_ASCII_CODE + 1; i++){
		pollRegisteredCallBacks(keyEventSet1[i]);
	}

	for(unsigned int i = 0; i < Z_ASCII_CODE - LEFTBRACKET_ASCII_CODE + 1; i++){
		pollRegisteredCallBacks(keyEventSet2[i]);
	}

	pollRegisteredCallBacks(upArrowEvent);
	pollRegisteredCallBacks(downArrowEvent);
	pollRegisteredCallBacks(rightArrowEvent);
	pollRegisteredCallBacks(leftArrowEvent);
}

void InputSystem::Update(float timeslice){
	SDL_Event e;

	beginFrame();

	while(SDL_PollEvent(&e)){

		int handled = TwEventSDL(&e, SDL_MAJOR_VERSION, SDL_MINOR_VERSION);
		//int handled = 0;

		if(!handled)
		{

			if(e.type == SDL_QUIT){
				MessageQuit quit;
				ModelViewerCore::Get()->Broadcast(&quit);
				return;
			}

			switch(e.type){
			case SDL_KEYUP:
				switch(e.key.keysym.sym){
				case SDLK_UP:
					keyReleased(upArrowEvent);
					break;
				case SDLK_DOWN:
					keyReleased(downArrowEvent);
					break;
				case SDLK_LEFT:
					keyReleased(leftArrowEvent);
					break;
				case SDLK_RIGHT:
					keyReleased(rightArrowEvent);
					break;
				case SDLK_ESCAPE:
					keyReleased(escapeEvent);
					break;
				case SDLK_DELETE:
					keyReleased(deleteEvent);
					break;
				case SDLK_TAB:
					keyReleased(tabEvent);
					break;
				default:
					int sym = e.key.keysym.sym;

					if(sym >= SPACE_ASCII_CODE && sym <= AT_SYMBOL_ASCII_CODE){
						keyReleased(keyEventSet1[AT_SYMBOL_ASCII_CODE - sym]);
					}
					else if(sym >= LEFTBRACKET_ASCII_CODE && sym <= Z_ASCII_CODE){
						keyReleased(keyEventSet2[Z_ASCII_CODE - sym]);
					}
				}
				break;
			case SDL_KEYDOWN:
				//printDebugMessage("Key is down");
				switch(e.key.keysym.sym){
				case SDLK_UP:
					if(!upArrowEvent.down) keyPressed(upArrowEvent);
					break;
				case SDLK_DOWN:
					if(!downArrowEvent.down) keyPressed(downArrowEvent);
					break;
				case SDLK_LEFT:
					if(!leftArrowEvent.down) keyPressed(leftArrowEvent);
					break;
				case SDLK_RIGHT:
					if(!rightArrowEvent.down) keyPressed(rightArrowEvent);
					break;
				case SDLK_ESCAPE:
					if(!escapeEvent.down) keyPressed(escapeEvent);
					break;
				case SDLK_DELETE:
					if(!deleteEvent.down) keyPressed(deleteEvent);
					break;
				case SDLK_TAB:
					if(!tabEvent.down){
						keyPressed(tabEvent);

						MessageTabPressed mtp;
						ModelViewerCore::Get()->Broadcast(&mtp);
					}
				default:
					int sym = e.key.keysym.sym;

					if(sym >= SPACE_ASCII_CODE && sym <= AT_SYMBOL_ASCII_CODE){
						if(!keyEventSet1[AT_SYMBOL_ASCII_CODE - sym].down) keyPressed(keyEventSet1[AT_SYMBOL_ASCII_CODE - sym]);
					}
					else if(sym >= LEFTBRACKET_ASCII_CODE && sym <= Z_ASCII_CODE){
						if(!keyEventSet2[Z_ASCII_CODE - sym].down) keyPressed(keyEventSet2[Z_ASCII_CODE - sym]);
					}
				}
				break;
			case SDL_WINDOWEVENT:
				if(e.window.event == SDL_WINDOWEVENT_RESIZED){
					MessageResize mr;

					ModelViewerCore::Get()->Broadcast(&mr);
				}
				break;
			default:
				if(e.type == SDL_DROPFILE)
				{
					MessageDropFile mdf(e.drop.file);
					ModelViewerCore::Get()->Broadcast(&mdf);
				}
				else if(e.type == SDL_MOUSEMOTION)
				{
					MessageMouseMotion mmm(e.motion.x, e.motion.y);
					ModelViewerCore::Get()->Broadcast(&mmm);
				}
				else if(e.type == SDL_MOUSEWHEEL)
				{
					MessageMouseWheel mmw(e.wheel.x, e.wheel.y);
					ModelViewerCore::Get()->Broadcast(&mmw);
				}
				else if(e.type == SDL_MOUSEBUTTONDOWN)
				{
					MessageMouseClick mmc(e.button.x, e.button.y, e.button.button);
					ModelViewerCore::Get()->Broadcast(&mmc);
				}
				else if(e.type == SDL_MOUSEBUTTONUP)
				{
					MessageMouseUp mmu(e.button.button);
					ModelViewerCore::Get()->Broadcast(&mmu);
				}
				break;
			}
		}
	}

	pollRegisteredCallBacks();
}