/* Start Header -------------------------------------------------------
Copyright (C) 2014 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written consent of DigiPen Institute of Technology is prohibited.
File Name:		GraphicsSystem.h
Purpose:		Graphics System for Drawing the the model
				It also does all the heavy work under the hood as well
Language:		C++ and compiled in VS2012
Platform:		Visual Studio 2012, Windows 8
Project:		CS560
Author:			Ilan Keshet
Creation date:	16/9/2014
- End Header --------------------------------------------------------*/

#include "Precompiled.h"
#include "GraphicsSystem.h"
#include "Model.h"
#include "InputSystem.h"
#include "WindowsSystem.h"

#define ASSETS_DIRECTORY "..\\Resources\\Assets\\"

#define CHECKERROR {int err = glGetError(); if (err) { fprintf(stderr, "OpenGL error %d (at line %d): %s\n", err, __LINE__, glewGetErrorString(err)); } }

#define PLANE_SIZE 100
#define ZNEAR 1.0f
#define ZFAR 200.0f
#define FOVY 45.0f
#define ASPECT 640.0f / 480.0f

GraphicsSystem::GraphicsSystem() : modelTexture(0), goalTexture(0), model(0), goal(0), path(&lDrawer), ikEnabled(false), endReached(false), initiateIk(false)
{
}

GraphicsSystem::~GraphicsSystem()
{
	if(modelTexture)
	{
		delete modelTexture;
	}

	if(goalTexture)
	{
		delete goalTexture;
	}

	if(model)
	{
		delete model;
	}

	if(goal)
	{
		delete goal;
	}
}

/******************************************************************************/
/*!
	Initializes the entire Graphics system including loading the model
*/
/******************************************************************************/
void GraphicsSystem::Initialize()
{
	glewExperimental = GL_TRUE;
	glewInit();

	glEnable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glLineWidth(0.25f);

	zoom = 45.0f;
	eyeXRotation = 45;
	eyeYRotation = 0;

	sceneCenterX = 0;
	sceneCenterY = 0;

	proj = glm::perspective(FOVY, ASPECT, ZNEAR, ZFAR);

	path.addPoint(glm::vec3(0.0f, 0.0f, 0.0f));

	glViewport(0, 0, WindowsSystem::Get()->GetWidth(), WindowsSystem::Get()->GetHeight());

	CHECKERROR;

	InitializeStaticShader();

	CHECKERROR;

	InitializeSkinnedShader();

	CHECKERROR;

	InitializeDebugShader();

	CHECKERROR;

	ikEnabled = false;
	initiateIk = false;

	LoadBinModel(ASSETS_DIRECTORY "Sphere.bin", goal);

	SetBinConfigurations("Sphere.bin", goalTransform, goal, goalTexture);

	LoadBinModel(ASSETS_DIRECTORY + binFile, model);

	SetBinConfigurations(binFile, modelTransform, model, modelTexture);

	CHECKERROR;

	editor.Initialize();

	lDrawer.Initialize(proj);
}

#define PLANE_Y_OFFSET -0.1f

/******************************************************************************/
/*!
	Updates the modelMatrix using the passed in modelTransform
*/
/******************************************************************************/
void GraphicsSystem::UpdateModelMatrix(Transformation const& modelTransform)
{
	modelMatrix = glm::translate(glm::mat4(1), modelTransform.translate);
	modelMatrix = glm::scale(modelMatrix, modelTransform.scale);
	modelMatrix = glm::rotate(modelMatrix, modelTransform.rotation, glm::vec3(0.0f, 1.0f, 0.0));
}

/******************************************************************************/
/*!
	Updates the Model and draws to the screen
*/
/******************************************************************************/
void GraphicsSystem::Update(float dt)
{
	UpdateModelMatrix(modelTransform);

	viewMatrix = glm::mat4(1);
	viewMatrix = glm::translate(viewMatrix, glm::vec3(sceneCenterX, sceneCenterY, -zoom));
	viewMatrix = glm::rotate(viewMatrix, eyeXRotation, glm::vec3(1.0f, 0.0f, 0.0f));
	viewMatrix = glm::rotate(viewMatrix, eyeYRotation, glm::vec3(0.0f, 1.0f, 0.0f));

	UpdateModel(dt);

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	if(fillPolygon)
	{
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	}
	else
	{
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	}

	RenderModel(model, modelTexture, showModel);

	glActiveTexture(GL_TEXTURE0 + modelTexture->getTexture());
	glBindTexture(GL_TEXTURE_2D, 0);

	//If there is more than 1 point on the path, then show the Goal Object
	if(path.numberOfPoints() > 1)
	{
		Transformation goalTransformOffset = goalTransform;
		goalTransformOffset.translate += goalOffset;

		UpdateModelMatrix(goalTransformOffset);
		RenderModel(goal, goalTexture, true);

		glActiveTexture(GL_TEXTURE0 + goalTexture->getTexture());
		glBindTexture(GL_TEXTURE_2D, 0);
	}

	glm::mat4 planeScaleMatrix = glm::scale(glm::mat4(1), planeTransform.scale);

	lDrawer.Bind(planeScaleMatrix, viewMatrix);

	int halfPlane = PLANE_SIZE / 2;

	if(showAxis)
	{
		lDrawer.DrawLine(glm::vec3(0.0 -halfPlane, 0.0, 0.0 - halfPlane), glm::vec3(5.0 - halfPlane, 0.0, 0.0 - halfPlane), glm::vec3(1.0, 0.0, 0.0));
		lDrawer.DrawLine(glm::vec3(0.0 -halfPlane, 0.0, 0.0 - halfPlane), glm::vec3(0.0 - halfPlane, 5.0, 0.0 - halfPlane), glm::vec3(0.0, 1.0, 0.0));
		lDrawer.DrawLine(glm::vec3(0.0 -halfPlane, 0.0, 0.0 - halfPlane), glm::vec3(0.0 - halfPlane, 0.0, 5.0 - halfPlane), glm::vec3(0.0, 0.0, 1.0));
	}

	if(showGround)
	{
		for(int i = -halfPlane; i <= halfPlane; i++)
		{
			lDrawer.DrawLine(glm::vec3(i, PLANE_Y_OFFSET, -halfPlane), glm::vec3(i, PLANE_Y_OFFSET, halfPlane), glm::vec3(0.1, 0.1, 0.1));
			lDrawer.DrawLine(glm::vec3(-halfPlane, PLANE_Y_OFFSET, i), glm::vec3(halfPlane, PLANE_Y_OFFSET, i), glm::vec3(0.1, 0.1, 0.1));
		}
	}

	if(showPath)
	{
		//Disabling depth test is to ensure it gets
		//drawn on top
		path.draw();
	}

	lDrawer.unBind();

	editor.Draw();
}

#define TAD_WALK_PACE 11.0f
#define TAD_RUN_PACE  18.0f

/******************************************************************************/
/*!
	Returns back the position of a bone in Model Space given the concatenated joint matrix
*/
/******************************************************************************/
glm::vec3 GraphicsSystem::GetModelSpacePosition(glm::mat4 jointMatrix)
{
	return glm::vec3(jointMatrix[3][0], jointMatrix[3][1], jointMatrix[3][2]);
}

#define MIN_DISTANCE 0.5f
#define EE_MIN_CHANGE 0.0001f

/******************************************************************************/
/*!
	Computes the Inverse Kinematics 
	jointMatrixBuffer -- The concatenated hierarchical matrix buffer for joints  
					  -- index 0 is the parent of the chosen root
					  -- index <end> is the End Effector

	jointChain		  -- Contains the names / indices of all the bones / bind position / etc

	jointChainPosesMap - Contains all the joint poses of all the bones that the IK will determine
						 This is put in a map for easy access later when trying to interpolate
*/
/******************************************************************************/
void GraphicsSystem::ComputeIKChain()
{
	vector<glm::mat4> jointMatrixBuffer;
	jointMatrixBuffer.resize(ikData.jointChain.size() + 1);

	//jointMatrix at 0 is the parent of the root
	//jointMatrix at end is the end effector
	jointMatrixBuffer[0] = model->GetJointMatrix(ikData.jointChain[ikData.jointChain.size() - 1]->parentIndex);

	//interate through all the jointChains -- using their index to get the current jointMatrix
	for(int i = ikData.jointChain.size() - 1; i >= 0; --i)
	{
		jointMatrixBuffer[jointMatrixBuffer.size() - i - 1] = model->GetJointMatrix(ikData.jointChain[i]->index);
		
		//This does 2 things -- 1 creates the map entry with the joint index
		//And then saves the current joints pose into that position
		JointPose &ikPose = ikData.jointChainPosesMap[ikData.jointChain[i]->index];
		ikPose = model->GetJointPose(ikData.jointChain[i]->index);
	}

	glm::vec3 goalWorldPosition = goalTransform.translate + goalOffset;

	glm::vec3 Pc = GetModelSpacePosition(jointMatrixBuffer[jointMatrixBuffer.size() - 1]);
	glm::vec3 PdModelSpace = glm::vec3(glm::inverse(modelMatrix) * glm::vec4(goalWorldPosition, 1.0f));
	float distanceToTarget = glm::length(Pc - PdModelSpace);
	
	//Continue doing the CCD algorithm until smaller than Min Distance is achieved
	while(distanceToTarget > MIN_DISTANCE)
	{
		//the std::map is ordered
		//having the Highest indexes last -- meaning
		//The end effector is at the end of the map  (hence why using rbegin)
		auto it = ikData.jointChainPosesMap.rbegin();

		int i = jointMatrixBuffer.size() - 1;

		it++; 
		i--; //This are both technically going the same direction, because it is an reverse iterator

		//Continue until we reach the root
		for( ; it != ikData.jointChainPosesMap.rend(); it++, i--)
		{
			glm::vec3 Pj = GetModelSpacePosition(jointMatrixBuffer[i]);

			glm::mat4 ModelToJointSpace = glm::inverse(jointMatrixBuffer[i]);

			Pj = glm::vec3(ModelToJointSpace * glm::vec4(Pj, 1.0f));
			Pc = glm::vec3(ModelToJointSpace * glm::vec4(Pc, 1.0f));
			glm::vec3 Pd = glm::vec3(ModelToJointSpace * glm::vec4(PdModelSpace, 1.0f));

			//The angle calculation is all done in the joints relative Space
			//Technically this means that Pj is always 0,0,0
			glm::vec3 Vck = glm::normalize(Pc - Pj);
			glm::vec3 Vdk = glm::normalize(Pd - Pj);

			float ak = acos(glm::clamp(glm::dot(Vck, Vdk) / (glm::length(Vck) * glm::length(Vdk)), -1.0f, 1.0f));
			glm::vec3 Vk = glm::cross(Vck, Vdk);

			//Create a quaternion from an angle and axis
			Quaternion quat(ak, Vk);
			Quaternion &jointR = it->second.Rotation; //And update the current joints rotation with that quaternion
			jointR = jointR * quat;

			glm::mat4 local = BuildTransform(it->second.Translation, jointR, it->second.Scale);

			//Update the jointmatrix at this joint using the new local transform
			jointMatrixBuffer[i] = jointMatrixBuffer[i - 1] * local;

			auto it2 = it;
			it2--;

			//Recalculate all the jointmatrices of all children.
			//until reaching the End Effector
			for(int j = i + 1; ; it2--, j++)
			{
				JointPose &pose = it2->second;
				glm::mat4 local = BuildTransform(pose.Translation, pose.Rotation, pose.Scale);

				jointMatrixBuffer[j] = jointMatrixBuffer[j - 1] * local;

				//End effector reached
				if(j == jointMatrixBuffer.size() - 1)
				{
					//determine new end effectors position
					Pc = GetModelSpacePosition(jointMatrixBuffer[j]);

					//Leave algorithm if already near enough to target
					if(glm::length(Pc - PdModelSpace) <= MIN_DISTANCE)
					{
						return;
					}

					break;
				}
			}
		}

		float newDistanceToTarget =  glm::length(Pc - PdModelSpace);
		
		//Leave algorithm if change was very small
		if(distanceToTarget - newDistanceToTarget < EE_MIN_CHANGE)
		{
			//No solution
			return;
		}
		else
		{
			distanceToTarget = newDistanceToTarget;
		}
	}
}

/******************************************************************************/
/*!
	Updates the model's animation and position on the path
*/
/******************************************************************************/
void GraphicsSystem::UpdateModel(float dt)
{
	//update the models animation based on its key frame information
	if(!pauseAnimation)
	{
		if(!endReached)
		{
			float speedPercent = 1.0f;

			//update the models position / rotation based on the path
			if(path.numberOfPoints() > 1)
			{
				speedPercent = path.UpdatePosition(modelSpeed, dt);

				modelTransform.translate = path.evaluate(path.GetCurrentU());

				float epsilsonStep = path.GetCurrentU() + 0.0001f;

				if(epsilsonStep < 1.0f)
				{
					glm::vec3 facingDirection = glm::normalize(path.evaluate(epsilsonStep) - modelTransform.translate);

					double angle = 90.0 - (180.0f / PI) * atan2(facingDirection.z, facingDirection.x);

					modelTransform.rotation = (float)angle;
				}
			}

			float currentSpeed = modelSpeed * speedPercent;
			float blendFactor = (currentSpeed - TAD_WALK_PACE) / (TAD_RUN_PACE - TAD_WALK_PACE); 
		
			if(blendFactor < 0)
			{
				blendFactor = 0;
			}
			else if(blendFactor > 1)
			{
				blendFactor = 1;
			}

			float pace = (1.0f - blendFactor) * TAD_WALK_PACE + blendFactor * TAD_RUN_PACE;
			float cyclesPerSecond = std::max(currentSpeed / pace, 0.75f);

			model->SetAnimationCyclesPerSecond(cyclesPerSecond);
		
			if(animationBlending && model->GetName() == "Tad")
			{
				//4 denotes the walk animation
				//0 represents the run animation
				model->UpdateBlend(4, 0, blendFactor, dt);
			}
			else
			{
				model->Update(dt);
			}
		}
		else
		{
			if(ikEnabled && model->GetName() == "Tad")
			{
				if(!initiateIk)
				{
					model->SetAnimationCyclesPerSecond(2.0f);
					
					//These two keyframes have Tad with his front foot on the ground
					vector<int> keyTimesToStopAt; keyTimesToStopAt.push_back(2); keyTimesToStopAt.push_back(14);
					initiateIk = model->UpdateUntilKeyFrame(dt, keyTimesToStopAt);
				}
				else
				{
					if(ikData.jointChainPosesMap.size() == 0)
					{
						ComputeIKChain();
					}

					ikData.blend += dt;

					if(ikData.blend > 1.0f)
					{
						ikData.blend = 1.0f;
					}

					model->UpdateIk(ikData.blend, ikData.jointChainPosesMap);
				}
			}
			else
			{
				model->UpdateUntilRestart(dt);
			}
		}
	}
	else
	{
		model->Update(0);
	}
}

/******************************************************************************/
/*!
	Renders just the Model
*/
/******************************************************************************/
void GraphicsSystem::RenderModel(Model* model, Texture *tex, bool showModel)
{
	glm::mat4 normalMatrix = glm::transpose(glm::inverse(viewMatrix * modelMatrix));

	glActiveTexture(GL_TEXTURE0 + tex->getTexture());
	glBindTexture(GL_TEXTURE_2D, tex->getTexture());

	if(model->IsSkinned())
	{
		skinnedShader.Bind();
		glUniformMatrix4fv(skinnedShader("modelMatrix"), 1, GL_FALSE, glm::value_ptr(modelMatrix));
		glUniformMatrix4fv(skinnedShader("viewMatrix"), 1, GL_FALSE, glm::value_ptr(viewMatrix));
		glUniformMatrix4fv(skinnedShader("normalMatrix"), 1, GL_FALSE, glm::value_ptr(normalMatrix));
		glUniform1i(skinnedShader("tex"), tex->getTexture());
	}
	else
	{
		staticShader.Bind();
		glUniformMatrix4fv(staticShader("modelMatrix"), 1, GL_FALSE, glm::value_ptr(modelMatrix));
		glUniformMatrix4fv(staticShader("viewMatrix"), 1, GL_FALSE, glm::value_ptr(viewMatrix));
		glUniformMatrix4fv(staticShader("normalMatrix"), 1, GL_FALSE, glm::value_ptr(normalMatrix));
		glUniform1i(staticShader("tex"), tex->getTexture());
	}

	glBindVertexArray(model->vao);

	if(showModel)
	{
		if(model)
		{
			if(model->IsSkinned())
			{
				skinnedShader.Bind();

				if(bindPose)
				{
					model->DrawBindPose(&skinnedShader);
				}
				else
				{
					model->Draw(&skinnedShader);
				}
			}
			else
			{
				staticShader.Bind();
				model->Draw(&staticShader);
			}
		}
	}

	lDrawer.Bind(modelMatrix, viewMatrix);

	RenderSkeleton(model);
}

/******************************************************************************/
/*!
	Renders just the Skeleton of the model
*/
/******************************************************************************/
void GraphicsSystem::RenderSkeleton(Model *model)
{
	if(model && showSkeleton)
	{
		if(bindPose)
		{
			model->DrawBindPoseSkeleton(&lDrawer);
		}
		else
		{
			model->DrawSkeleton(&lDrawer);
		}
	}
}

/******************************************************************************/
/*!
	Initializes all the static vertex attributes to the passed in shader program
*/
/******************************************************************************/
void GraphicsSystem::SetStaticVertexAttributes(GLuint shaderProgram, unsigned int size)
{
	//Bind Different Buffer for the Shader
	GLint posAttrib = glGetAttribLocation(shaderProgram, "position");
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, size, 0);

	CHECKERROR;

	GLint texAttrib = glGetAttribLocation(shaderProgram, "tex");
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, size, (void*)(6 * sizeof(GLfloat)));

	CHECKERROR;

	GLint normalAttrib = glGetAttribLocation(shaderProgram, "normal");
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, size, (void*)(3 * sizeof(GLfloat)));

	CHECKERROR;

	GLint tangentAttrib = glGetAttribLocation(shaderProgram, "tangent");
	glEnableVertexAttribArray(3);
	glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, size, (void*)(8 * sizeof(GLfloat)));

	CHECKERROR;
}

/******************************************************************************/
/*!
	Initializes all the skinned vertex attributes to the passed in shader program
*/
/******************************************************************************/
void GraphicsSystem::SetSkinnedVertexAttributes(GLuint shaderProgram, unsigned int size)
{
	//Bind Different Buffer for the Shader
	GLint posAttrib = glGetAttribLocation(shaderProgram, "position");
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, size, 0);

	GLint normalAttrib = glGetAttribLocation(shaderProgram, "normal");
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, size, (void*)(3 * sizeof(GLfloat)));

	GLint texAttrib = glGetAttribLocation(shaderProgram, "tex");
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, size, (void*)(6 * sizeof(GLfloat)));

	GLint boneWeightAttrib = glGetAttribLocation(shaderProgram, "boneWeights");
	glEnableVertexAttribArray(3);
	glVertexAttribPointer(3, 4, GL_FLOAT, GL_FALSE, size, (void*)(8 * sizeof(GLfloat)));

	GLint boneIndicesAttrib = glGetAttribLocation(shaderProgram, "boneIndices");
	glEnableVertexAttribArray(4);
	glVertexAttribIPointer(4, 4, GL_UNSIGNED_BYTE, size, (void*)(12 * sizeof(GLfloat)));

	GLint tangentAttrib = glGetAttribLocation(shaderProgram, "tangent");
	glEnableVertexAttribArray(5);
	glVertexAttribPointer(5, 3, GL_FLOAT, GL_FALSE, size, (void*)(16 * sizeof(GLfloat)));

	CHECKERROR;
}

/******************************************************************************/
/*!
	Loads the config file from the assets directory -- and
	sets up the texture from that file
*/
/******************************************************************************/
void GraphicsSystem::SetBinConfigurations(string binFile, Transformation &modelTransform, Model *& model, Texture *&tex)
{
	string configFile = ASSETS_DIRECTORY + GetFilenameFromPath(binFile) + ".config";

	modelTransform.scale = glm::vec3(1.0, 1.0, 1.0);
	modelTransform.translate = glm::vec3(0.0, 0.0, 0.0);
	ikData.jointChain.clear();
	ikData.jointChainPosesMap.clear();

	if(FileExists(configFile))
	{
		string line;
		ifstream input(configFile);

		while(getline(input, line))
		{
			string command;
			string tmp;

			if(line.find("[MODEL]") != string::npos)
			{
				while(getline(input, line))
				{
					stringstream ss;
					ss.str(line);

					command = "";

					ss >> command;

					if(command == "")
					{
						break;
					}

					ss >> tmp;
					ss >> tmp;

					if(command == "texture")
					{
						LoadTexture(ASSETS_DIRECTORY + tmp, tex);
					}
					else if(command == "animation")
					{
						Animation* loadedAnim = model->LoadAnimation(ASSETS_DIRECTORY + tmp);

						ss >> tmp;

						if(tmp == "normalize")
						{
							loadedAnim->NormalizeTimeLine();
						}
					}
					else if(command == "worldscale")
					{
						float scaleX = (float)atof(tmp.c_str());
						float scaleY;
						float scaleZ;

						ss >> scaleY;
						ss >> scaleZ;

						modelTransform.scale = glm::vec3(scaleX, scaleY, scaleZ);
					}
					else if(command == "worldtranslate")
					{
						float translateX = (float)atof(tmp.c_str());
						float translateY;
						float translateZ;

						ss >> translateY;
						ss >> translateZ;

						modelTransform.translate = glm::vec3(translateX, translateY, translateZ);
					}
				}
			}
			else if(line.find_first_of("[IK]") != string::npos)
			{
				string ikHandleName = "";
				string ikRootName = "";

				while(getline(input, line))
				{
					stringstream ss;
					ss.str(line);

					command = "";

					ss >> command;

					if(command == "")
					{
						break;
					}

					ikEnabled = true;

					if(command == "handle")
					{
						ikHandleName = line.substr(line.find("= ") + 2, string::npos);
					}
					else if(command == "root")
					{
						ikRootName = line.substr(line.find("= ") + 2, string::npos);
					}
				}

				if(!model->GetJointChain(ikHandleName, ikRootName, ikData.jointChain))
				{
					printf("Could create joint chain with handle: %s, root: %s\n", ikHandleName.c_str(), ikRootName.c_str());
					ikEnabled = false;
					ikData.jointChain.clear();
				}
			}
		}
	}
}

/******************************************************************************/
/*!
	Loads a passed in texture
*/
/******************************************************************************/
void GraphicsSystem::LoadTexture(string textureFile, Texture *&tex)
{
	CHECKERROR;

	if(tex)
	{
		GLuint id = tex->getTexture();

		glDeleteTextures(1, &id);

		delete tex;
	}

	this->textureFile = GetFilenameWithExtensionFromPath(textureFile);
	tex = new Texture(textureFile);
	
	staticShader.Bind();
	glUniform1i(staticShader("tex"), 0);
	staticShader.unBind();

	skinnedShader.Bind();
	glUniform1i(skinnedShader("tex"), 0);
	skinnedShader.unBind();

	CHECKERROR;
}

/******************************************************************************/
/*!
	Handles Messages sent from the core
*/
/******************************************************************************/
void GraphicsSystem::SendMsg(Message * mess){
	if(mess->MessageId == Mid::DropFile)
	{
		MessageDropFile* mdf = dynamic_cast<MessageDropFile*>(mess);

		string ext = GetFileExtension(mdf->filepath);

		if(ext == "bin")
		{
			binFile = GetFilenameWithExtensionFromPath(mdf->filepath);

			LoadBinModel(mdf->filepath, model);

			SetBinConfigurations(binFile, modelTransform, model, modelTexture);
		}
		else if(ext == "png" || ext == "tga" || ext == "jpg")
		{
			LoadTexture(mdf->filepath, modelTexture);
		}
	}
	else if(mess->MessageId == Mid::MouseClick)
	{
		MessageMouseClick *mmc = dynamic_cast<MessageMouseClick*>(mess);
		mouseX = mmc->x;
		mouseY = mmc->y;
		mouseButton = mmc->mouseButton;

		mouseButtonDown = true;

		//Ray Casting with ground plane on right mouse button
		if(mouseButton == SDL_BUTTON_RIGHT)
		{
			glm::vec3 eyePos = glm::vec3(glm::inverse(viewMatrix) * glm::vec4(0.0f, 0.0f, 0.0f, 1.0f));

			float x = (2.0f * mouseX) / WindowsSystem::Get()->GetWidth() - 1.0f;
			float y = 1.0f - (2.0f * mouseY) / WindowsSystem::Get()->GetHeight(); 

			glm::vec4 rayClip(x, y, -1.0, 1.0);

			glm::vec4 rayEye = glm::inverse(proj) * rayClip;
			rayEye = glm::vec4(rayEye.x, rayEye.y, -1.0f, 0.0f);

			glm::vec3 rayWor = glm::vec3(glm::inverse(viewMatrix) * rayEye);

			//Intersect a ray with the ground plane with y = 0
			float t = eyePos.y / -rayWor.y;

			//Compute the raycasted position
			glm::vec3 rayCastPos = eyePos + rayWor * t;

			path.addPointAndCalc(rayCastPos);

			Point p = path.evaluate(0.99999f);

			glm::vec3 direction = glm::normalize(rayCastPos - p);

			goalTransform.translate = rayCastPos + 7.0f * direction;
			goalTransform.translate.y = 10.0f;
		}
	}
	else if(mess->MessageId == Mid::MouseMotion)
	{
		if(mouseButtonDown)
		{
			MessageMouseMotion *mmm = dynamic_cast<MessageMouseMotion*>(mess);
			int dx = mmm->x - mouseX;
			int dy = mmm->y - mouseY;

			if(mouseButton == SDL_BUTTON_LEFT)
			{
				eyeYRotation += dx;
				eyeXRotation += dy;
			}
			else if(mouseButton == SDL_BUTTON_MIDDLE)
			{
				sceneCenterX += dx / 10.0f;
				sceneCenterY -= dy / 10.0f;
			}

			mouseX = mmm->x;
			mouseY = mmm->y;
		}
	}
	else if(mess->MessageId == Mid::MouseWheel)
	{
		MessageMouseWheel *mmw = dynamic_cast<MessageMouseWheel*>(mess);
		zoom -= mmw->yScroll / 2.0f;
	}
	else if(mess->MessageId == Mid::MouseUp)
	{
		mouseButtonDown = false;
	}
	else if(mess->MessageId == Mid::TabPressed)
	{
		if(model->GetName() != "Tad")
		{
			int totalAnimations = model->TotalAnimations();

			if(totalAnimations != 0)
			{
				int animationId = model->GetCurrentAnimationId();

				int nextAnimationId = animationId + 1;

				if(nextAnimationId == totalAnimations)
				{
					nextAnimationId = 0;
				}

				model->SetCurrentAnimationId(nextAnimationId);
			}
		}
	}
	else if(mess->MessageId == Mid::RestartAnimation)
	{
		path.Restart();
		endReached = false;
		initiateIk = false;
		ikData.Restart();
	}
	else if(mess->MessageId == Mid::EndReached)
	{
		endReached = true;
	}
}

/******************************************************************************/
/*!
	Loads a Bin file
*/
/******************************************************************************/
void GraphicsSystem::LoadBinModel(string filepath, Model *&model)
{
	if(model)
	{
		delete model;
		model = 0;
	}

	CHECKERROR;

	model = Model::LoadFile(filepath);

	CHECKERROR;

	glGenVertexArrays(1,&model->vao);
	glBindVertexArray(model->vao);

	CHECKERROR;

	glGenBuffers(1, &model->vbo);
	glBindBuffer(GL_ARRAY_BUFFER,model->vbo);

	CHECKERROR;

	Mesh const * mesh = model->GetMesh();

	if(mesh)
	{
		int size;

		if(model->IsSkinned())
		{
			size = sizeof(SkinnedModelVertex);
		}
		else
		{
			size = sizeof(StaticModelVertex);
		}

		CHECKERROR;

		glBufferData(GL_ARRAY_BUFFER, mesh->numVertices * size, mesh->vertexBufferData, GL_STATIC_DRAW);

		CHECKERROR;

		glGenBuffers(1, &model->ebo);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, model->ebo);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, mesh->numIndices * sizeof(unsigned int), mesh->indices, GL_STATIC_DRAW);

		if(model->IsSkinned())
		{
			SetSkinnedVertexAttributes(skinnedShader.getShaderProgram(), sizeof(SkinnedModelVertex));
		}
		else
		{
			SetStaticVertexAttributes(staticShader.getShaderProgram(), sizeof(StaticModelVertex));
		}
	}
}

/******************************************************************************/
/*!
	Initializes the static shader -- for rendering objects that do not animate
*/
/******************************************************************************/
void GraphicsSystem::InitializeStaticShader()
{
	// Create Shader Program
	staticShader.createShaderProgram("Shaders\\staticVertexShader.glsl","Shaders\\staticFragmentShader.glsl");
	staticShader.Bind();
	staticShader.addUniform("projMatrix");
	staticShader.addUniform("modelMatrix");
	staticShader.addUniform("viewMatrix");
	staticShader.addUniform("normalMatrix");
	staticShader.addUniform("viewInvMatrix");
	staticShader.addUniform("tex");

	CHECKERROR;

	glUniformMatrix4fv(staticShader("projMatrix"), 1, GL_FALSE, glm::value_ptr(proj));

	CHECKERROR;

	glUniform1i(staticShader("tex"), 0);

	CHECKERROR;
}

/******************************************************************************/
/*!
	Initializes the debug shader for drawing lines
*/
/******************************************************************************/
void GraphicsSystem::InitializeDebugShader()
{
}

/******************************************************************************/
/*!
	Initializes the skinned shader for rendering objects that animate
*/
/******************************************************************************/
void GraphicsSystem::InitializeSkinnedShader()
{
	// Create Shader Program
	skinnedShader.createShaderProgram("Shaders\\skinnedVertexShader.glsl","Shaders\\skinnedFragmentShader.glsl");
	skinnedShader.Bind();
	skinnedShader.addUniform("projMatrix");
	skinnedShader.addUniform("modelMatrix");
	skinnedShader.addUniform("viewMatrix");
	skinnedShader.addUniform("normalMatrix");
	skinnedShader.addUniform("viewInvMatrix");
	skinnedShader.addUniform("tex");
	skinnedShader.addUniform("skinningMatrices");
	skinnedShader.addUniform("bindPose");

	glUniformMatrix4fv(skinnedShader("projMatrix"), 1, GL_FALSE, glm::value_ptr(proj));

	//glUniform1i(glGetUniformLocation(sceneShaderProgram, "tex"), 0);
	glUniform1i(skinnedShader("tex"), 0);
}

IKData::IKData() : blend(0)
{
}

void IKData::Restart()
{
	blend = 0;
	jointChainPosesMap.clear();
}