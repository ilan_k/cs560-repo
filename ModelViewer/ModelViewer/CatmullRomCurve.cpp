/* Start Header -------------------------------------------------------
Copyright (C) 2014 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written consent of DigiPen Institute of Technology is prohibited.
File Name:		CatmullRomCurve.cpp
Purpose:		Class for creating a curve that can be managed by arc length
Language:		C++ and compiled in VS2012
Platform:		Visual Studio 2012, Windows 8
Project:		CS560
Author:			Ilan Keshet
Creation date:	11/13/2014
- End Header --------------------------------------------------------*/

#include "Precompiled.h"
#include "CatmullRomCurve.h"
#include "ModelViewerCore.h"

//tension parameter
#define T 0.5f

CatmullRomCurve::CatmullRomCurve(LineDrawer* lDrawer) : currentU(0.0f), t(0.0f), Curve(lDrawer), L(0.01f){

}

float CatmullRomCurve::getCatmullRomX(float t, Point const &P0, Point const &P1, Point const &P2, Point const &P3){
	return ( ( 1 * P1.x ) + 
		(-T * P0.x + T * P2.x) * t +
		(2 * T * P0.x + (T - 3)* P1.x + (3 - 2 * T) * P2.x - T * P3.x) * t * t +
		(-T * P0.x + (2 - T) * P1.x + (T - 2) * P2.x + T * P3.x) * t * t * t);
}

float CatmullRomCurve::getCatmullRomY(float t, Point const &P0, Point const &P1, Point const &P2, Point const &P3){
	return ( ( 1 * P1.y ) + 
		(-T * P0.y + T * P2.y) * t +
		(2 * T * P0.y + (T - 3)* P1.y + (3 - 2 * T) * P2.y -T * P3.y) * t * t +
		(-T * P0.y + (2 - T) * P1.y + (T - 2) * P2.y + T * P3.y) * t * t * t);
}

float CatmullRomCurve::getCatmullRomZ(float t, Point const &P0, Point const &P1, Point const &P2, Point const &P3){
	return ( ( 1 * P1.z ) + 
		(-T * P0.z + T * P2.z) * t +
		(2 * T * P0.z + (T - 3)* P1.z + (3 - 2 * T) * P2.z -T * P3.z) * t * t +
		(-T * P0.z + (2 - T) * P1.z + (T - 2) * P2.z + T * P3.z) * t * t * t);
}

#define CONTROL_POINT_STEP 1.0f / (points.size() - 1)

//Evaluates a point on the curve, the passed in u should be between 0 to 1
Point CatmullRomCurve::evaluate(float u)
{
	if(points.size() > 1)
	{
		if(u == 1.0f)
		{
			return points[points.size() - 1];
		}

		//The following finds the Catmull Rom Spline segment 
		//that the passed in u is on
		unsigned int ind = 1;
		float start = 0;
		float end = CONTROL_POINT_STEP;

		while(end < u)
		{
			start = end;
			end += CONTROL_POINT_STEP;
			ind++;
		}

		//If at the first curve, there is no P i minus 2  curve -- use the same end point twice
		Point& P0 = (ind == 1) ? points[ind - 1] : points[ind - 2];

		//If at the first curve, there is no P i minus 1  curve -- use the same end point
		Point& P1 = points[ind - 1];

		//Current Pi
		Point& P2 = points[ind];

		//If at the last curve, use the last end point twice
		Point &P3 = (ind == points.size() - 1) ? points[ind] : points[ind + 1];

		//normalizes u so that on this catmull rom spline it is bounded by 0 to 1
		u = (u - start) / (end - start);

		assert(u >= 0.0 && u <= 1.0);

		float x = getCatmullRomX(u, P0, P1, P2, P3);
		float y = getCatmullRomY(u, P0, P1, P2, P3);
		float z = getCatmullRomZ(u, P0, P1, P2, P3);

		return Point(x, y, z);
	}
	else
	{
		return points[0];
	}
}

//Algorithm for binary searching in a Table for a given key
float BinarySearch(std::map<float, float> const &map, float key)
{
	if(key == 0.0f)
	{
		return 0.0f;
	}

	if(key == 1.0f)
	{
		return 1.0f;
	}

	//std::map already has a binary search function for exactly
	//my needs
	auto itup = map.upper_bound(key);
	auto itlow = itup;
	--itlow;

	assert(itlow != map.end() && itup != map.end());

	if(itlow == itup)
	{
		return itlow->second;
	}

	float startU = itlow->first;
	float endU = itup->first;
	float startS = itlow->second;
	float endS = itup->second;

	//Find the interpolated parameter t that is inbetween the 
	//two stored Us
	float t = (key - startU) / (endU - startU);

	assert(t >= 0 && t <= 1.0f);

	//Linear interpolate the result s from the two end points using calculated t
	return (1.0f - t) * startS + t * endS;
}

#define P1 0.3f
#define P2 0.8f

//T1 defines 0 to T1 where the increasing speed occurs
#define T1 (P1 * maxT)

//T2 defines 0 to T2 where the decreasing speed occurs
#define T2 (P2 * maxT)

//Pass in distance travelled from last time step
//Returns back the percent of speed from max speed that
//the object is traveling
float CatmullRomCurve::UpdatePosition(float maxSpeed, float dt)
{
	float s_t;
	t += dt;

	//This required a bit of pen and paper
	//to figure out the maxT until Tad reaches the end of the curve
	float maxT = (2.0f * L) / (maxSpeed * (1.0f + P2 - P1));

	float speedPercent = 1.0f;

	//Restart the path if the t passes the maxTime
	if(t > maxT)
	{
		MessageEndReached mer;
		ModelViewerCore::Get()->Broadcast(&mer);
		return 0.0f;
	}
	//Linearly increase speed for t < T1
	//The equations however are based on Distance however, not Velocity.
	//requires integration
	else if(t < T1)
	{
		s_t = maxSpeed * (t * t) / (2.0f * T1);
		speedPercent = t / T1;

		printf("Increasing Velocity");
	}
	//Constant Speed in this interval
	else if(t >= T1 && t < T2)
	{
		s_t = maxSpeed * T1 / 2.0f + maxSpeed * (t - T1);
		speedPercent = 1.0f;

		printf("Constant Velocity");
	}
	//Decreasing speed in this interval
	else if(t >= T2)
	{
		s_t = maxSpeed * T1 / 2.0f + maxSpeed * (T2 - T1) + (maxSpeed / 2.0f) * (t - T2);
		speedPercent = (t - maxT) / (T2 - maxT);

		printf("Decreasing Velocity");
	}

	//Normalizes s_t to be in the range of 0 to 1
	s_t /= L;

	printf("\tDis: %f, t: %f, anim: %f\r", s_t, t, speedPercent);

	currentU = BinarySearch(sToU, s_t);

	return speedPercent;
}


#define EPS1 0.05 //minimum error that a curve segment can have
#define EPS2 0.1 //minimum step size between curve segments

//Adaptive Arc Length Computation method as discussed in class
void CatmullRomCurve::recalculate()
{
	if(points.size() > 1)
	{
		float distanceTravelled = 0;

		if(uToS.size() > 0)
		{
			distanceTravelled = L * BinarySearch(uToS, currentU);
		}

		uToS.clear();
		sToU.clear();
	
		uToS[0.0f] = 0.0f;

		//List of curve segments
		list<CurveSegment> curveSegments;
		//Initially contains the entire curve
		curveSegments.push_back(CurveSegment(0.0f, 1.0f));

		//Continue while there are no more curve segments
		while(!curveSegments.empty())
		{
			//This is algorithm discussed in class
			for(auto it = curveSegments.begin(); it != curveSegments.end(); )
			{
				float u_a = it->u_a;
				float u_b = it->u_b;
				float u_m = (u_a + u_b) / 2.0f;

				Point const &p_a = evaluate(u_a);
				Point p_m = evaluate(u_m);
				Point p_b = evaluate(u_b);

				float A = glm::length(p_m - p_a);
				float B = glm::length(p_b - p_m);
				float C = glm::length(p_b - p_a);

				float D = A + B - C;

				//Requirements to add new curve segments
				//either error was too large, or stepsize too small
				if(D > EPS1 || abs(u_a - u_b) > EPS2)
				{
					CurveSegment newSegment(u_m, u_b);

					it->u_b = u_m;
				
					auto insertIt = it;
					insertIt++;

					//Inserting new curve segment after the current curve segment
					curveSegments.insert(insertIt, newSegment);
					++it; //Curve segment just inserted
					++it; //Next curve segment to process
				}
				//If the curve segment is the first 
				//in the list, and can be removed -- it is removed
				else if(it == curveSegments.begin())
				{
					auto arcIt = uToS.find(it->u_a);

					assert(arcIt != uToS.end());

					float u_a_s = arcIt->second;

					uToS[u_m] = u_a_s + A;
					uToS[u_b] = u_a_s + A + B;

					auto nextIt = it;
					++nextIt;

					//Erasing curve segment from curveseg list
					curveSegments.erase(it);

					it = nextIt;
				}
				else
				{
					++it;
				}
			}
		}

		auto last = uToS.rbegin();

		L = last->second;

		for(auto it = uToS.begin(); it != uToS.end(); ++it)
		{
			float s = it->second / L;

			sToU[s] = it->first;

			it->second = s;
		}

		//The following fixes the model to have the CurrentU at the same
		//place on the new curve
		if(distanceTravelled > 0)
		{
			distanceTravelled /= L;

			currentU = BinarySearch(sToU, distanceTravelled);
		}
	}
}

void CatmullRomCurve::draw(int levelOfDetail){
	if(this->points.size() > 1){
		double deltaU = 1.0 / ((double)(levelOfDetail * this->points.size()));

		Point last = evaluate(0);
		Point current = last;

		for(double u = deltaU; u < 1.0; u += deltaU)
		{
			current = evaluate((float)u);

			lDrawer->DrawLine(last, current);

			last = current;
		}

		current = evaluate(1.0);
		
		lDrawer->DrawLine(last, current);
	}
}