/* Start Header -------------------------------------------------------
Copyright (C) 2014 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written consent of DigiPen Institute of Technology is prohibited.
File Name:		Texture.cpp
Purpose:		Functionality for loading textures
Language:		C++ and compiled in VS2012
Platform:		Visual Studio 2012, Windows 8
Project:		CS560
Author:			Ilan Keshet
Creation date:	16/9/2014
- End Header --------------------------------------------------------*/

#include "Precompiled.h"
#include "Texture.h"

#define CHECKERROR {int err = glGetError(); if (err) { fprintf(stderr, "OpenGL error (at line %d):\n", __LINE__);} }

	bool buildTexImage(SDL_Surface* TextureData){
		//Format must either RGB, or RGBA  (ie has 24 bits or 32 bits)
		if(TextureData->format->BitsPerPixel != 24 && TextureData->format->BitsPerPixel != 32){
			cout << "Texture format's Bits Per Pixel was not 24 or 32." << endl;
			return false;
		}

		GLenum Format;
		GLint Colors = TextureData->format->BytesPerPixel;

		if (Colors == 4 ){
			if (TextureData->format->Rmask == 0x000000ff){
				Format = GL_RGBA;
			}
			else{
				Format = GL_BGRA;
			}
		}
		else if (Colors == 3){
			if (TextureData->format->Rmask == 0x000000ff)
				Format = GL_RGB;
			else
				Format = GL_BGR;
		}
		else{
			return false;
		}

		glTexImage2D(GL_TEXTURE_2D, 0, Colors, TextureData->w, TextureData->h, 0, Format, GL_UNSIGNED_BYTE, TextureData->pixels);

		return true;
	}

	Texture::Texture(string textureFile){
		SDL_Surface* image = NULL;

		if((image = IMG_Load(textureFile.c_str())) == NULL){
			cout << "Error could not load image from file: " << textureFile << endl;
			assert(false);
		}

		CHECKERROR;

		this->width = image->w;
		this->height = image->h;

		this->textureID = 0;
		
		glGenTextures(1, &this->textureID);

		CHECKERROR;

		//If OpenGL context was not initialized before calling of glGenTexture, break here
		assert(this->textureID != 0);

		glBindTexture(GL_TEXTURE_2D, this->textureID);
		CHECKERROR;
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		CHECKERROR;
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		CHECKERROR;
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		CHECKERROR;
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		CHECKERROR;
		glTexParameteri(GL_TEXTURE_2D, GL_GENERATE_MIPMAP, GL_TRUE);   //Requires GL 1.4. Removed from GL 3.1 and above.
		
		CHECKERROR;

		if(!buildTexImage(image)){
			cout << "Could not build a Texture Image from " << textureFile << endl;
			assert(false);
		}

		CHECKERROR;

		//Free surface
		SDL_FreeSurface(image);
	}

	unsigned int Texture::getTexture(){
		return this->textureID;
	}

	unsigned int Texture::getImageWidth(){
		return this->width;
	}

	unsigned Texture::getImageHeight(){
		return this->height;
	}

	Texture::~Texture(){
	}