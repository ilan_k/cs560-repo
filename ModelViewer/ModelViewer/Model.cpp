/* Start Header -------------------------------------------------------
Copyright (C) 2014 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written consent of DigiPen Institute of Technology is prohibited.
File Name:		Model.cpp
Purpose:		Class for storing / loading, and animating a model
Language:		C++ and compiled in VS2012
Platform:		Visual Studio 2012, Windows 8
Project:		CS560
Author:			Ilan Keshet
Creation date:	16/9/2014
- End Header --------------------------------------------------------*/

#include "Precompiled.h"
#include "Model.h"
#include "BinFileDeseralizer.h"
#include "GLSLshader.h"

Model::Model(std::string modelName) : modelName(modelName), modelType(Static), animationHandler(0), mesh(0)
{
}

Model::~Model()
{
	if(mesh)
	{
		delete mesh;
	}

	if(animationHandler)
	{
		delete animationHandler;
	}

	glDeleteBuffers(1, &ebo);
	glDeleteBuffers(1, &vbo);
	glDeleteVertexArrays(1, &vao);
}

//Chris Peters Binary File Format
const unsigned int FileStartKey = 'dpm2';
const unsigned int AnimationKey = 'anim';
const unsigned int MeshKey = 'mesh';
const unsigned int SkeletonKey = 'skel';

//My Maya SDK Plugin format
const unsigned int FileStartKey2 = 'maya';

/******************************************************************************/
/*!
	Loads a binary file format either from my Maya SDK plugin or Chris Peters Bin File
*/
/******************************************************************************/
Model* Model::LoadFile(string filename)
{
	BinFileDeseralizer deser(filename);

	ChunkHeader ch = deser.ReadChunkHeader();

	bool oldFormat = false;

	if(ch.type == FileStartKey)
	{
		oldFormat = true;
	}
	
	if(!oldFormat && ch.type != FileStartKey2)
	{
		return NULL;
	}

	Model * createdModel = new Model(GetFilenameFromPath(filename));

	ch = deser.PeekChunkHeader();

	while(ch.type != 0)
	{
		deser.ReadChunkHeader();

		switch(ch.type)
		{
		case MeshKey:
			{
				createdModel->mesh = new Mesh;
				createdModel->mesh->Read(deser, oldFormat);
				createdModel->mesh->Initialize();
			}
			break;
		case SkeletonKey:
			{
				createdModel->modelType = Skinned;
				Skeleton *skel = new Skeleton;
				skel->Read(deser, oldFormat);
				skel->Initialize();
				createdModel->animationHandler = new AnimationHandler(skel);
			}
			break;
		case AnimationKey:
			{
				Animation* anim = new Animation();
				anim->Read(deser, oldFormat);
				createdModel->animationHandler->AddAnimation(anim);
			}
			break;
		default:
			//This line does not skip chunk -- instead if jumps to end of file --
			//This is because endPos is incorrect in the binary file
			deser.SkipChunk(ch);
			break;
		}

		ch = deser.PeekChunkHeader();
	}

	return createdModel;
}


bool Model::GetJointChain(std::string const &handle, std::string const &root, vector<Joint*> &jointsOut)
{
	if(animationHandler)
	{
		return animationHandler->GetJointChain(handle, root, jointsOut);
	}
	else
	{
		return false;
	}
}

Animation* Model::LoadAnimation(string filename)
{
	BinFileDeseralizer deser(filename);

	ChunkHeader ch = deser.ReadChunkHeader();

	ch = deser.PeekChunkHeader();

	Animation* anim = 0;

	while(ch.type != 0)
	{
		deser.ReadChunkHeader();

		switch(ch.type)
		{
		case AnimationKey:
			{
				anim = new Animation();
				anim->Read(deser, false);

				assert(anim->TotalJointPoseSets() == animationHandler->GetSkeleton().GetNumberOfJoints());

				animationHandler->AddAnimation(anim);
			}
			break;
		}

		ch = deser.PeekChunkHeader();
	}

	return anim;
}

/******************************************************************************/
/*!
	Draws the Model -- whether it is a static or skinned 
*/
/******************************************************************************/
void Model::Draw(GLSLshader* shader) const
{
	if(mesh)
	{
		if(modelType == Skinned)
		{
			glUniform1i((*shader)("bindPose"), 0);
			animationHandler->Draw(shader, *mesh);
		}
		else if(modelType == Static)
		{
			glDrawElements(GL_TRIANGLES, mesh->numIndices, GL_UNSIGNED_INT, 0);
		}
	}
}

void Model::SetAnimationCyclesPerSecond(float cycles)
{
	if(animationHandler)
	{
		animationHandler->SetAnimationCyclesPerSecond(cycles);
	}	
}

/******************************************************************************/
/*!
	Updates the Model -- moving the animation forward by dt
*/
/******************************************************************************/
void Model::Update(float dt)
{
	if(animationHandler)
	{
		animationHandler->Update(dt);
	}
}

bool Model::UpdateUntilRestart(float dt)
{
	if(animationHandler)
	{
		return animationHandler->UpdateUntilRestart(dt);
	}

	return false;
}

bool Model::UpdateUntilKeyFrame(float dt, vector<int> const &keyframesToStopAt)
{
	if(animationHandler)
	{
		return animationHandler->UpdateUntilKeyFrame(dt, keyframesToStopAt);
	}

	return false;
}

void Model::UpdateBlend(int animId1, int animId2, float blendFactor, float dt)
{
	if(animationHandler)
	{
		animationHandler->UpdateBlend(animId1, animId2, blendFactor, dt);
	}
}

void Model::UpdateIk(float k, std::map<int, JointPose> const & ikJointPoseMap)
{
	if(animationHandler)
	{
		animationHandler->UpdateIk(k, ikJointPoseMap);
	}
}

void Model::RestartAnimation()
{
	if(animationHandler)
	{
		animationHandler->Restart();
	}
}

/******************************************************************************/
/*!
	Draws the Bind Pose
*/
/******************************************************************************/
void Model::DrawBindPose(GLSLshader* shader) const
{
	if(mesh)
	{
		if(modelType == Skinned)
		{
			glUniform1i((*shader)("bindPose"), 1);
			animationHandler->Draw(shader, *mesh);
		}
		else if(modelType == Static)
		{
			glDrawElements(GL_TRIANGLES, mesh->numIndices, GL_UNSIGNED_INT, 0);
		}
	}
}

/******************************************************************************/
/*!
	Draws the Bind Pose Skeleton -- the Debug shader should be activate
	before calling this function
*/
/******************************************************************************/
void Model::DrawBindPoseSkeleton(LineDrawer * lDrawer) const
{
	if(this->modelType == Skinned)
	{
		this->animationHandler->DrawSkeleton(lDrawer, true);
	}
}


/******************************************************************************/
/*!
	Draws the skeleton -- based on the current pose
*/
/******************************************************************************/
void Model::DrawSkeleton(LineDrawer * lDrawer) const
{
	if(this->modelType == Skinned)
	{
		this->animationHandler->DrawSkeleton(lDrawer, false);
	}
}

int Model::TotalAnimations()
{
	if(animationHandler)
	{
		return animationHandler->TotalAnimations();
	}
	else
	{
		return 0;
	}
}

void Model::SetCurrentAnimationId(int animation)
{
	assert(animationHandler);

	animationHandler->SetCurrentAnimationId(animation);
}

int Model::GetCurrentAnimationId()
{
	assert(animationHandler);

	return animationHandler->GetCurrentAnimationId();
}

JointPose Model::GetJointPose(int joint)
{
	assert(animationHandler);

	return animationHandler->GetJointPose(joint);
}

glm::vec3 Model::GetJointModelPosition(int joint)
{
	assert(animationHandler);

	return animationHandler->GetJointModelPosition(joint);
}

glm::mat4 Model::GetJointMatrix(int joint)
{
	assert(animationHandler);

	return animationHandler->GetJointMatrix(joint);
}