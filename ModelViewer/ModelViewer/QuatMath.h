/* Start Header -------------------------------------------------------
Copyright (C) 2014 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written consent of DigiPen Institute of Technology is prohibited.
File Name:		QuatMath.h
Purpose:		My Math library for Quaternions
Language:		C++ and compiled in VS2012
Platform:		Visual Studio 2012, Windows 8
Project:		CS560
Author:			Ilan Keshet
Creation date:	16/9/2014
- End Header --------------------------------------------------------*/

#pragma once

#include <glm.hpp>

#define PI 3.141593f

//A class for Quaternions -- Internally stored as unit quaternions
//in S^3
class Quaternion
{
public:
	Quaternion() : quat(0.0, 0.0, 0.0, 1.0) {}

	Quaternion(glm::vec4 const& data);

	Quaternion(float angle, glm::vec3 axis);

	//0 accesses x, 1 accesses y, 2 accesses z, 3 accesses
	float operator[](int i) const
	{
		assert(i >= 0 && i <= 3);

		return quat[i];
	}

	Quaternion operator*(Quaternion const & rhs);

	Quaternion conjugate();

	glm::vec4 const & Get() const;
	
	//Converts a Quaternion to a Matrix
	glm::mat4 ToMat4() const;
	
	static float Quaternion::dotProduct(Quaternion const & q1, Quaternion const &q2);

private:
	//stored order: x,y,z,w
	glm::vec4 quat;
};

//t = 0 is at quat1, t = 1 is at quat2
Quaternion Slerp(Quaternion const &quat1, Quaternion const &quat2, float t);
