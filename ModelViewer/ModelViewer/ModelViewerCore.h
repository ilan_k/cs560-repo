/* Start Header ------------------------------------------------------- 
Copyright (C) 2013 DigiPen Institute of Technology. 
Reproduction or disclosure of this file or its contents without the prior 
written consent of DigiPen Institute of Technology is prohibited. 
 
File Name: GEGameCore.h
Purpose: Handles managing systems added into the GameEngine as well as running the game loop
Language: Microsoft Visual C++, Compiler: MSV 2010
Platform: Has been tested on Windows 7 in DigiPen Lab Computers
Project: CS529_ilan.k_Game_Engine
Author: Ilan Keshet, ilan.k, 60001813
Creation date: December 13th 2013
- End Header --------------------------------------------------------*/ 

#pragma once

#include "Message.h"
#include "System.h"

class ModelViewerCore
{
public:
	ModelViewerCore();
	~ModelViewerCore();
	///Update all the systems until the game is no longer active.
	void GameLoop();
		
	string GetName(){ return "Core"; }

	static ModelViewerCore* Get();

	///Destroy all systems in reverse order that they were added.
	void DestroySystems();
		
	///Adds a new system to the game.
	void AddSystem(System* system);
		
	///Initializes all systems in the game.
	void Initialize();

	void Broadcast(Message *mess);
private:
	//Tracks all the systems the game uses
	vector<System*> Systems;

	//Is the game running (true) or being shut down (false)?
	bool ModelViewerRunning;
};