/* Start Header -------------------------------------------------------
Copyright (C) 2014 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written consent of DigiPen Institute of Technology is prohibited.
File Name:		WindowsSystem.h
Purpose:		Functionality for loading / opening a window
Language:		C++ and compiled in VS2012
Platform:		Visual Studio 2012, Windows 8
Project:		CS560
Author:			Ilan Keshet
Creation date:	16/9/2014
- End Header --------------------------------------------------------*/

#include "Precompiled.h"
#include "WindowsSystem.h"

WindowsSystem * windowsSys;

WindowsSystem::WindowsSystem(const char* ApplicationName, int Width, int Height):
	window(NULL),ApplicationName(ApplicationName), WIDTH(Width), HEIGHT(Height)
{
	windowsSys = this;
}

WindowsSystem::~WindowsSystem()
{
	ShutDown();
}

WindowsSystem const* WindowsSystem::Get()
{
	return windowsSys;
}

void WindowsSystem::Initialize()
{
	if( SDL_Init( SDL_INIT_VIDEO ) < 0 )
	{
		printf( "SDL could not initialize! SDL_Error: %s\n", SDL_GetError());
	}
	else
	{
		//Setting up the Attribute for stecil buffer
		SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE, 8);
		//Create window
		window = SDL_CreateWindow("3D pipeline", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, WIDTH, HEIGHT, SDL_WINDOW_OPENGL);
		if( window == NULL )
		{
			printf( "Window could not be created! SDL_Error: %s\n", SDL_GetError() );
		}
		else
		{
			SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
			SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 2);

			SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_COMPATIBILITY);

			context = SDL_GL_CreateContext(window);

			printf("OpenGL Version: %s\n", glGetString(GL_VERSION));
			printf("GLSL Version: %s\n", glGetString(GL_SHADING_LANGUAGE_VERSION));
			printf("Rendered by: %s\n", glGetString(GL_RENDERER));
		}
	}
}

void WindowsSystem::ShutDown()
{
	SDL_GL_DeleteContext(context);
	SDL_DestroyWindow( window );
	SDL_Quit();
}

void WindowsSystem::Update(float dt)
{
	SDL_GL_SwapWindow(window);
}
