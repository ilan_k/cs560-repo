#include "Precompiled.h"
#include "FrameRateTimer.h"
#include <SDL.h>

namespace FrameRateTimer{	
	static int startTime;
	static int frameRate = 1;

	void FrameRateTimerStart(){
		startTime = SDL_GetTicks();
	}

	void FrameRateTimerEnd(){
		frameRate = SDL_GetTicks() - startTime;
	}

	float GetFrameTime(){
		return ((float)frameRate)/1000.0f;
	}
}