#pragma once

#include "System.h"

class WindowsSystem : public System
{
public:
	WindowsSystem(const char*, int, int);
	~WindowsSystem();
	void Initialize();
	void Update(float dt);
	string GetName() { return "Windows"; }

	int GetWidth() const { return WIDTH; }
	int GetHeight() const { return HEIGHT; }

	static WindowsSystem const* Get(); 

private:
	void ShutDown();
	SDL_Window* window;
	SDL_GLContext context;
	SDL_Event windowEvent;
	const char* ApplicationName;
	int WIDTH;
	int HEIGHT;
};