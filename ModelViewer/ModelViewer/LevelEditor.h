#pragma once

#include "AntTweakBar.h"

#define ASSET_DIRECTORY "..\\..\\SkyEngine\\Game\\Assets"
#define SHADER_DIRECTORY "..\\..\\SkyEngine\\Game\\Shaders"

extern bool showModel;
extern bool showGround;
extern bool showModel;
extern bool showSkeleton;
extern bool fillPolygon;
extern bool bindPose;
extern bool pauseAnimation;
extern bool showPath;
extern bool showAxis;
extern bool animationBlending;

extern float modelSpeed;
extern glm::vec3 goalOffset;

class LevelEditor
{
public:
	LevelEditor();

	//Should be destroyed before Graphics
	~LevelEditor();

	void Initialize();

	void Refresh() const;

	void Draw();

	static LevelEditor const* Get();

private:
	void AddFileList(string const &DisplayLabel, string twVariableName, vector<string> const& fileList, int * editorVariable);

	void InitializeBar();

	TwBar *bar;
};