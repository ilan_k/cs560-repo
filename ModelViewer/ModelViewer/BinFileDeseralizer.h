/* Start Header -------------------------------------------------------
Copyright (C) 2014 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written consent of DigiPen Institute of Technology is prohibited.
File Name:		BinFileDeseralizer.h
Purpose:		Class for reading a binary file in either Chris Peters format or mine
Language:		C++ and compiled in VS2012
Platform:		Visual Studio 2012, Windows 8
Project:		CS560
Author:			Ilan Keshet
Creation date:	16/9/2014
- End Header --------------------------------------------------------*/

#pragma once

struct ChunkHeader
{
	unsigned int type;
	unsigned int size;
	unsigned int startPos;
	unsigned int endPos; //endPos is not set correctly in ChrisPeters Bin file
};

class BinFileDeseralizer
{
public:
	BinFileDeseralizer(string file);

	ChunkHeader ReadChunkHeader();

	ChunkHeader PeekChunkHeader();

	//This function does not work correctly, as endPos is always wrong
	void SkipChunk(ChunkHeader &ch);

	~BinFileDeseralizer();

	template<typename T>
	void Read(T& data)
	{
		binFile.read((char*)&data, sizeof(T));
	}

	template<typename T>
	void ReadBytes(T* data, unsigned int numberOfBytes)
	{
		binFile.read((char*)data, numberOfBytes);
	}

	void Read(string& str)
	{
		unsigned char size = 0;
		Read(size);

		str.resize(size);
		ReadBytes(&str[0], size);
	}

	template<typename VertexType>
	void ReadVertices(VertexType* data, unsigned int numVertices)
	{
		unsigned int vertexSize = sizeof(VertexType) - sizeof(glm::vec3);

		for(unsigned int i = 0; i < numVertices; ++i, ++data)
		{
			ReadBytes(data, vertexSize);
		}
	}

	template<typename VertexType>
	void ComputeTangents(VertexType* data, unsigned int numVertices, unsigned int* indices, unsigned int numIndices)
	{
		int size = sizeof(VertexType);

		for(unsigned int i = 0; i < numIndices; i += 3)
		{
			VertexType& Vv0 = data[indices[i]];
			VertexType& Vv1 = data[indices[i + 1]];
			VertexType& Vv2 = data[indices[i + 2]];

			glm::vec3 &pv0 = Vv0.pos;
			glm::vec3 &pv1 = Vv1.pos;
			glm::vec3 &pv2 = Vv2.pos;

			glm::vec3 &nv0 = Vv0.norm;
			glm::vec3 &nv1 = Vv1.norm;
			glm::vec3 &nv2 = Vv2.norm;

			glm::vec2 &tv0 = Vv0.tex;
			glm::vec2 &tv1 = Vv1.tex;
			glm::vec2 &tv2 = Vv2.tex;

			glm::vec3 deltaPos1 = pv1 - pv0;
			glm::vec3 deltaPos2 = pv2 - pv0;

			glm::vec2 deltaUV1 = tv1 - tv0;
			glm::vec2 deltaUV2 = tv2 - tv0;

			float denom = deltaUV1.x * deltaUV2.y - deltaUV2.x * deltaUV1.y;

			if(denom != 0)
			{
				float r = 1.0f / (deltaUV1.x * deltaUV2.y - deltaUV2.x * deltaUV1.y);

				glm::vec3 Tangent = (deltaUV2.y * deltaPos1 - deltaUV1.y * deltaPos2) * r;
			
				Vv0.tangent += Tangent;
				Vv1.tangent += Tangent;
				Vv2.tangent += Tangent;
			}
			else
			{
				Vv0.tangent += glm::vec3(1);
				Vv1.tangent += glm::vec3(1);
				Vv2.tangent += glm::vec3(1);
			}
		}

		for(unsigned int i = 0; i < numVertices; ++i)
		{
			VertexType& Vv = data[i];

			Vv.tangent = glm::normalize(Vv.tangent);
		}
	}

private:
	ifstream binFile;
};