/* Start Header -------------------------------------------------------
Copyright (C) 2014 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written consent of DigiPen Institute of Technology is prohibited.
File Name:		TextSerialization.cpp
Purpose:		Implementation of Text Serialization
Language:		C++ and compiled in VS2010
Platform:		Visual Studio 2010, Windows 8
Project:		CS529_aloksawant_4
Author:			Alok Sawant
Creation date:	2013/10/30
- End Header --------------------------------------------------------*/

#include "TextSerialization.h"
#include <assert.h>
#include <fstream>
#include <algorithm>
#include <functional>

////////////////////////////////////////////////////

void TextSerializer::TrimSpace(std::string & str)
{
	// left trim
	str.erase(str.begin(), find_if(str.begin(), str.end(), std::not1(std::ptr_fun<int, int>(isspace))));

	// right trim
	str.erase(find_if(str.rbegin(), str.rend(), std::not1(std::ptr_fun<int, int>(isspace))).base(), str.end());
}

void TextSerializer::ToLower(std::string & str)
{
	transform(str.begin(), str.end(), str.begin(), ::tolower);
}

////////////////////////////////////////////////////

class SettingsBlock
{
public:

	std::string				m_name;
	std::string				m_nameLwr;
	std::vector<std::string>		m_headers;
	std::vector<std::string>		m_values;

	SettingsBlock * Clone()
	{
		SettingsBlock * block = new SettingsBlock();
		*block = *this;
		return block;
	}

	void Merge(SettingsBlock * block)
	{
		for(auto it1 = block->m_headers.begin(); it1 != block->m_headers.end(); ++it1)
		{
			bool merged = false;
			for(auto it2 = m_headers.begin(); it2 != m_headers.end(); ++it2)
			{
				if(*it1 == *it2)
				{
					int index1 = it1 - block->m_headers.begin();
					int index2 = it2 - m_headers.begin();
					m_values[index2] = block->m_values[index1];
					merged = true;
					break;
				}
			}
			if(!merged)
			{
				int index1 = it1 - block->m_headers.begin();
				m_headers.push_back(*it1);
				m_values.push_back(block->m_values[index1]);
			}
		}
	}

	std::string GetString(const std::string & setting, std::string const & default = "")
	{
		for(auto it = m_headers.begin(); it != m_headers.end(); ++it)
		{
			if(!setting.compare(*it))
			{
				int index = it - m_headers.begin();
				return m_values[index];
			}
		}

		return default;
	}

	int GetInt(const std::string & setting, int default)
	{
		std::string str = GetString(setting);
		if(!str.empty())
		{
			return atoi(str.c_str());
		}

		return default;
	}

	float GetFloat(const std::string & setting, float default)
	{
		std::string str = GetString(setting);
		if(!str.empty())
		{
			return (float)atof(str.c_str());
		}

		return default;
	}

	static void SplitString(const std::string & str, std::vector<std::string> & words, char token)
	{
		if(!str.empty())
		{
			int start = 0;
			for(int i = 1; i < (int)str.length(); ++i)
			{
				if(str[i] == token)
				{
					std::string strToken = str.substr(start, i - start);
					TextSerializer::TrimSpace(strToken);
					words.push_back(strToken);
					start = i + 1;
				}
			}
			std::string strToken = str.substr(start, str.length() - start);
			TextSerializer::TrimSpace(strToken);
			words.push_back(strToken);
		}
	}

	bool GetWordList(const std::string & setting, std::vector<std::string> & words, char token)
	{
		std::string str = GetString(setting);
		if(!str.empty())
		{
			SplitString(str, words, token);
			return true;
		}
		return false;
	}

	static void GetOverriddenString(const std::string & strOriginal, std::string & strHeader, std::string & strOverridden)
	{
		if(!strOriginal.empty())
		{
			int start = 0;
			int end = 0;
			for(int i = 0; i < (int)strOriginal.length(); ++i)
			{
				if(strOriginal[i] == '(')
					start = i;
				else if(strOriginal[i] == ')')
					end = i - 1;
			}
			if(start > 0)
				strHeader = strOriginal.substr(0, start);
			if(start < end)
				strOverridden = strOriginal.substr(start + 1, end - start);

			TextSerializer::TrimSpace(strHeader);
			TextSerializer::TrimSpace(strOverridden);
		}
	}

	////////////////////////////////////////////////////

	void SetString(const std::string & setting, std::string const & str)
	{
		for(auto it = m_headers.begin(); it != m_headers.end(); ++it)
		{
			if(!setting.compare(*it))
			{
				int index = it - m_headers.begin();
				m_values[index] = str;
				return;
			}
		}

		// Not present, add header and value
		m_headers.push_back(setting);
		m_values.push_back(str);
	}

	void SetInt(const std::string & setting, int value)
	{
		std::string str = std::to_string((long long)value);
		SetString(setting, str);
	}

	void SetFloat(const std::string & setting, float value)
	{
		std::string str = std::to_string((long double)value);
		SetString(setting, str);
	}

	bool SetWordList(const std::string & setting, std::vector<std::string> const & words, char token)
	{
		if(words.size() > 0)
		{
			char separator[] = {token, ' ', 0};
			std::string str = words[0];
			for(int i = 1; i < (int)words.size(); ++i)
			{
				str += separator;
				str += words[i];
			}

			SetString(setting, str);
			return true;
		}
		return false;
	}
};

////////////////////////////////////////////////////

TextSerializer::TextSerializer() :
	m_settingBlocks()
{
}

TextSerializer::~TextSerializer()
{
	if(GetBlockCount() > 0)
	{
		assert(false);
	}
}

bool TextSerializer::OpenFile(std::string const & filename)
{
	char *				strFile = NULL;
	bool				bIgnoreLine = false;
	bool				bFoundBlock = false;
	bool				bFoundBlockName = false;
	SettingsBlock *	block = NULL;

	m_filename = filename;
	std::ifstream file(filename.c_str());
	if(file.is_open())
	{
		int begin = (int)file.tellg();
		file.seekg(0, std::ios::end);
		int end = (int)file.tellg();
		file.seekg(0);

		int size = end - begin;

		if(size > 0)
		{
			strFile = new char[end - begin];

			int i = 0;
			int blockIndex = 0;
			int lineStart = 0;
			int equalPos = 0;
			int lineEnd = 0;

			bool eof = false;
			while(!eof)
			{
				char ch;
				if(!file.eof())
				{
					ch = file.get();
				}
				else
				{
					--i;
					ch = '\n';
					eof = true;
				}

				switch(ch)
				{
					case ';':
					{
						// Ignore comment
						bIgnoreLine = true;
					}
					break;

					case '\n':
					{
						bFoundBlockName = false;
						bIgnoreLine = false;
						if(lineStart < equalPos && equalPos < i && block)
						{
							lineEnd = i;

							// Save line header
							int size = equalPos - lineStart + 1;
							char * temp = new char[size];
							memcpy(temp, strFile + lineStart, size - 1);
							temp[size - 1] = '\0';
							std::string header = temp;
							TextSerializer::TrimSpace(header);
							TextSerializer::ToLower(header);
							delete temp;

							// Save line value
							size = lineEnd - equalPos + 1;
							temp = new char[size];
							memcpy(temp, strFile + equalPos, size - 1);
							temp[size - 1] = '\0';
							std::string value = temp;
							TextSerializer::TrimSpace(value);
							//TextSerializer::ToLower(value);
							delete temp;

							block->m_headers.push_back(header);
							block->m_values.push_back(value);
						}

						lineStart = i;
						equalPos = i;
						lineEnd = i;
					}
					break;

					case '=':
					{
						if(lineStart < i && lineStart == equalPos) // 2nd = can be used in value
						{
							equalPos = i;
						}
					}
					break;

					case '[':
					{
						bFoundBlockName = true;
						blockIndex = i;
					}
					break;

					case ']':
					{
						if(bFoundBlockName)
						{
							bFoundBlockName = false;
							std::string str;
							int size = i - blockIndex + 1;
							char * temp = new char[size];
							memcpy(temp, strFile + blockIndex, size - 1);
							temp[size - 1] = '\0';

							if(block)
							{
								m_settingBlocks.push_back(block);
							}

							block = new SettingsBlock();
							block->m_name = temp;
							block->m_nameLwr = temp;
							TextSerializer::ToLower(block->m_nameLwr);
							delete temp;
						}
					}
					break;

					default:
					{
						if(!bIgnoreLine)
						{
							strFile[i] = ch;
							++i;
						}
					}
					break;
				}
			}
			if(block) // add last block
			{
				m_settingBlocks.push_back(block);
			}

			file.close();
			delete[] strFile;
		}

		return true;
	}

	return false;
}

void TextSerializer::CloseFile()
{
	for(auto it = m_settingBlocks.begin(); it != m_settingBlocks.end(); ++it)
		delete *it;
	m_settingBlocks.clear();
}

int TextSerializer::GetBlockCount()
{
	return m_settingBlocks.size();
}

int TextSerializer::GetBlockCount(int blockIndex)
{
	assert(blockIndex >= 0 && (unsigned)blockIndex < m_settingBlocks.size());

	return m_settingBlocks[blockIndex]->m_headers.size();
}

void TextSerializer::Merge(TextSerializer const & other)
{
	for(auto itOther = other.m_settingBlocks.begin(); itOther != other.m_settingBlocks.end(); ++itOther)
	{
		bool merged = false;
		for(auto itThis = m_settingBlocks.begin(); itThis != m_settingBlocks.end(); ++itThis)
		{
			if((*itThis)->m_name == (*itOther)->m_name)
			{
				// Merge block
				(*itThis)->Merge(*itOther);
				merged = true;
				break;
			}
		}
		if(!merged)
		{
			// Add block
			SettingsBlock * block = (*itOther)->Clone();
			m_settingBlocks.push_back(block);
		}
	}
}

std::string TextSerializer::GetBlockName(int blockIndex)
{
	return m_settingBlocks[blockIndex]->m_name;
}

int TextSerializer::GetBlockId(std::string const & blockname, bool createIfNotFound)
{
	int i = 0;
	for(auto it = m_settingBlocks.begin(); it != m_settingBlocks.end(); ++it, ++i)
	{
		if((*it)->m_name == blockname)
			return i;
	}
	if(createIfNotFound)
	{
		SettingsBlock * block = new SettingsBlock();
		block->m_name = blockname;
		block->m_nameLwr = blockname;
		TextSerializer::ToLower(block->m_nameLwr);
		m_settingBlocks.push_back(block);
		return m_settingBlocks.size() - 1;
	}
	return -1;
}

std::string TextSerializer::GetString(int blockIndex, std::string const & setting, std::string const & default)
{
	if(blockIndex >= 0 && blockIndex < GetBlockCount())
		return m_settingBlocks[blockIndex]->GetString(setting, default);
	else
		return "";
}

bool TextSerializer::GetBool(int blockIndex, std::string const & setting, bool default)
{
	if(blockIndex >= 0 && blockIndex < GetBlockCount())
	{
		return m_settingBlocks[blockIndex]->GetInt(setting, default) > 0;
	}
	else
		return default;
}

int TextSerializer::GetInt(int blockIndex, std::string const & setting, int default)
{
	if(blockIndex >= 0 && blockIndex < GetBlockCount())
		return m_settingBlocks[blockIndex]->GetInt(setting, default);
	else
		return default;
}

float TextSerializer::GetFloat(int blockIndex, std::string const & setting, float default)
{
	if(blockIndex >= 0 && blockIndex < GetBlockCount())
		return m_settingBlocks[blockIndex]->GetFloat(setting, default);
	else
		return default;
}

bool TextSerializer::GetWordList(int blockIndex, std::string const & setting, std::vector<std::string> & words, char token)
{
	if(blockIndex >= 0 && blockIndex < GetBlockCount())
		return m_settingBlocks[blockIndex]->GetWordList(setting, words, token);
	else
		return false;
}

void TextSerializer::GetSettings(int blockIndex, std::vector<std::string> & words)
{
	assert(blockIndex >= 0 && blockIndex < GetBlockCount());


}

/*static*/ void TextSerializer::GetOverriddenString(std::string const & strOriginal, std::string & strHeader, std::vector<std::string> & aOverriddens, char token)
{
	std::string str;
	SettingsBlock::GetOverriddenString(strOriginal, strHeader, str);
	SettingsBlock::SplitString(str, aOverriddens, token);
}

////////////////////////////////////////////////////

void TextSerializer::SetString(int blockIndex, std::string const & setting, std::string const & value)
{
	if(blockIndex >= 0 && blockIndex < GetBlockCount())
		m_settingBlocks[blockIndex]->SetString(setting, value);
}

void TextSerializer::SetInt(int blockIndex, std::string const & setting, int value)
{
	if(blockIndex >= 0 && blockIndex < GetBlockCount())
		m_settingBlocks[blockIndex]->SetInt(setting, value);
}

void TextSerializer::SetFloat(int blockIndex, std::string const & setting, float value)
{
	if(blockIndex >= 0 && blockIndex < GetBlockCount())
		m_settingBlocks[blockIndex]->SetFloat(setting, value);
}

bool TextSerializer::SetWordList(int blockIndex, std::string const & setting, std::vector<std::string> const & words, char token)
{
	if(blockIndex >= 0 && blockIndex < GetBlockCount())
		return m_settingBlocks[blockIndex]->SetWordList(setting, words, token);
	else
		return false;
}

void TextSerializer::SaveFile()
{
	std::ofstream file(m_filename.c_str());
	if(file.is_open())
	{
		for(auto it = m_settingBlocks.begin(); it != m_settingBlocks.end(); ++it)
		{
			SettingsBlock * block = *it;
			file << "[" << block->m_name << "]\n";
			for(int i = 0; i < (int)block->m_headers.size(); ++i)
			{
				file << block->m_headers[i] << " = " << block->m_values[i] << "\n";
			}

			file << "\n";
		}

		file.close();
	}
}
