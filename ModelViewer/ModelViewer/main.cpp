#include "Precompiled.h"
#include "GraphicsSystem.h"
#include "WindowsSystem.h"
#include "InputSystem.h"

#include "ModelViewerCore.h"

//Screen dimension constants
const char WindowTitle[] = "Demo";
const int SCREEN_WIDTH = 840;
const int SCREEN_HEIGHT = 680;

int main(int argc, char** args){
	string bin;
	string tex;

	if(argc > 1)
	{
		bin = args[1];
	}
	else
	{
		bin = "Tad.bin";
	}

	if(argc > 2)
	{
		tex = args[2];
	}
	else
	{
		tex = "Tad.png";
	}

	ModelViewerCore core;

	WindowsSystem *demo = new WindowsSystem(WindowTitle,SCREEN_WIDTH,SCREEN_HEIGHT);
	InputSystem* input = new InputSystem;

	GraphicsSystem *gra = new GraphicsSystem;
	gra->SetBinFile(bin);
	gra->SetTexFile(tex);

	core.AddSystem(demo);
	core.AddSystem(input);
	core.AddSystem(gra);

	core.Initialize();

	core.GameLoop();

	core.DestroySystems();

	return 0;
}