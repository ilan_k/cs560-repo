/* Start Header -------------------------------------------------------
Copyright (C) 2014 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written consent of DigiPen Institute of Technology is prohibited.
File Name:		CatmullRomCurve.h
Purpose:		Class for creating a curve that can be managed by arc length
Language:		C++ and compiled in VS2012
Platform:		Visual Studio 2012, Windows 8
Project:		CS560
Author:			Ilan Keshet
Creation date:	11/13/2014
- End Header --------------------------------------------------------*/


#pragma once

#include "Curve.h"

#include <list>

//a particular curve segment on a curve
struct CurveSegment
{
	CurveSegment(float u_a, float b) : u_a(u_a), u_b(b) {}

	float u_a;
	float u_b;
};

//Assumes that map is bounded from 0 to 1
//Returns back an interpolated value between an upper and lower
//bound of the passed in key
float BinarySearch(std::map<float, float> const &map, float key);

class CatmullRomCurve : public Curve{
public:
	CatmullRomCurve(LineDrawer* lDrawer);

	//Draws the curve with given level of detail
	virtual void draw(int levelOfDetail = 10);

	//Evaluates a point on the curve, the passed in u should be between 0 to 1
	Point evaluate(float u);

	//Pass in distance travelled from last time step
	//Returns back the percent of speed from max speed that
	//the object is traveling
	float UpdatePosition(float maxSpeed, float dt);

	//Returns the current U parameter -- which is guaranteed to be 
	//between 0 to 1. 
	float GetCurrentU() { return currentU; }

	//Gets the total arc length of the curve
	float GetCurveLength() { return L; }

	//Restarts the position along the path
	void Restart() 
	{ 
		currentU = 0.0f; 
		t = 0.0f; 
	}

	//Updates the internal arc length tables
	//by using the Adaptive Approach as discussed in class
	//This function is called whenever a new point is added to the curve
	virtual void recalculate();

private:
	float getCatmullRomX(float t, Point const &P0, Point const &P1, Point const &P2, Point const &P3);
	float getCatmullRomY(float t, Point const &P0, Point const &P1, Point const &P2, Point const &P3);
	float getCatmullRomZ(float t, Point const &P0, Point const &P1, Point const &P2, Point const &P3);

	Point computeTension(Point const & pip1, Point const & p1m1);

	std::vector<Point> interpolatedCurve;

	float currentU; //[0...1]
	float t;        //Denotes total time in seconds traveling along path

	float L;		//Length of path

	std::map<float, float> uToS;	//Table to go from u to Arc Length
	std::map<float, float> sToU;	//Table to get u from Inverse Arc Length
};