#include "Precompiled.h"
#include "LineDrawer.h"
#include "GLSLshader.h"

GLfloat debugLine[] = {
	0.0f,0.0f,0.0f, 1.0f, 0.0f, 0.0f,
	5.0f,0.0f,0.0f, 1.0f, 0.0f, 0.0f
};

LineDrawer::~LineDrawer()
{
	glDeleteBuffers(1, &dbo);
	glDeleteVertexArrays(1,&dao);
}

void LineDrawer::Initialize(glm::mat4 const &proj)
{
	debugShader.createShaderProgram("Shaders\\DebugShader\\DebugvertexShader.glsl","Shaders\\DebugShader\\DebugfragmentShader.glsl");
	debugShader.Bind();

	debugShader.addUniform("debugModel");
	debugShader.addUniform("debugView");
	debugShader.addUniform("debugProj");

	glGenVertexArrays(1,&dao);
	glBindVertexArray(dao);

	glGenBuffers(1, &dbo);
	glBindBuffer(GL_ARRAY_BUFFER,dbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(debugLine), debugLine, GL_DYNAMIC_DRAW);

	glUniformMatrix4fv(debugShader("debugProj"), 1, GL_FALSE, glm::value_ptr(proj));

	GLint posAttrib = glGetAttribLocation(debugShader.getShaderProgram(), "position");
	glEnableVertexAttribArray(posAttrib);
	glVertexAttribPointer(posAttrib, 3, GL_FLOAT, GL_FALSE,   6*sizeof(GLfloat), 0);

	GLint colorAttrib = glGetAttribLocation(debugShader.getShaderProgram(), "color");
	glEnableVertexAttribArray(colorAttrib);
	glVertexAttribPointer(colorAttrib, 3, GL_FLOAT, GL_FALSE, 6*sizeof(GLfloat), (void*)(3*sizeof(GLfloat)));
}

void LineDrawer::Bind(glm::mat4 const& modelMatrix, glm::mat4 const& viewMatrix)
{
	debugShader.Bind();

	glUniformMatrix4fv(debugShader("debugModel"), 1, GL_FALSE, glm::value_ptr(modelMatrix));
	glUniformMatrix4fv(debugShader("debugView"), 1, GL_FALSE, glm::value_ptr(viewMatrix));
}

void LineDrawer::unBind()
{
	debugShader.unBind();
}

void LineDrawer::DrawLineWithPoints(glm::vec3 const &p1, glm::vec3 const &p2, glm::vec3 const &color)
{
	DrawLine(p1, p2, color);

	GLfloat* data;
	data = (GLfloat*) glMapBuffer(GL_ARRAY_BUFFER, GL_READ_WRITE);

	glPointSize(8.0);

	data[3] = 0.0f;
	data[4] = 0.5f;
	data[5] = 0.5f;

	data[9] = 0.0f;
	data[10] = 0.5f;
	data[11] = 0.5f;

	glUnmapBuffer(GL_ARRAY_BUFFER);

	glDrawArrays(GL_POINTS,0,2);
}

void LineDrawer::DrawLine(glm::vec3 const &p1, glm::vec3 const &p2, glm::vec3 const &color)
{
	glBindVertexArray(dao);
	glBindBuffer(GL_ARRAY_BUFFER, dbo);

	GLfloat* data;
	data = (GLfloat*) glMapBuffer(GL_ARRAY_BUFFER, GL_READ_WRITE);

	data[0] = p1.x;
	data[1] = p1.y;
	data[2] = p1.z;
	data[3] = color[0];
	data[4] = color[1];
	data[5] = color[2];

	data[6] = p2.x;
	data[7] = p2.y;
	data[8] = p2.z;
	data[9] = color[0];
	data[10] = color[1];
	data[11] = color[2];
	
	glUnmapBuffer(GL_ARRAY_BUFFER);

	glDrawArrays(GL_LINES,0,2);
}