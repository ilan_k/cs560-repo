/* Start Header -------------------------------------------------------
Copyright (C) 2014 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written consent of DigiPen Institute of Technology is prohibited.
File Name:		GraphicsSystem.h
Purpose:		Graphics System for Drawing the the model
				It also does all the heavy work under the hood as well
Language:		C++ and compiled in VS2012
Platform:		Visual Studio 2012, Windows 8
Project:		CS560
Author:			Ilan Keshet
Creation date:	16/9/2014
- End Header --------------------------------------------------------*/

#pragma once

#include "System.h"
#include "Texture.h"
#include "GLSLshader.h"
#include "LevelEditor.h"
#include "TextSerialization.h"
#include "LineDrawer.h"
#include "CatmullRomCurve.h"

class Model;
struct Joint;
struct JointPose;

struct Transformation
{
	Transformation() : scale(1.0f, 1.0f, 1.0f), translate(1.0f, 1.0f, 1.0f), rotation(0) {}

	glm::vec3 scale;
	glm::vec3 translate;
	float	  rotation;
};

struct IKData
{
	IKData();

	void Restart();

	vector<Joint*>					jointChain;
	std::map<int, JointPose>		jointChainPosesMap;

	float blend;
};

class GraphicsSystem : public System
{
public:
	GraphicsSystem();
	~GraphicsSystem();
	void							Initialize();
	void							Update(float dt);
	string						    GetName() { return "Graphics"; }

	void							LoadBinModel(string filepath, Model *&modelToLoad);
	void							LoadTexture(string textureFile, Texture *&tex);

	void                            SendMsg(Message * mess);

	void							SetBinFile(string binFile) { this->binFile = binFile; }
	void							SetTexFile(string textureFile) { this->textureFile = textureFile; }
private:
	string							binFile;
	string							textureFile;
	LevelEditor						editor;

	Transformation					modelTransform;
	Transformation					goalTransform;
	Transformation					planeTransform;

	glm::mat4						modelMatrix;
	glm::mat4						viewMatrix;
	glm::mat4                       proj;

	glm::vec3						GetModelSpacePosition(glm::mat4 jointMatrix);

	void                            SetStaticVertexAttributes(GLuint, unsigned int size);
	void                            SetSkinnedVertexAttributes(GLuint, unsigned int size);

	void							LoadMultipleAnimation(int blockid, TextSerializer &ts);
	void                            InitializeDebugShader();
	void                            InitializeStaticShader();
	void                            InitializeSkinnedShader();

	void							SetBinConfigurations(string binFile, Transformation &modelTransform, Model *& model, Texture *&tex);

	void							ComputeIKChain();
	void							UpdateModel(float dt);
	void							UpdateModelMatrix(Transformation const& modelTransform);

	void                            RenderModel(Model* model, Texture *tex, bool showModel);
	void							RenderSkeleton(Model *model);

	float							zoom;
	float							eyeXRotation;
	float							eyeYRotation;
	float							sceneCenterX;
	float							sceneCenterY;

	Texture                         *modelTexture;
	Texture							*goalTexture;

	int                             mouseX;
	int                             mouseY;
	int                             mouseButton;
	bool                            mouseButtonDown;

	bool							ikEnabled;
	bool							initiateIk;
	IKData							ikData;

	Model*							model;
	Model*							goal;

	GLSLshader                      staticShader;
	GLSLshader                      skinnedShader;

	LineDrawer						lDrawer;
	CatmullRomCurve					path;

	bool							endReached;
};

