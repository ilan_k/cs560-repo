#pragma once

#include "GLSLshader.h"

class LineDrawer
{
public:
	LineDrawer(){};

	~LineDrawer();

	void Initialize(glm::mat4 const &proj);

	void DrawLine(glm::vec3 const &p1, glm::vec3 const &p2, glm::vec3 const &color = glm::vec3(0.0, 1.0, 0.0));

	void DrawLineWithPoints(glm::vec3 const &p1, glm::vec3 const &p2, glm::vec3 const &color = glm::vec3(0.0, 1.0, 0.0));

	void Bind(glm::mat4 const& modelMatrix, glm::mat4 const& viewMatrix);

	void unBind();

private:
	GLSLshader debugShader;
	GLuint dao, dbo;
};