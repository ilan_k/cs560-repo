/* Start Header -------------------------------------------------------
Copyright (C) 2014 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written consent of DigiPen Institute of Technology is prohibited.
File Name:		Animation.h
Purpose:		Class for Running animations of a model -- can choose which animation to use
Language:		C++ and compiled in VS2012
Platform:		Visual Studio 2012, Windows 8
Project:		CS560
Author:			Ilan Keshet
Creation date:	16/9/2014
- End Header --------------------------------------------------------*/

#pragma once

#include "BinAsset.h"
#include "Math.h"

#define NEW_JOINTPOSE_SIZE sizeof(JointPose)

#define OLD_JOINTPOSE_SIZE sizeof(float) + sizeof(glm::vec3) + sizeof(glm::quat)

struct JointPose
{
	float time;
	glm::vec3 Translation;
	Quaternion Rotation;
	glm::vec3 Scale;
};

typedef vector<JointPose> JointPoses;

class Animation : public BinAsset
{
	float duration;
	vector< JointPoses > bonePoseSets;

	vector<int> currentJointPosesFrame;

public:
	friend class Model;

	//Resets the animation back to the beginning of the animation
	void Restart();

	//Reads in all the keyframe information from the binary file
	void Read(BinFileDeseralizer& file, bool oldFileVersion);

	void NormalizeTimeLine();

	//Updates the animation with given dt
	void Update(float dt);

	void Initialize();

	float GetDuration() { return duration; }

	float GetKeyFrameTime(int keyframe);

	JointPose const & GetCurrentJointPose(int jointIndex);

	JointPose const & GetPreviousJointPose(int jointIndex);

	JointPose const * GetNextJointPose(int jointIndex);

	unsigned int TotalJointPoseSets() const { return bonePoseSets.size(); }
};