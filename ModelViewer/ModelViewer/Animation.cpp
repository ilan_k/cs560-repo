/* Start Header -------------------------------------------------------
Copyright (C) 2014 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written consent of DigiPen Institute of Technology is prohibited.
File Name:		Animation.cpp
Purpose:		Class for Running animations of a model -- can choose which animation to use
Language:		C++ and compiled in VS2012
Platform:		Visual Studio 2012, Windows 8
Project:		CS560
Author:			Ilan Keshet
Creation date:	16/9/2014
- End Header --------------------------------------------------------*/

#include "Precompiled.h"
#include "Animation.h"

/******************************************************************************/
/*!
	Reads all the animation data from file.
	In my new Maya SDK format -- I specify Scales on all joints --
	so I have added that new information in
*/
/******************************************************************************/
void Animation::Read(BinFileDeseralizer& file, bool oldFileVersion)
{
	file.Read(duration);

	int numberOfBonePoseSets = 0;

	file.Read(numberOfBonePoseSets);

	bonePoseSets.resize(numberOfBonePoseSets);

	for(int i = 0; i < numberOfBonePoseSets; i++)
	{
		int numberOfPoses = 0;
		file.Read(numberOfPoses);

		bonePoseSets[i].resize(numberOfPoses);

		for(int j = 0; j < numberOfPoses; j++)
		{
			if(oldFileVersion)
			{
				file.ReadBytes(&(bonePoseSets[i][j]), OLD_JOINTPOSE_SIZE);
				bonePoseSets[i][j].Scale = glm::vec3(1.0, 1.0, 1.0);
			}
			else
			{
				file.ReadBytes(&(bonePoseSets[i][j]), NEW_JOINTPOSE_SIZE);
			}
		}
	}

	//Sets the currentJointPosesFrame to all 0's and has size of numberOfBonePoseSets
	currentJointPosesFrame.resize(numberOfBonePoseSets);
}

void Animation::NormalizeTimeLine()
{
	for(unsigned int i = 0; i < bonePoseSets.size(); ++i)
	{
		for(auto it = bonePoseSets[i].begin(); it != bonePoseSets[i].end(); ++it)
		{
			it->time /= duration;
		}
	}

	duration = 1.0f;
}

/******************************************************************************/
/*!
	Updates the animation
*/
/******************************************************************************/
void Animation::Update(float currentTime)
{
	for(unsigned int i = 0; i < bonePoseSets.size(); i++)
	{
		int& currentBoneFrame = currentJointPosesFrame[i];

		assert(currentBoneFrame >= 0 && currentBoneFrame <(int)bonePoseSets[i].size());

		JointPose& pose = bonePoseSets[i][currentBoneFrame];

		//The following two loops ensures that the currentBoneFrame is linked up 
		//to the passed in time
		while(currentBoneFrame < (int)bonePoseSets[i].size() - 1 && 
			bonePoseSets[i][currentBoneFrame + 1].time < currentTime)
		{
			currentBoneFrame++;
		}

		while(currentBoneFrame > 0 && 
			bonePoseSets[i][currentBoneFrame].time > currentTime)
		{
			currentBoneFrame--;
		}

		assert(currentBoneFrame >= 0 && currentBoneFrame < (int)bonePoseSets[i].size());
	}
}

/******************************************************************************/
/*!
	Restarts all the currentJointPose indices to 0 -- which
	sets the currentBoneFrame of all joints back to their beginning position
*/
/******************************************************************************/
void Animation::Restart()
{
	for(auto it = currentJointPosesFrame.begin(); it != currentJointPosesFrame.end(); ++it)
	{
		*it = 0;
	}
}

void Animation::Initialize()
{
	Restart();
}

/******************************************************************************/
/*!
	Gets the current JointPose of a given jointIndex
*/
/******************************************************************************/
JointPose const & Animation::GetCurrentJointPose(int jointIndex)
{
	assert(jointIndex >= 0 && jointIndex < (int)bonePoseSets.size()); 

	const int & currentJointPoseIndex = currentJointPosesFrame[jointIndex];

	assert(currentJointPoseIndex >= 0 && currentJointPoseIndex < (int)bonePoseSets[jointIndex].size()); 

	return bonePoseSets[jointIndex][currentJointPoseIndex];
}

/******************************************************************************/
/*!
	Gets the previous JointPose of a given jointIndex.

	If there is no previous -- it uses the last frame (assumes that the first and last link up)
*/
/******************************************************************************/
JointPose const & Animation::GetPreviousJointPose(int jointIndex)
{
	assert(jointIndex >= 0 && jointIndex < (int)bonePoseSets.size()); 

	const int & currentJointPoseIndex = currentJointPosesFrame[jointIndex];

	int previousJointPoseIndex = currentJointPoseIndex - 1;

	if(previousJointPoseIndex < 0)
	{
		previousJointPoseIndex = bonePoseSets[jointIndex].size() - 1;
	}

	assert(previousJointPoseIndex >= 0 && previousJointPoseIndex < (int)bonePoseSets[jointIndex].size()); 

	return bonePoseSets[jointIndex][previousJointPoseIndex];
}


/******************************************************************************/
/*!
	Gets the next JointPose of a given jointIndex.

	If there is no next -- it uses the first frame (assumes that the first and last link up)
*/
/******************************************************************************/
JointPose const * Animation::GetNextJointPose(int jointIndex)
{
	assert(jointIndex >= 0 && jointIndex < (int)bonePoseSets.size()); 

	const int & currentJointPoseIndex = currentJointPosesFrame[jointIndex];

	int nextJointPoseIndex = currentJointPoseIndex + 1;

	if(nextJointPoseIndex >= (int)bonePoseSets[jointIndex].size())
	{
		return 0;
	}

	assert(nextJointPoseIndex >= 0 && nextJointPoseIndex < (int)bonePoseSets[jointIndex].size()); 

	return &bonePoseSets[jointIndex][nextJointPoseIndex];
}

float Animation::GetKeyFrameTime(int keyframe)
{
	return bonePoseSets[0][keyframe].time;
}