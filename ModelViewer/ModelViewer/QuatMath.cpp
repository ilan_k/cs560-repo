/* Start Header -------------------------------------------------------
Copyright (C) 2014 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written consent of DigiPen Institute of Technology is prohibited.
File Name:		QuatMath.cpp
Purpose:		My Math library for Quaternions
Language:		C++ and compiled in VS2012
Platform:		Visual Studio 2012, Windows 8
Project:		CS560
Author:			Ilan Keshet
Creation date:	16/9/2014
- End Header --------------------------------------------------------*/

#include "Precompiled.h"
#include "QuatMath.h"

Quaternion::Quaternion(glm::vec4 const& data) : quat(0.0, 0.0f, 0.0f, 1.0f)
{
	float len = glm::length(data);

	if(len)
	{
		//Normalize quaternion
		quat = data / len;
	}
	//Ensure that the passed in data has length
	else
	{ 
		assert(false);
	}
}

Quaternion::Quaternion(float angle, glm::vec3 axis)
{
	float len = glm::length(axis);

	if(len)
	{
		axis = axis / len;

		quat[0] = axis[0] * sin(angle / 2);
		quat[1] = axis[1] * sin(angle / 2);
		quat[2] = axis[2] * sin(angle / 2);
		quat[3] = cos(angle / 2.0f);
	}
	else
	{
		assert(false);
	}
}

Quaternion Quaternion::operator*(Quaternion const & rhs)
{
	float Ps = this->quat[3];
	float Qs = rhs[3];

	glm::vec3 Pv = glm::vec3(this->quat[0], this->quat[1], this->quat[2]);
	glm::vec3 Qv = glm::vec3(rhs.quat[0], rhs.quat[1], rhs.quat[2]);

	float w = Ps * Qs - glm::dot(Pv, Qv);

	glm::vec3 xyz = Ps * Qv + Pv * Qs + glm::cross(Pv, Qv);

	return Quaternion(glm::vec4(xyz, w));
}

Quaternion Quaternion::conjugate()
{
	float x = -quat[0];
	float y = -quat[1];
	float z = -quat[2];
	float w = quat[3];

	return Quaternion(glm::vec4(x, y, z, w));
}

//Returns back the 4 individual components of the quaternion
glm::vec4 const & Quaternion::Get() const
{
	return quat;
}

//Converts the quaternion to a rotation matrix
glm::mat4 Quaternion::ToMat4() const
{
	float x = quat[0];
	float y = quat[1];
	float z = quat[2];
	float w = quat[3];

	return glm::mat4(1.0f - 2.0f * y * y - 2.0f * z * z, 2.0f * x * y + 2.0f * z * w, 2.0f * x * z - 2.0f * y * w, 0.0f,
		             2.0f * x * y - 2.0f * z * w, 1.0f - 2.0f * x * x - 2.0f * z * z, 2.0f * y * z + 2.0f * x * w, 0.0f,
					 2.0f * x * z + 2.0f * y * w, 2.0f * y * z - 2.0f * x * w, 1.0f - 2.0f * x * x - 2.0f * y * y, 0.0f,
					 0.0f, 0.0f, 0.0f, 1.0f);
}

float Quaternion::dotProduct(Quaternion const & q1, Quaternion const &q2)
{
	return q1[0] * q2[0] + q1[1] * q2[1] + q1[2] * q2[2] + q1[3] * q2[3];
}

Quaternion Slerp(Quaternion const &quat1, Quaternion const &quat2, float t)
{
	//Ensure t is between 0 to 1
	assert(t >= 0 && t <= 1);

	float alpha;
	float beta;

	//Grab the quaternion datas
	glm::vec4 const& p = quat1.Get();
	glm::vec4 const& q = quat2.Get();

	//ensure pDotQ is between -1 to 1 to inverse cosine fails
	float pDotq = glm::clamp(Quaternion::dotProduct(quat1, quat2), -1.0f, 1.0f); 
	bool flip = false;

	//Since Quaternions have 2x degrees of rotation
	//we only want 0 - 1 range
	if(pDotq < 0)
	{
		flip = true;
		pDotq = -pDotq;
	}

	//linearly interpolate when cos(theta) is very close to 1
	if(1.0 - pDotq < 0.000001)
	{
		alpha = 1.0f - t;
		beta = t;
	}
	else
	{
		float theta = acos(pDotq);
		float sin_t = sin(theta);

		alpha = (float)(sin((1.0f - t) * theta) / sin_t);
		beta = (float)(sin(t * theta) / sin_t);
	}

	if(flip)
	{
		beta = -beta;
	}

	glm::vec4 interpolatedQuat;
	interpolatedQuat[0] = alpha * p[0] + beta * q[0];
	interpolatedQuat[1] = alpha * p[1] + beta * q[1];
	interpolatedQuat[2] = alpha * p[2] + beta * q[2];
	interpolatedQuat[3] = alpha * p[3] + beta * q[3];

	return Quaternion(interpolatedQuat);
}