/* Start Header -------------------------------------------------------
Copyright (C) 2014 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written consent of DigiPen Institute of Technology is prohibited.
File Name:		GLSLshader class for creating / binding / using shaders
Purpose:		Gives some functionality of File / Folders
Language:		C++ and compiled in VS2012
Platform:		Visual Studio 2012, Windows 8
Project:		CS560
Author:			Ilan Keshet
Creation date:	16/9/2014
- End Header --------------------------------------------------------*/

#pragma once

class GLSLshader
{
public:
								   GLSLshader(void);
								   ~GLSLshader(void);
	void				           createShaderProgram(const string& ,const string& ,const string&);
	void				           createShaderProgram(const string& ,const string&);
	void				           addAttribute(const string&);
	void				           addUniform(const string&);
	void				           BindFragData(const string&);
	void				           Bind();
	void				           unBind();
	GLuint				           operator[](const string&) const; //Attribute
	GLuint				           operator()(const string&) const; //Uniform
	GLuint                         getShaderProgram();
	void						   ShaderCompileErrorLog(GLuint shader);
private:						   
	GLint                          shaderStatus;
	GLuint                         shaderProgram;
	GLuint                         shaders[3];
	string						   readShaderFile(const string&);
	map<string, GLuint>			   attributeMap;
	map<string, GLuint>			   uniformMap;
};