/* Start Header -------------------------------------------------------
Copyright (C) 2014 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written consent of DigiPen Institute of Technology is prohibited.
File Name:		FileUtil.h
Purpose:		Gives some functionality of File / Folders
Language:		C++ and compiled in VS2012
Platform:		Visual Studio 2012, Windows 8
Project:		CS560
Author:			Ilan Keshet
Creation date:	16/9/2014
- End Header --------------------------------------------------------*/

#pragma once

string GetFilenameFromPath(string path);
			
string GetFilenameWithExtensionFromPath(string path);
			
string GetFileExtension(string path);

bool FileExists(string path);

void GetFilesInDirectory(vector<string> &out, const string &directory);

void GetDirectoriesInDirectory(vector<string> &out, const string &directory);