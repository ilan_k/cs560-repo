#pragma once

#include <glm.hpp>
#include "QuatMath.h"

inline glm::mat4 BuildTransform(glm::vec3 const &t, Quaternion const &q, glm::vec3 const& s)
{
	glm::mat4 rotation = q.ToMat4();

	glm::mat4 scale(1);
	scale[0][0] = s.x;
	scale[1][1] = s.y;
	scale[2][2] = s.z;

	glm::mat4 translation(1);
	translation[3][0] = t.x;
	translation[3][1] = t.y;
	translation[3][2] = t.z;

	return translation * rotation * scale;
}