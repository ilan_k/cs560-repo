#pragma once

#include "BinFileDeseralizer.h"

class BinAsset
{
public:
	virtual void Read(BinFileDeseralizer &file, bool oldFileVersion) = 0;
	virtual void Initialize(){}
};