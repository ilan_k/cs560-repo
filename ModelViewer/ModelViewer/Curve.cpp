#include "Precompiled.h"
#include "Curve.h"

Pvector Curve::points;

void Curve::draw(int levelOfDetail) {
	if (!points.empty()) {
		connectTheDots(glm::vec3(1.0, 0.0, 0.0));
	}
}

void Curve::connectTheDots(glm::vec3 const &color) {
	Pvector::iterator it, next;

	if (points.size() > 1) {
		for (it = points.begin(); it != points.end()-1; it++) {
			next = it+1;

			lDrawer->DrawLineWithPoints(*it, *next, color);
		}
	}
}

void Curve::addPoint(glm::vec3 const &p)
{
	points.push_back(p);
}

void Curve::addPointAndCalc(glm::vec3 const &p)
{
	points.push_back(p);

	recalculate();
}

void Curve::clearAllPoints(){
	this->points.clear();
}

void Curve::reserve(int size)
{
	points.reserve(size);
}
