#pragma once //Makes sure this header is only included once

namespace Mid // Message id
{
	enum MessageIdType
	{
		Resize,
		Quit,
		DropFile,
		MouseClick,
		MouseUp,
		MouseMotion,
		MouseWheel,
		TabPressed,
		RestartAnimation,
		EndReached
	};
}

///Base message class. New message types are defined by deriving from this
///class and mapping defined an element of the MessageIdType enumeration.
class  Message
{
public:
	Message(Mid::MessageIdType id) : MessageId(id){}
	Mid::MessageIdType MessageId;
	virtual ~Message(){}
};

///Message to tell the game to quit
class  MessageQuit : public Message
{
public:
	MessageQuit() : Message(Mid::Quit) {}
};

class MessageResize : public Message{
public:
	MessageResize() : Message(Mid::Resize){}
};

class MessageDropFile : public Message{
public:
	string filepath;

	MessageDropFile(string filepath) : Message(Mid::DropFile), filepath(filepath){}
};

class MessageMouseClick : public Message{
public:
	int x;
	int y;
	int mouseButton;

	MessageMouseClick(int xCoor, int yCoor, int mb) : Message(Mid::MouseClick), x(xCoor), y(yCoor), mouseButton(mb){}
};

class MessageMouseUp : public Message{
public:
	int mouseButton;

	MessageMouseUp(int mb) : Message(Mid::MouseUp), mouseButton(mb) {}
};

class MessageMouseMotion : public Message{
public:
	int x;
	int y;

	MessageMouseMotion(int xCoor, int yCoor) :Message(Mid::MouseMotion), x(xCoor), y(yCoor){}
};

class MessageMouseWheel : public Message{
public:
	int xScroll;
	int yScroll;

	MessageMouseWheel(int xScroll, int yScroll) : Message(Mid::MouseWheel), xScroll(xScroll), yScroll(yScroll){}
};

class MessageTabPressed : public Message
{
public:

	MessageTabPressed() : Message(Mid::TabPressed) {}
};

class MessageRestartAnimation : public Message
{
public:
	MessageRestartAnimation() : Message(Mid::RestartAnimation) {}
};

class MessageEndReached : public Message
{
public:
	MessageEndReached() : Message(Mid::EndReached) {}
};
