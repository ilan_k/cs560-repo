#version 330
in vec3 OutputNormal;
in vec3 OutputTangent;
in vec3 OutputTex;

out vec4 outColor;

uniform sampler2D tex;

uniform mat4 normalMatrix;

void main(){
  vec3 texColor = texture(tex, OutputTex.xy).rgb;

  vec4 normal = normalMatrix * vec4(OutputNormal, 1.0);
  normal = vec4(normalize(normal.xyz), 1.0);

  outColor = vec4(texColor, 1.0);
}