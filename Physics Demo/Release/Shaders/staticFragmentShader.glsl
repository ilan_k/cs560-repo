#version 330
in vec3 OutputNormal;
in vec3 OutputTangent;
in vec3 OutputTex;

out vec4 outColor;

uniform sampler2D tex;

uniform mat4 normalMatrix;


void main(){
	vec3 color = texture(tex, OutputTex.st).rgb;

    outColor = vec4(color, 1.0);
}