#include "Precompiled.h"

string GetFilenameFromPath(string path){
	int indSlash = path.find_last_of("\\/");

	string str;

	if(indSlash != -1){
		str = path.substr(indSlash + 1, path.size() - indSlash - 1);
	}
	else{
		str = path;
	}

	int indFirstDot = str.find_first_of(".");

	return str.substr(0, indFirstDot);
}

string GetFilenameWithExtensionFromPath(string path){
	int indSlash = path.find_last_of("\\/");

	string str;

	if(indSlash != -1){
		str = path.substr(indSlash + 1, path.size() - indSlash - 1);
	}
	else{
		str = path;
	}

	return str;
}

string GetFileExtension(string path){
	int indDot = path.find_last_of(".");

	if(indDot == -1){
		return "";
	}
	else{
		return path.substr(indDot + 1, path.size() - indDot - 1);
	}
}

bool FileExists(string path){
	ifstream input(path);

	if(input.is_open()){
		input.close();

		return true;
	}
	else{
		return false;
	}
}

/* Returns a list of files in a directory (except the ones that begin with a dot) */
void GetAllFilesInDirectory(vector<string> & out, string const & directory, bool fullPath)
{
	HANDLE dir;
	WIN32_FIND_DATA fileData;

	if((dir = FindFirstFile((directory + "/*").c_str(), &fileData)) == INVALID_HANDLE_VALUE)
		return; // No files found

	do
	{
		if((fileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) != 0) // Is directory
			continue;
		
		string filename = fileData.cFileName;
		if(filename[0] == '.')
			continue;

		if(fullPath)
		{
			out.push_back(directory + "/" + filename);
		}
		else
		{
			out.push_back(filename);
		}
	}
	while(FindNextFile(dir, &fileData));

	FindClose(dir);
}

void GetDirectoriesInDirectory(vector<string> &out, const string &directory)
{
    HANDLE dir;
    WIN32_FIND_DATA file_data;

    if ((dir = FindFirstFile((directory + "/*").c_str(), &file_data)) == INVALID_HANDLE_VALUE)
        return; /* No files found */

    do {
        const string file_name = file_data.cFileName;
        const string full_file_name = directory + "/" + file_name;
        const bool is_file = (file_data.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) == 0;

        if (file_name[0] == '.')
            continue;

        if (is_file)
            continue;

        out.push_back(full_file_name);
    } while (FindNextFile(dir, &file_data));

    FindClose(dir);
} // GetFilesInDirectory
