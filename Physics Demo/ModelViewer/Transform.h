/* Start Header -------------------------------------------------------
Copyright (C) 2014 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written consent of DigiPen Institute of Technology is prohibited.
File Name:		Transform.h
Purpose:	    class that holds the Transform component of an object. 
Language:		C++ and compiled in VS2012
Platform:		Visual Studio 2012, Windows 8
Project:		CS560
Author:			Ilan Keshet
Creation date:	20/12/2014
- End Header --------------------------------------------------------*/


#pragma once

#include "Component.h"

class Transform : public Component
{
public:
	Transform() : Component(CID_Transform), position(0), scale(1, 1, 1), updatePosition(false) {}

	virtual std::string GetName() { return "Transform"; }

	virtual void Initialize() {};

	virtual Component* Clone()
	{
		return new Transform();
	}

	void SetPosition(glm::vec3 &pos)
	{
		updatePosition = true;
		newPosition = pos;
	}

	glm::vec3 const& GetPosition() const
	{
		return position;
	}

	glm::vec3 const& GetScale() const
	{
		return scale;
	}

private:
	friend class PhysicsSystem;
	friend class ObjectFactory;

	glm::vec3 position;
	glm::vec3 scale;

	bool updatePosition;
	glm::vec3 newPosition;
};