/* Start Header -------------------------------------------------------
Copyright (C) 2014 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written consent of DigiPen Institute of Technology is prohibited.
File Name:		AnimationHandler.h
Purpose:		Class for Running animations of a model -- can choose which animation to use
Language:		C++ and compiled in VS2012
Platform:		Visual Studio 2012, Windows 8
Project:		CS560
Author:			Ilan Keshet
Creation date:	16/9/2014
- End Header --------------------------------------------------------*/

#pragma once

#include "LineDrawer.h"
#include "Skeleton.h"
#include "Animation.h"
#include "Math.h"

class Mesh;
class GLSLshader;

class AnimationHandler
{
public:
	AnimationHandler(Skeleton* skel);

	~AnimationHandler();
	
	//Adds an new animation to the list of animations
	void AddAnimation(Animation* anim);

	//Draws the currently animating animation
	void Draw(GLSLshader* shader, Mesh const& mesh) const;

	void DrawSkeleton(LineDrawer *lDrawer, bool bindPose = false) const;

	void Update(float dt);

	bool UpdateUntilRestart(float dt);

	bool UpdateUntilKeyFrame(float dt, vector<int> const &keyframesToStopAt);

	void UpdateBlend(int animId1, int animId2, float blendFactor, float dt);

	void UpdateIk(float k, std::map<int, JointPose> const & ikJointPoseMap);

	void Restart();

	Skeleton const& GetSkeleton() { return *skel; }

	int TotalAnimations() { return animations.size(); }

	void SetCurrentAnimationId(int animation);

	void SetAnimationCyclesPerSecond(float cycles) { animationCyclesPerSecond = cycles; }

	int GetCurrentAnimationId();

	bool GetJointChain(std::string const &handle, std::string const &root, vector<Joint*> &jointsOut);

	JointPose GetJointPose(int joint);

	glm::vec3 GetJointModelPosition(int joint);

	glm::mat4 GetJointMatrix(int joint);

private:
	AnimationHandler();

	Skeleton* skel;

	vector<glm::mat4> jointMatrixBuffer;

	//Time goes from 0 to CurrentRunningAnimation's duration
	float currentTime;
	float animationCyclesPerSecond;
	int currentRunningAnimation;

	vector<Animation*> animations;
};