#include "Precompiled.h"
#include "LevelEditor.h"
#include "WindowsSystem.h"
#include "ModelViewerCore.h"
#include "Transform.h"

#include <stddef.h>

bool showGround = true;
bool fillPolygon = true;

glm::vec3 redAnchor;
glm::vec3 blueAnchor;
glm::vec3 greenAnchor;
glm::vec3 yellowAnchor;

Transform *redTrans;
Transform *blueTrans;
Transform *greenTrans;
Transform *yellowTrans;

void TW_CALL SetVec3CallBack(const void *value, void *clientData)
{
	glm::vec3* clientVec3Data = (glm::vec3*) clientData;
	glm::vec3* valueVec3 = (glm::vec3*)value;

	if(*valueVec3 == *clientVec3Data)
	{
		return;
	}

	if(clientData == &redAnchor)
	{
		redAnchor = *valueVec3;
		redTrans->SetPosition(redAnchor);
	}

	if(clientData == &blueAnchor)
	{
		blueAnchor = *valueVec3;
		blueTrans->SetPosition(blueAnchor);
	}

	if(clientData == &greenAnchor)
	{
		greenAnchor = *valueVec3;
		greenTrans->SetPosition(greenAnchor);
	}

	if(clientData == &yellowAnchor)
	{
		yellowAnchor = *valueVec3;
		yellowTrans->SetPosition(yellowAnchor);
	}

	*clientVec3Data = *valueVec3;
}

void TW_CALL GetVec3CallBack(void *value, void *clientData)
{
	glm::vec3* clientVec3Data = (glm::vec3*) clientData;
	glm::vec3 *ptr = (glm::vec3*)value;
	*ptr = *clientVec3Data;
}

void TW_CALL SetBoolCallBack(const void *value, void *clientData)
{
	bool* clientVec2Data = (bool*) clientData;
	bool* valueVec2 = (bool*)value;

	if(*valueVec2 == *clientVec2Data)
	{
		return;
	}

	*clientVec2Data = *valueVec2;
}

void TW_CALL GetBoolCallBack(void *value, void *clientData)
{
	bool* clientVec2Data = (bool*) clientData;
	bool *ptr = (bool*)value;
	*ptr = *clientVec2Data;
}

void TW_CALL SetFloatCallBack(const void *value, void *clientData)
{
	float* clientFloatData = (float*) clientData;
	float* valueFloat = (float*)value;

	if(*valueFloat == *clientFloatData)
	{
		return;
	}

	*clientFloatData = *valueFloat;
}

void TW_CALL GetFloatCallBack(void *value, void *clientData)
{
	float* clientFloatData = (float*) clientData;
	float* ptr = (float*)value;
	*ptr = *clientFloatData;
}

void TW_CALL ButtonCallBack(void *clientData)
{
	MessageRestartAnimation mra;
	ModelViewerCore::Get()->Broadcast(&mra);
}

LevelEditor *editor;

LevelEditor::LevelEditor()
{
	redTrans = 0;
	blueTrans = 0;
	greenTrans = 0;
	yellowTrans = 0;

	editor = this;
}

LevelEditor::~LevelEditor()
{
	TwTerminate();
}

void LevelEditor::SetRedAnchorTransform(Transform * t)
{
	redTrans = t;
	redAnchor = redTrans->GetPosition();
}

void LevelEditor::SetBlueAnchorTransform(Transform * t)
{
	blueTrans = t;
	blueAnchor = blueTrans->GetPosition();
}

void LevelEditor::SetGreenAnchorTransform(Transform * t)
{
	greenTrans = t;
	greenAnchor = greenTrans->GetPosition();
}

void LevelEditor::SetYellowAnchorTransform(Transform * t)
{
	yellowTrans = t;
	yellowAnchor = yellowTrans->GetPosition();
}

void LevelEditor::Initialize()
{
	TwInit(TW_OPENGL_CORE, NULL);

	int width = WindowsSystem::Get()->GetWidth();
	int height = WindowsSystem::Get()->GetHeight();

	TwWindowSize(width, height);

	InitializeBar();
}

void LevelEditor::Refresh() const
{
	TwRefreshBar(bar);
}

void LevelEditor::InitializeBar()
{
	bar = TwNewBar("bar");

	TwAddVarCB(bar, "showGround", TW_TYPE_BOOL32, SetBoolCallBack, GetBoolCallBack, &showGround, "label='Show Ground' key=g");
	TwAddVarCB(bar, "fillPolygon", TW_TYPE_BOOL32, SetBoolCallBack, GetBoolCallBack, &fillPolygon, "label='Fill Polygon' key=f");

    TwStructMember pointMembers[] = { 
        { "X", TW_TYPE_FLOAT, offsetof(glm::vec3, x), " Step=0.05 " },
        { "Y", TW_TYPE_FLOAT, offsetof(glm::vec3, y), " Step=0.05 " }, 
		{ "Z", TW_TYPE_FLOAT, offsetof(glm::vec3, z), " Step=0.05 " }
	};

	TwType pointType = TwDefineStruct("POINT", pointMembers, 3, sizeof(glm::vec3), NULL, NULL);

	TwAddVarCB(bar, "redAnchor", pointType, SetVec3CallBack, GetVec3CallBack, &redAnchor, "label='Red Anchor position'");
	TwAddVarCB(bar, "blueAnchor", pointType, SetVec3CallBack, GetVec3CallBack, &blueAnchor, "label='Blue Anchor position'");
	TwAddVarCB(bar, "greenAnchor", pointType, SetVec3CallBack, GetVec3CallBack, &greenAnchor, "label='Green Anchor position'");
	TwAddVarCB(bar, "yellowAnchor", pointType, SetVec3CallBack, GetVec3CallBack, &yellowAnchor, "label='Yellow Anchor position'");
}

void LevelEditor::Draw()
{
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	TwDraw();
}

LevelEditor* LevelEditor::Get()
{
	return editor;
}