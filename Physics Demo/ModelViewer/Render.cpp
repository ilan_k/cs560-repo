/* Start Header -------------------------------------------------------
Copyright (C) 2014 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written consent of DigiPen Institute of Technology is prohibited.
File Name:		Render.cpp
Purpose:	    Component that handles the graphical display of an object
Language:		C++ and compiled in VS2012
Platform:		Visual Studio 2012, Windows 8
Project:		CS560
Author:			Ilan Keshet
Creation date:	20/12/2014
- End Header --------------------------------------------------------*/

#include "Precompiled.h"
#include "Component.h"
#include "Transform.h"
#include "Model.h"
#include "Texture.h"
#include "Render.h"

#include "GraphicsSystem.h"

void Render::Initialize()
{
	trans = this->owner->has(Transform);

	GraphicsSystem::Get()->RegisterRender(this);
}

Render::~Render()
{
	GraphicsSystem::Get()->UnregisterRender(this->owner->GetId());
}