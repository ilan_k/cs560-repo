/* Start Header -------------------------------------------------------
Copyright (C) 2014 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written consent of DigiPen Institute of Technology is prohibited.
File Name:		Render.h
Purpose:	    Component that handles the graphical display of an object
Language:		C++ and compiled in VS2012
Platform:		Visual Studio 2012, Windows 8
Project:		CS560
Author:			Ilan Keshet
Creation date:	20/12/2014
- End Header --------------------------------------------------------*/

#pragma once

#include "Component.h"

class Transform;

class Render : public Component
{
public:
	Render() : Component(CID_Render) {};

	~Render();

	virtual std::string GetName() { return "Render"; }

	virtual void Initialize();

	virtual Component* Clone()
	{
		Render *rend = new Render();
		rend->model = this->model;
		rend->tex = this->tex;

		return rend;
	}

	Model* model;
	Texture* tex;
	Transform const * trans;
};