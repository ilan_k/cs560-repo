/* Start Header -------------------------------------------------------
Copyright (C) 2014 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written consent of DigiPen Institute of Technology is prohibited.
File Name:		ObjectFactory.cpp
Purpose:		Class that creates all the objects in the scene
Language:		C++ and compiled in VS2012
Platform:		Visual Studio 2012, Windows 8
Project:		CS560
Author:			Ilan Keshet
Creation date:	20/12/2014
- End Header --------------------------------------------------------*/

#include "Precompiled.h"
#include "ObjectManager.h"
#include "ObjectFactory.h"
#include "Render.h"
#include "Transform.h"
#include "Connection.h"
#include "Body.h"

#include "LevelEditor.h"

#define ASSETS_DIRECTORY "..\\Resources\\Assets\\"

/******************************************************************************/
/*!
	Creates 1 row of the bridge
*/
/******************************************************************************/
void ObjectFactory::CreateBridgeRow(ObjectManager* objManager, glm::vec3 position, std::vector<Object*> &nodes, 
									std::string &anchor1Model, std::string &anchor2Model)
{
	for(int count = -5; count <= 5; count++)
	{
		Object *obj;

		if(count == -5)
		{
			obj = objManager->CreateObject(anchor1Model);
		}
		else if(count == 5)
		{
			obj = objManager->CreateObject(anchor2Model);
		}
		else
		{
			obj = objManager->CreateObject("Sphere");
		}

		Transform * trans = obj->has(Transform);

		trans->scale = glm::vec3(1.0f, 1.0f, 1.0f);

		trans->position = position;
		trans->position.x *= count;

		Body * objBody = obj->has(Body);

		if(count == -5 || count == 5)
		{
			objBody->isStatic = true;
		}

		if(nodes.size() > 0)
		{
			Object* behindObj = nodes.back();

			Body* behindBody = behindObj->has(Body);

			behindBody->body2Connection = objBody;
			objBody->body1Connection = behindBody;

			Object* connectObj = objManager->CreateObject("Connection");
			
			Connection* connect = connectObj->has(Connection);
			connect->Initialize(behindBody, objBody);
		}

		nodes.push_back(obj);
	}
}


/******************************************************************************/
/*!
	Creates a Sphere archetype object
*/
/******************************************************************************/
void ObjectFactory::CreateSphereToArchetypeList(ObjectManager* objManager, std::string &textureName, std::string &objName)
{
	Render *r = new Render();
	r->model = objManager->GetModel("Sphere");
	r->tex = objManager->GetTexture(textureName);

	Transform *trans = new Transform();

	Body *body = new Body();

	Object *obj = new Object(objName);

	obj->AddComponent(r); obj->AddComponent(trans); obj->AddComponent(body);

	objManager->AddArchetype(obj);
}


/******************************************************************************/
/*!
	Initializes the scene with the Bridge
*/
/******************************************************************************/
void ObjectFactory::Initialize(ObjectManager* objManager)
{
	if(!objManager->LoadModel(ASSETS_DIRECTORY "Sphere.bin"))
	{
		assert(false);
	}

	if(!objManager->LoadTexture(ASSETS_DIRECTORY "white.png"))
	{
		assert(false);
	}

	if(!objManager->LoadTexture(ASSETS_DIRECTORY "red.png"))
	{
		assert(false);
	}

	if(!objManager->LoadTexture(ASSETS_DIRECTORY "blue.png"))
	{
		assert(false);
	}


	if(!objManager->LoadTexture(ASSETS_DIRECTORY "green.png"))
	{
		assert(false);
	}

	if(!objManager->LoadTexture(ASSETS_DIRECTORY "yellow.png"))
	{
		assert(false);
	}

	std::string whiteTex = "white"; std::string whiteModelName = "Sphere";
	std::string redTex = "red"; std::string redModelName = "RedSphere";
	std::string blueTex = "blue"; std::string blueModelName = "BlueSphere";
	std::string greenTex = "green"; std::string greenModelName = "GreenSphere";
	std::string yellowTex = "yellow"; std::string yellowModelName = "YellowSphere";

	CreateSphereToArchetypeList(objManager, whiteTex, whiteModelName);
	CreateSphereToArchetypeList(objManager, redTex, redModelName);
	CreateSphereToArchetypeList(objManager, blueTex, blueModelName);
	CreateSphereToArchetypeList(objManager, greenTex, greenModelName);
	CreateSphereToArchetypeList(objManager, yellowTex, yellowModelName);

	Connection *connect = new Connection();

	Object* obj = new Object("Connection");
	obj->AddComponent(connect);
	
	objManager->AddArchetype(obj);

	std::vector<Object*> nodesRow1;
	std::vector<Object*> nodesRow2;

	CreateBridgeRow(objManager, glm::vec3(4.0f, 10.0f, 2.0f), nodesRow1, redModelName, blueModelName);
	CreateBridgeRow(objManager, glm::vec3(4.0f, 10.0f, -2.0f), nodesRow2, greenModelName, yellowModelName);

	LevelEditor* le = LevelEditor::Get();

	le->SetRedAnchorTransform(nodesRow1[0]->has(Transform));
	le->SetBlueAnchorTransform(nodesRow1[nodesRow1.size() - 1]->has(Transform));

	le->SetGreenAnchorTransform(nodesRow2[0]->has(Transform));
	le->SetYellowAnchorTransform(nodesRow2[nodesRow2.size() - 1]->has(Transform));


	for(unsigned int i = 0; i < nodesRow1.size(); i++)
	{
		Object* objConnection = objManager->CreateObject("Connection");

		Connection* connect = objConnection->has(Connection);

		connect->tension = 80.0f;

		connect->Initialize(nodesRow1[i]->has(Body), nodesRow2[i]->has(Body));
	}
}