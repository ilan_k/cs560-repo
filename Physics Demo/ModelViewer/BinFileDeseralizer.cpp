/* Start Header -------------------------------------------------------
Copyright (C) 2014 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written consent of DigiPen Institute of Technology is prohibited.
File Name:		BinFileDeseralizer.cpp
Purpose:		Class for reading a binary file in either Chris Peters format or mine
Language:		C++ and compiled in VS2012
Platform:		Visual Studio 2012, Windows 8
Project:		CS560
Author:			Ilan Keshet
Creation date:	16/9/2014
- End Header --------------------------------------------------------*/

#include "Precompiled.h"
#include "BinFileDeseralizer.h"

BinFileDeseralizer::BinFileDeseralizer(string file) : binFile(file, ios::binary | ios::in)
{
}

BinFileDeseralizer::~BinFileDeseralizer()
{
	if(binFile.is_open())
	{		
		binFile.close();
	}
}

/******************************************************************************/
/*!
	Reads one chunk of a binary file
	ChunkHeaders have type (unsigned int), and size (unsigned int) 
*/
/******************************************************************************/
ChunkHeader BinFileDeseralizer::ReadChunkHeader()
{
	ChunkHeader ch;
	ch.type = 0;
	Read(ch.type);
	Read(ch.size);

	//position of current model in the input stream
	ch.startPos = (unsigned int)binFile.tellg();
	ch.endPos = ch.startPos + ch.size;

	return ch;
}

/******************************************************************************/
/*!
	Reads the chunkheader -- but then seeks back to the beginning of the header
*/
/******************************************************************************/
ChunkHeader BinFileDeseralizer::PeekChunkHeader()
{
	ChunkHeader ch = ReadChunkHeader();
	binFile.seekg(-int(sizeof(unsigned int) * 2), ios::cur);

	return ch;
}

//This function does not work correctly, as endPos is always wrong
void BinFileDeseralizer::SkipChunk(ChunkHeader &ch)
{
	binFile.seekg(ch.endPos);
}

