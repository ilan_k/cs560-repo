/* Start Header -------------------------------------------------------
Copyright (C) 2014 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written consent of DigiPen Institute of Technology is prohibited.
File Name:		GraphicsSystem.h
Purpose:		Graphics System for Drawing the the model
				It also does all the heavy work under the hood as well
Language:		C++ and compiled in VS2012
Platform:		Visual Studio 2012, Windows 8
Project:		CS560
Author:			Ilan Keshet
Creation date:	16/9/2014
- End Header --------------------------------------------------------*/

#include "Precompiled.h"
#include "GraphicsSystem.h"
#include "Model.h"
#include "InputSystem.h"
#include "WindowsSystem.h"
#include "Render.h"
#include "Connection.h"
#include "Transform.h"
#include "ObjectManager.h"

#define ASSETS_DIRECTORY "..\\Resources\\Assets\\"

#define CHECKERROR {int err = glGetError(); if (err) { fprintf(stderr, "OpenGL error %d (at line %d): %s\n", err, __LINE__, glewGetErrorString(err)); } }

#define PLANE_SIZE 100
#define ZNEAR 1.0f
#define ZFAR 200.0f
#define FOVY 45.0f
#define ASPECT 640.0f / 480.0f

GraphicsSystem *graphics = 0;

GraphicsSystem::GraphicsSystem() : planeTransform(0)
{
	graphics = this;
}

GraphicsSystem::~GraphicsSystem()
{
	if(planeTransform)
	{
		delete planeTransform;
	}
}

/******************************************************************************/
/*!
	Initializes the entire Graphics system including loading the model
*/
/******************************************************************************/
void GraphicsSystem::Initialize()
{
	glewExperimental = GL_TRUE;
	glewInit();

	glEnable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glLineWidth(0.25f);

	zoom = 45.0f;
	eyeXRotation = 45;
	eyeYRotation = 0;

	sceneCenterX = 0;
	sceneCenterY = 0;

	proj = glm::perspective(FOVY, ASPECT, ZNEAR, ZFAR);

	glViewport(0, 0, WindowsSystem::Get()->GetWidth(), WindowsSystem::Get()->GetHeight());

	CHECKERROR;

	InitializeStaticShader();

	CHECKERROR;

	InitializeSkinnedShader();

	CHECKERROR;

	InitializeDebugShader();

	CHECKERROR;

	planeTransform = new Transform();

	ObjectManager *objMan = ObjectManager::Get();

	editor.Initialize();

	lDrawer.Initialize(proj);
}

#define PLANE_Y_OFFSET -0.1f

/******************************************************************************/
/*!
	Updates the modelMatrix using the passed in modelTransform
*/
/******************************************************************************/
void GraphicsSystem::UpdateModelMatrix(Transform const& modelTransform)
{
	modelMatrix = glm::translate(glm::mat4(1), modelTransform.GetPosition());
	modelMatrix = glm::scale(modelMatrix, modelTransform.GetScale());
	modelMatrix = glm::rotate(modelMatrix, 0.0f, glm::vec3(0.0f, 1.0f, 0.0));
}

/******************************************************************************/
/*!
	Updates the Model and draws to the screen
*/
/******************************************************************************/
void GraphicsSystem::Update(float dt)
{
	viewMatrix = glm::mat4(1);
	viewMatrix = glm::translate(viewMatrix, glm::vec3(sceneCenterX, sceneCenterY, -zoom));
	viewMatrix = glm::rotate(viewMatrix, eyeXRotation, glm::vec3(1.0f, 0.0f, 0.0f));
	viewMatrix = glm::rotate(viewMatrix, eyeYRotation, glm::vec3(0.0f, 1.0f, 0.0f));

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	if(fillPolygon)
	{
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	}
	else
	{
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	}

	for(auto it = renders.begin(); it != renders.end(); ++it)
	{
		UpdateModelMatrix(*it->second->trans);

		RenderModel(it->second->model, it->second->tex, true);
	}

	lDrawer.Bind(glm::mat4(1), viewMatrix);

	for(auto it = connections.begin(); it != connections.end(); ++it)
	{
		lDrawer.DrawLine(it->second->fromTransform->GetPosition(), it->second->toTransform->GetPosition());
	}

	lDrawer.unBind();

	glm::mat4 planeScaleMatrix = glm::scale(glm::mat4(1), planeTransform->GetScale());

	lDrawer.Bind(planeScaleMatrix, viewMatrix);

	int halfPlane = PLANE_SIZE / 2;

	if(true)
	{
		lDrawer.DrawLine(glm::vec3(0.0 -halfPlane, 0.0, 0.0 - halfPlane), glm::vec3(5.0 - halfPlane, 0.0, 0.0 - halfPlane), glm::vec3(1.0, 0.0, 0.0));
		lDrawer.DrawLine(glm::vec3(0.0 -halfPlane, 0.0, 0.0 - halfPlane), glm::vec3(0.0 - halfPlane, 5.0, 0.0 - halfPlane), glm::vec3(0.0, 1.0, 0.0));
		lDrawer.DrawLine(glm::vec3(0.0 -halfPlane, 0.0, 0.0 - halfPlane), glm::vec3(0.0 - halfPlane, 0.0, 5.0 - halfPlane), glm::vec3(0.0, 0.0, 1.0));
	}

	if(showGround)
	{
		for(int i = -halfPlane; i <= halfPlane; i++)
		{
			lDrawer.DrawLine(glm::vec3(i, PLANE_Y_OFFSET, -halfPlane), glm::vec3(i, PLANE_Y_OFFSET, halfPlane), glm::vec3(0.1, 0.1, 0.1));
			lDrawer.DrawLine(glm::vec3(-halfPlane, PLANE_Y_OFFSET, i), glm::vec3(halfPlane, PLANE_Y_OFFSET, i), glm::vec3(0.1, 0.1, 0.1));
		}
	}

	lDrawer.unBind();

	editor.Draw();
}

/******************************************************************************/
/*!
	Renders just the Model
*/
/******************************************************************************/
void GraphicsSystem::RenderModel(Model* model, Texture *tex, bool showModel)
{
	glm::mat4 normalMatrix = glm::transpose(glm::inverse(viewMatrix * modelMatrix));

	glActiveTexture(GL_TEXTURE0 + tex->getTexture());
	glBindTexture(GL_TEXTURE_2D, tex->getTexture());

	if(model->IsSkinned())
	{
		skinnedShader.Bind();
		glUniformMatrix4fv(skinnedShader("modelMatrix"), 1, GL_FALSE, glm::value_ptr(modelMatrix));
		glUniformMatrix4fv(skinnedShader("viewMatrix"), 1, GL_FALSE, glm::value_ptr(viewMatrix));
		glUniformMatrix4fv(skinnedShader("normalMatrix"), 1, GL_FALSE, glm::value_ptr(normalMatrix));
		glUniform1i(skinnedShader("tex"), tex->getTexture());
	}
	else
	{
		staticShader.Bind();
		glUniformMatrix4fv(staticShader("modelMatrix"), 1, GL_FALSE, glm::value_ptr(modelMatrix));
		glUniformMatrix4fv(staticShader("viewMatrix"), 1, GL_FALSE, glm::value_ptr(viewMatrix));
		glUniformMatrix4fv(staticShader("normalMatrix"), 1, GL_FALSE, glm::value_ptr(normalMatrix));
		glUniform1i(staticShader("tex"), tex->getTexture());
	}

	glBindVertexArray(model->vao);

	if(showModel)
	{
		if(model)
		{
			if(model->IsSkinned())
			{
				skinnedShader.Bind();

				model->Draw(&skinnedShader);
			}
			else
			{
				staticShader.Bind();
				model->Draw(&staticShader);
			}
		}
	}

	lDrawer.Bind(modelMatrix, viewMatrix);

	RenderSkeleton(model);
}

/******************************************************************************/
/*!
	Renders just the Skeleton of the model
*/
/******************************************************************************/
void GraphicsSystem::RenderSkeleton(Model *model)
{
	if(model)
	{
		model->DrawSkeleton(&lDrawer);
	}
}

/******************************************************************************/
/*!
	Initializes all the static vertex attributes to the passed in shader program
*/
/******************************************************************************/
void GraphicsSystem::SetStaticVertexAttributes(GLuint shaderProgram, unsigned int size)
{
	//Bind Different Buffer for the Shader
	GLint posAttrib = glGetAttribLocation(shaderProgram, "position");
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, size, 0);

	CHECKERROR;

	GLint texAttrib = glGetAttribLocation(shaderProgram, "tex");
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, size, (void*)(6 * sizeof(GLfloat)));

	CHECKERROR;

	GLint normalAttrib = glGetAttribLocation(shaderProgram, "normal");
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, size, (void*)(3 * sizeof(GLfloat)));

	CHECKERROR;

	GLint tangentAttrib = glGetAttribLocation(shaderProgram, "tangent");
	glEnableVertexAttribArray(3);
	glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, size, (void*)(8 * sizeof(GLfloat)));

	CHECKERROR;
}

/******************************************************************************/
/*!
	Initializes all the skinned vertex attributes to the passed in shader program
*/
/******************************************************************************/
void GraphicsSystem::SetSkinnedVertexAttributes(GLuint shaderProgram, unsigned int size)
{
	//Bind Different Buffer for the Shader
	GLint posAttrib = glGetAttribLocation(shaderProgram, "position");
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, size, 0);

	GLint normalAttrib = glGetAttribLocation(shaderProgram, "normal");
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, size, (void*)(3 * sizeof(GLfloat)));

	GLint texAttrib = glGetAttribLocation(shaderProgram, "tex");
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, size, (void*)(6 * sizeof(GLfloat)));

	GLint boneWeightAttrib = glGetAttribLocation(shaderProgram, "boneWeights");
	glEnableVertexAttribArray(3);
	glVertexAttribPointer(3, 4, GL_FLOAT, GL_FALSE, size, (void*)(8 * sizeof(GLfloat)));

	GLint boneIndicesAttrib = glGetAttribLocation(shaderProgram, "boneIndices");
	glEnableVertexAttribArray(4);
	glVertexAttribIPointer(4, 4, GL_UNSIGNED_BYTE, size, (void*)(12 * sizeof(GLfloat)));

	GLint tangentAttrib = glGetAttribLocation(shaderProgram, "tangent");
	glEnableVertexAttribArray(5);
	glVertexAttribPointer(5, 3, GL_FLOAT, GL_FALSE, size, (void*)(16 * sizeof(GLfloat)));

	CHECKERROR;
}

/******************************************************************************/
/*!
	Loads a passed in texture
*/
/******************************************************************************/
void GraphicsSystem::LoadTexture(string textureFile, Texture *&tex)
{
	CHECKERROR;

	if(tex)
	{
		GLuint id = tex->getTexture();

		glDeleteTextures(1, &id);

		delete tex;
	}

	this->textureFile = GetFilenameWithExtensionFromPath(textureFile);
	tex = new Texture(textureFile);
	
	staticShader.Bind();
	glUniform1i(staticShader("tex"), 0);
	staticShader.unBind();

	skinnedShader.Bind();
	glUniform1i(skinnedShader("tex"), 0);
	skinnedShader.unBind();

	CHECKERROR;
}

/******************************************************************************/
/*!
	Handles Messages sent from the core
*/
/******************************************************************************/
void GraphicsSystem::SendMsg(Message * mess){
	if(mess->MessageId == Mid::MouseClick)
	{
		MessageMouseClick *mmc = dynamic_cast<MessageMouseClick*>(mess);
		mouseX = mmc->x;
		mouseY = mmc->y;
		mouseButton = mmc->mouseButton;

		mouseButtonDown = true;
	}
	else if(mess->MessageId == Mid::MouseMotion)
	{
		if(mouseButtonDown)
		{
			MessageMouseMotion *mmm = dynamic_cast<MessageMouseMotion*>(mess);
			int dx = mmm->x - mouseX;
			int dy = mmm->y - mouseY;

			if(mouseButton == SDL_BUTTON_LEFT)
			{
				eyeYRotation += dx;
				eyeXRotation += dy;
			}
			else if(mouseButton == SDL_BUTTON_MIDDLE || mouseButton == SDL_BUTTON_RIGHT)
			{
				sceneCenterX += dx / 10.0f;
				sceneCenterY -= dy / 10.0f;
			}

			mouseX = mmm->x;
			mouseY = mmm->y;
		}
	}
	else if(mess->MessageId == Mid::MouseWheel)
	{
		MessageMouseWheel *mmw = dynamic_cast<MessageMouseWheel*>(mess);
		zoom -= mmw->yScroll / 2.0f;
	}
	else if(mess->MessageId == Mid::MouseUp)
	{
		mouseButtonDown = false;
	}
}

/******************************************************************************/
/*!
	Loads a Bin file
*/
/******************************************************************************/
void GraphicsSystem::LoadBinModel(string filepath, Model *&model)
{
	if(model)
	{
		delete model;
		model = 0;
	}

	CHECKERROR;

	model = Model::LoadFile(filepath);

	CHECKERROR;

	glGenVertexArrays(1,&model->vao);
	glBindVertexArray(model->vao);

	CHECKERROR;

	glGenBuffers(1, &model->vbo);
	glBindBuffer(GL_ARRAY_BUFFER,model->vbo);

	CHECKERROR;

	Mesh const * mesh = model->GetMesh();

	if(mesh)
	{
		int size;

		if(model->IsSkinned())
		{
			size = sizeof(SkinnedModelVertex);
		}
		else
		{
			size = sizeof(StaticModelVertex);
		}

		CHECKERROR;

		glBufferData(GL_ARRAY_BUFFER, mesh->numVertices * size, mesh->vertexBufferData, GL_STATIC_DRAW);

		CHECKERROR;

		glGenBuffers(1, &model->ebo);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, model->ebo);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, mesh->numIndices * sizeof(unsigned int), mesh->indices, GL_STATIC_DRAW);

		if(model->IsSkinned())
		{
			SetSkinnedVertexAttributes(skinnedShader.getShaderProgram(), sizeof(SkinnedModelVertex));
		}
		else
		{
			SetStaticVertexAttributes(staticShader.getShaderProgram(), sizeof(StaticModelVertex));
		}
	}
}

/******************************************************************************/
/*!
	Initializes the static shader -- for rendering objects that do not animate
*/
/******************************************************************************/
void GraphicsSystem::InitializeStaticShader()
{
	// Create Shader Program
	staticShader.createShaderProgram("Shaders\\staticVertexShader.glsl","Shaders\\staticFragmentShader.glsl");
	staticShader.Bind();
	staticShader.addUniform("projMatrix");
	staticShader.addUniform("modelMatrix");
	staticShader.addUniform("viewMatrix");
	staticShader.addUniform("normalMatrix");
	staticShader.addUniform("viewInvMatrix");
	staticShader.addUniform("tex");

	CHECKERROR;

	glUniformMatrix4fv(staticShader("projMatrix"), 1, GL_FALSE, glm::value_ptr(proj));

	CHECKERROR;

	glUniform1i(staticShader("tex"), 0);

	CHECKERROR;
}

/******************************************************************************/
/*!
	Initializes the debug shader for drawing lines
*/
/******************************************************************************/
void GraphicsSystem::InitializeDebugShader()
{
}

/******************************************************************************/
/*!
	Initializes the skinned shader for rendering objects that animate
*/
/******************************************************************************/
void GraphicsSystem::InitializeSkinnedShader()
{
	// Create Shader Program
	skinnedShader.createShaderProgram("Shaders\\skinnedVertexShader.glsl","Shaders\\skinnedFragmentShader.glsl");
	skinnedShader.Bind();
	skinnedShader.addUniform("projMatrix");
	skinnedShader.addUniform("modelMatrix");
	skinnedShader.addUniform("viewMatrix");
	skinnedShader.addUniform("normalMatrix");
	skinnedShader.addUniform("viewInvMatrix");
	skinnedShader.addUniform("tex");
	skinnedShader.addUniform("skinningMatrices");
	skinnedShader.addUniform("bindPose");

	glUniformMatrix4fv(skinnedShader("projMatrix"), 1, GL_FALSE, glm::value_ptr(proj));

	//glUniform1i(glGetUniformLocation(sceneShaderProgram, "tex"), 0);
	glUniform1i(skinnedShader("tex"), 0);
}

GraphicsSystem* GraphicsSystem::Get()
{
	return graphics;
}

void GraphicsSystem::RegisterRender(Render * rend)
{
	renders[rend->owner->GetId()] = rend;
}

void GraphicsSystem::UnregisterRender(int id)
{
	auto it = renders.find(id);

	if(it != renders.end())
	{
		renders.erase(it);
	}
}

void GraphicsSystem::RegisterConnection(Connection * connect)
{
	connections[connect->owner->GetId()] = connect;
}

void GraphicsSystem::UnregisterConnection(int id)
{
	auto it = connections.find(id);

	if(it != connections.end())
	{
		connections.erase(it);
	}
}