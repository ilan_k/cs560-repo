/* Start Header ------------------------------------------------------- 
Copyright (C) 2013 DigiPen Institute of Technology. 
Reproduction or disclosure of this file or its contents without the prior 
written consent of DigiPen Institute of Technology is prohibited. 
 
File Name: GEGameCore.cpp
Purpose: Handles managing systems added into the GameEngine as well as running the game loop
Language: Microsoft Visual C++, Compiler: MSV 2010
Platform: Has been tested on Windows 7 in DigiPen Lab Computers
Project: CS529_ilan.k_Game_Engine
Author: Ilan Keshet, ilan.k, 60001813
Creation date: December 13th 2013
- End Header --------------------------------------------------------*/ 

#include "Precompiled.h"
#include "ObjectManager.h"
#include "ObjectFactory.h"
#include "ModelViewerCore.h"
#include "SDL.h"
#include "FrameRateTimer.h"

//A global pointer to the core
ModelViewerCore* CORE;

ModelViewerCore::ModelViewerCore() : objManager(0)
{
	ModelViewerRunning = true;

	CORE = this; //Set the global pointer
}

ModelViewerCore::~ModelViewerCore()
{
	if(objManager)
	{
		delete objManager;
	}
}

ModelViewerCore* ModelViewerCore::Get()
{
	return CORE;
}

void ModelViewerCore::Initialize(){
	for (unsigned i = 0; i < Systems.size(); i++){
		Systems[i]->Initialize();
	}

	objManager = new ObjectManager();
	ObjectFactory::Initialize(objManager);
}

void ModelViewerCore::GameLoop()
{
	//Initialize the last time variable so our first frame
	//is "zero" seconds (and not some huge unknown number)
	FrameRateTimer::FrameRateTimerStart();

	while(ModelViewerRunning)
	{
		FrameRateTimer::FrameRateTimerStart();

		float ft = (float)FrameRateTimer::GetFrameTime();

		//cout << "Frame Time: " << ft << endl;

		//Update every system and tell each one how much
		//time has passed since the last update
		/*for (unsigned i = 0; i < Systems.size(); ++i){
			Systems[i]->Update(ft);
		}*/

		for(int i = Systems.size() - 1; i >= 0; i--)
		{
			Systems[i]->Update(ft);
		}

		FrameRateTimer::FrameRateTimerEnd();
	}
}

void ModelViewerCore::Broadcast(Message *mess){
	if(mess->MessageId == Mid::Quit){
		ModelViewerRunning = false;
	}
	else{
		for(auto it = Systems.begin(); it != Systems.end(); ++it){
			(*it)->SendMsg(mess);
		}
	}
}

void ModelViewerCore::AddSystem(System* system)
{
	//Add a system to the core to be updated
	//every frame
	Systems.push_back(system);
}

void ModelViewerCore::DestroySystems()
{			
	//Delete all the systems in reverse order
	//that they were added in (to minimize any
	//dependency problems between systems)
	while(Systems.size()){
		delete Systems[Systems.size() - 1];

		Systems.pop_back();
	}
}