/* Start Header -------------------------------------------------------
Copyright (C) 2014 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written consent of DigiPen Institute of Technology is prohibited.
File Name:		PhysicsSystem.cpp
Purpose:	    Class that manages all the physics interaction of all Body Components and Connection commponents in scene
Language:		C++ and compiled in VS2012
Platform:		Visual Studio 2012, Windows 8
Project:		CS560
Author:			Ilan Keshet
Creation date:	20/12/2014
- End Header --------------------------------------------------------*/


#include "Precompiled.h"
#include "PhysicsSystem.h"
#include "Body.h"
#include "Transform.h"
#include "Connection.h"

#define GRAVITY 9.8f
#define DRAG	0.5f

PhysicsSystem* physics = 0;

PhysicsSystem::PhysicsSystem()
{
	physics = this;
}

PhysicsSystem::~PhysicsSystem()
{
}

PhysicsSystem* PhysicsSystem::Get()
{
	return physics;
}

/******************************************************************************/
/*!
	Applies the gravity force to the object
*/
/******************************************************************************/
void PhysicsSystem::ApplyGravityForce(Body * body)
{
	if(!body->isStatic)
	{
		body->netForce += glm::vec3(0.0f, -1.0f, 0.0f) * GRAVITY * body->mass;
	}
}


/******************************************************************************/
/*!
	Applies the tension force to each of the two bodies in the connection
	provided they aren't static objects
*/
/******************************************************************************/
void PhysicsSystem::ApplyTensionForces(Connection * connect)
{
	glm::vec3 deltaPos1 = connect->fromBody->trans->position - connect->toBody->trans->position;
	glm::vec3 deltaPos2 = -deltaPos1;

	if(glm::length(deltaPos1))
	{
		if(!connect->fromBody->isStatic)
		{
			connect->fromBody->netForce += -connect->tension * (deltaPos1 - connect->restLength * glm::normalize(deltaPos1));
		}

		if(!connect->toBody->isStatic)
		{
			connect->toBody->netForce += -connect->tension * (deltaPos2 - connect->restLength * glm::normalize(deltaPos2));
		}
	}
}

/******************************************************************************/
/*!
	//Applies forces to all bodies / updates velocities
	// / integrates positions of all bodies
*/
/******************************************************************************/
void PhysicsSystem::Update(float dt)
{

	for(auto it = bodies.begin(); it != bodies.end(); ++it)
	{
		Transform * trans = it->second->owner->has(Transform);

		if(trans->updatePosition)
		{
			trans->updatePosition = false;
			trans->position = trans->newPosition;
		}
	}

	//Reset the net force of all bodies to 0
	for(auto it = bodies.begin(); it != bodies.end(); ++it)
	{
		it->second->netForce = glm::vec3(0.0f, 0.0f, 0.0f);
	}

	//Update sum of all forces
	for(auto it = bodies.begin(); it != bodies.end(); ++it)
	{
		ApplyGravityForce(it->second);
	}

	for(auto it = connections.begin(); it != connections.end(); ++it)
	{
		ApplyTensionForces(it->second);
	}

	//Apply drag force
	for(auto it = bodies.begin(); it != bodies.end(); ++it)
	{
		it->second->netForce += -DRAG * it->second->velocity;
	}

	//Update velocities
	for(auto it = bodies.begin(); it != bodies.end(); ++it)
	{
		it->second->velocity += it->second->netForce / it->second->mass  * dt;
	}

	//Integrate positions
	for(auto it = bodies.begin(); it != bodies.end(); ++it)
	{
		Body* b = it->second;

		if(!b->isStatic)
		{
			//Basic euler integration
			b->trans->position += b->velocity * dt;
		}
	}
}

void PhysicsSystem::Initialize()
{
}

void PhysicsSystem::SendMsg(Message * mess)
{
}

void PhysicsSystem::RegisterBody(Body* body)
{
	bodies[body->owner->GetId()] = body;
}

void PhysicsSystem::UnregisterBody(int id)
{
	auto it = bodies.find(id);

	if(it != bodies.end())
	{
		bodies.erase(it);
	}
}

void PhysicsSystem::RegisterConnection(Connection * connect)
{
	connections[connect->owner->GetId()] = connect;
}

void PhysicsSystem::UnregisterConnection(int id)
{
	auto it = connections.find(id);

	if(it != connections.end())
	{
		connections.erase(it);
	}
}