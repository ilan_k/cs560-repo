/* Start Header -------------------------------------------------------
Copyright (C) 2014 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written consent of DigiPen Institute of Technology is prohibited.
File Name:		ObjectManager.cpp
Purpose:	    class that managers (contains) all the objects and archetypes of the scene
Language:		C++ and compiled in VS2012
Platform:		Visual Studio 2012, Windows 8
Project:		CS560
Author:			Ilan Keshet
Creation date:	20/12/2014
- End Header --------------------------------------------------------*/

#include "Precompiled.h"
#include "Object.h"
#include "Texture.h"
#include "Model.h"
#include "ObjectManager.h"
#include "GraphicsSystem.h"

ObjectManager *manager;

ObjectManager::ObjectManager()
{
	manager = this;
}

ObjectManager* ObjectManager::Get()
{
	return manager;
}

Object* ObjectManager::FindObjectWithArchetypeName(std::string archetypeName)
{
	for(auto it = objects.begin(); it != objects.end(); ++it)
	{
		if(it->second->GetName() == archetypeName)
		{
			return it->second;
		}
	}

	return 0;
}


ObjectManager::~ObjectManager()
{
	for(auto it = objects.begin(); it != objects.end(); it++)
	{
		delete it->second;
	}

	for(auto it = archetypes.begin(); it != archetypes.end(); ++it)
	{
		delete it->second;
	}

	for(auto it = models.begin(); it != models.end(); ++it)
	{
		delete it->second;
	}

	for(auto it = textures.begin(); it != textures.end(); ++it)
	{
		delete it->second;
	}
}

bool ObjectManager::AddArchetype(Object* obj)
{
	auto it = archetypes.find(obj->archetypeName);

	if(it == archetypes.end())
	{
		archetypes[obj->archetypeName] = obj;
		return true;
	}
	else
	{
		return false;
	}
}

Object* ObjectManager::CreateObject(std::string archetypeName)
{
	auto it = archetypes.find(archetypeName);

	if(it != archetypes.end())
	{
		Object* obj = it->second->Clone();
		obj->Initialize();

		objects[obj->objectId] = obj;

		return obj;
	}
	else
	{
		return 0;
	}
}

bool ObjectManager::LoadModel(std::string filepath)
{
	std::string modelName = GetFilenameFromPath(filepath);

	auto it = models.find(modelName);

	if(it == models.end())
	{
		Model* model = 0;
		GraphicsSystem::Get()->LoadBinModel(filepath, model);

		models[modelName] = model;

		return true;
	}
	else
	{
		return false;
	}
}

bool ObjectManager::LoadTexture(std::string filepath)
{
	std::string textureName = GetFilenameFromPath(filepath);

	auto it = textures.find(textureName);

	if(it == textures.end())
	{
		Texture *tex = 0;
		GraphicsSystem::Get()->LoadTexture(filepath, tex);

		textures[textureName] = tex;

		return true;
	}
	else
	{
		return false;
	}
}

Model* ObjectManager::GetModel(std::string modelName)
{
	auto it = models.find(modelName);

	if(it != models.end())
	{
		return it->second;
	}
	else
	{
		return 0;
	}
}

Texture* ObjectManager::GetTexture(std::string textureName)
{
	auto it = textures.find(textureName);

	if(it != textures.end())
	{
		return it->second;
	}
	else
	{
		return 0;
	}
}