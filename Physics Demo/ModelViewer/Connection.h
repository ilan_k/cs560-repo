/* Start Header -------------------------------------------------------
Copyright (C) 2014 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written consent of DigiPen Institute of Technology is prohibited.
File Name:		Connection.h
Purpose:		Class that contains the link between two objects (rope)
Language:		C++ and compiled in VS2012
Platform:		Visual Studio 2012, Windows 8
Project:		CS560
Author:			Ilan Keshet
Creation date:	20/12/2014
- End Header --------------------------------------------------------*/

#pragma once

#include "Component.h"

#define SPRING_K 40.0f
#define LENGTH	 4.0f

class Transform;
class Body;

class Connection : public Component
{
public:
	Connection() : Component(CID_Connection), tension(SPRING_K), restLength(LENGTH), fromTransform(0), toTransform(0){}

	~Connection();

	virtual std::string GetName() { return "Connection"; }

	virtual void Initialize();

	virtual Component* Clone()
	{
		Connection* connect = new Connection();

		connect->fromTransform = this->fromTransform;
		connect->toTransform = this->toTransform;
		connect->tension = this->tension;

		return new Connection();
	}

	void Initialize(Body* fromBody, Body* toBody);

	Transform *fromTransform;
	Transform *toTransform;

	Body *fromBody;
	Body *toBody;

	float tension;
	float restLength;
};