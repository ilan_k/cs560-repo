/* Start Header -------------------------------------------------------
Copyright (C) 2014 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written consent of DigiPen Institute of Technology is prohibited.
File Name:		PhysicsSystem.h
Purpose:	    Class that manages all the physics interaction of all Body Components and Connection commponents in scene
Language:		C++ and compiled in VS2012
Platform:		Visual Studio 2012, Windows 8
Project:		CS560
Author:			Ilan Keshet
Creation date:	20/12/2014
- End Header --------------------------------------------------------*/

#pragma once

#include "System.h"

class Body;
class Connection;

class PhysicsSystem : public System
{
public:
	PhysicsSystem();

	~PhysicsSystem();

	static PhysicsSystem* Get();

	//Applies forces to all bodies / updates velocities
	// / integrates positions of all bodies
	virtual void Update(float dt);

	virtual std::string GetName() { return "Physics"; }

	virtual void Initialize();

	virtual void SendMsg(Message * mess);

	void RegisterBody(Body* body);
	void UnregisterBody(int id);

	void RegisterConnection(Connection * connect);
	void UnregisterConnection(int id);


private:
	//Applies the Tension forces to the two
	//bodies that are connected to the connection component
	void ApplyTensionForces(Connection * connect);
	

	//Applies the Gravity force to a body
	void ApplyGravityForce(Body * body);

	std::unordered_map<int, Body*> bodies;

	std::unordered_map<int, Connection*> connections;
};