#pragma once

#include "LineDrawer.h"

typedef glm::vec3 Point;
typedef std::vector<Point> Pvector;

class Curve  
{
protected:
	static Pvector points;
	LineDrawer *lDrawer;

public:
	//This function is called whenever a new point is added to the curve
	virtual void recalculate() {}

	Curve(LineDrawer* lDrawer) : lDrawer(lDrawer){};
	virtual ~Curve() {};
	virtual void draw(int levelOfDetail = 20);

	void addPoint(glm::vec3 const &p);

	void addPointAndCalc(glm::vec3 const &p);

	void connectTheDots(glm::vec3 const &color = glm::vec3(1.0, 0.0, 0.0));

	void clearAllPoints();

	int numberOfPoints(){ return this->points.size(); }

	//Reserves an amount of size of total points
	void reserve(int size);
};