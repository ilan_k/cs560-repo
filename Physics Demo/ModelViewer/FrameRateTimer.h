#pragma once

namespace FrameRateTimer{
	void FrameRateTimerStart();

	void FrameRateTimerEnd();

	float GetFrameTime();
}