#include "Precompiled.h"
#include "GraphicsSystem.h"
#include "WindowsSystem.h"
#include "PhysicsSystem.h"
#include "InputSystem.h"

#include "ModelViewerCore.h"

//Screen dimension constants
const char WindowTitle[] = "Demo";
const int SCREEN_WIDTH = 840;
const int SCREEN_HEIGHT = 680;

int main(int argc, char** args){
	ModelViewerCore core;

	WindowsSystem *demo = new WindowsSystem(WindowTitle,SCREEN_WIDTH,SCREEN_HEIGHT);
	InputSystem* input = new InputSystem;


	PhysicsSystem* phy = new PhysicsSystem;
	GraphicsSystem *gra = new GraphicsSystem;

	core.AddSystem(demo);
	core.AddSystem(input);
	core.AddSystem(gra);
	core.AddSystem(phy);

	core.Initialize();

	core.GameLoop();

	core.DestroySystems();

	return 0;
}