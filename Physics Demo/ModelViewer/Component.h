/* Start Header -------------------------------------------------------
Copyright (C) 2014 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written consent of DigiPen Institute of Technology is prohibited.
File Name:		Component.cpp
Purpose:		Base Class functionality for components
Language:		C++ and compiled in VS2012
Platform:		Visual Studio 2012, Windows 8
Project:		CS560
Author:			Ilan Keshet
Creation date:	20/12/2014
- End Header --------------------------------------------------------*/

#pragma once

#include "Object.h"

class Component
{
public: 
	Component(COMPID id) : id(id), owner(0) {};

	virtual std::string GetName() = 0;

	virtual void Initialize() = 0;

	virtual Component* Clone() = 0;

	const COMPID id;

	Object* owner;

private:
	Component();
};