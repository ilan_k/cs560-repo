#pragma once

#include "AntTweakBar.h"

#define ASSET_DIRECTORY "..\\..\\SkyEngine\\Game\\Assets"
#define SHADER_DIRECTORY "..\\..\\SkyEngine\\Game\\Shaders"

extern bool showGround;
extern bool fillPolygon;

extern glm::vec3 redAnchor;
extern glm::vec3 blueAnchor;
extern glm::vec3 greenAnchor;
extern glm::vec3 yellowAnchor;

class Transform;

class LevelEditor
{
public:
	LevelEditor();

	//Should be destroyed before Graphics
	~LevelEditor();

	void Initialize();

	void SetRedAnchorTransform(Transform * t);

	void SetBlueAnchorTransform(Transform * t);

	void SetGreenAnchorTransform(Transform * t);

	void SetYellowAnchorTransform(Transform * t);

	void Refresh() const;

	void Draw();

	static LevelEditor* Get();

private:
	void AddFileList(string const &DisplayLabel, string twVariableName, vector<string> const& fileList, int * editorVariable);

	void InitializeBar();

	TwBar *bar;
};