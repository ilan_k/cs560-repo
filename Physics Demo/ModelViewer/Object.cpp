/* Start Header -------------------------------------------------------
Copyright (C) 2014 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written consent of DigiPen Institute of Technology is prohibited.
File Name:		Object.cpp
Purpose:		Container class that contains a list of components
Language:		C++ and compiled in VS2012
Platform:		Visual Studio 2012, Windows 8
Project:		CS560
Author:			Ilan Keshet
Creation date:	20/12/2014
- End Header --------------------------------------------------------*/

#include "Precompiled.h"
#include "Component.h"
#include "Object.h"

Object::~Object()
{
	for(auto it = components.begin(); it != components.end(); ++it)
	{
		delete it->second;
	}

	components.clear();
}

void Object::AddComponent(Component* comp)
{
	components[comp->id] = comp;
}

void Object::Initialize()
{
	for(auto it = components.begin(); it != components.end(); ++it)
	{
		it->second->owner = this;
		it->second->Initialize();
	}
}

Object* Object::Clone()
{
	Object *obj = new Object(this->archetypeName);

	for(auto it = components.begin(); it != components.end(); ++it)
	{
		obj->AddComponent(it->second->Clone());
	}

	return obj;
}