/* Start Header -------------------------------------------------------
Copyright (C) 2014 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written consent of DigiPen Institute of Technology is prohibited.
File Name:		AnimationHandler.cpp
Purpose:		Class for Running animations of a model -- can choose which animation to use
Language:		C++ and compiled in VS2012
Platform:		Visual Studio 2012, Windows 8
Project:		CS560
Author:			Ilan Keshet
Creation date:	16/9/2014
- End Header --------------------------------------------------------*/

#include "Precompiled.h"
#include "AnimationHandler.h"
#include "Mesh.h"

AnimationHandler::AnimationHandler(Skeleton* skel) :	skel(skel), 
														currentTime(0),
														animationCyclesPerSecond(1),
														currentRunningAnimation(-1)
														
{
	jointMatrixBuffer.resize(skel->GetNumberOfJoints());
}

AnimationHandler::~AnimationHandler()
{
	if(skel)
	{
		delete skel;
	}

	for(auto it = animations.begin(); it != animations.end(); ++it)
	{
		delete *it;
	}
}


/******************************************************************************/
/*!
	Draws the model by computing all the skinning matrices, and then sending them
	into the shader. 
*/
/******************************************************************************/
void AnimationHandler::Draw(GLSLshader* shader, Mesh const& mesh) const
{
	vector<glm::mat4> skinningMatrices;
	skinningMatrices.resize(this->jointMatrixBuffer.size());

	vector<Joint> const& joints = this->skel->GetJoints();

	for(unsigned int i = 0; i < joints.size(); ++i)
	{
		//skinningMatrices take a vertice from Model Space to the bones local space
		//Then take it to the space that the bone is currently in, in the animation
		skinningMatrices[i] = this->jointMatrixBuffer[i] * joints[i].invBindTransform; 
	}

	GLfloat const * start = &skinningMatrices[0][0][0];

	glUniformMatrix4fv((*shader)("skinningMatrices"), joints.size(), GL_FALSE, start);
	glDrawElements(GL_TRIANGLES, mesh.numIndices, GL_UNSIGNED_INT, 0);
}

/******************************************************************************/
/*!
	Draws the Skeleton -- the second parameter decides if it should be in bindpose or not
*/
/******************************************************************************/
void AnimationHandler::DrawSkeleton(LineDrawer *lDrawer, bool bindPose) const
{
	if(skel){
		if(bindPose)
		{
			skel->DrawBindPose(lDrawer);
		}
		else
		{
			skel->Draw(lDrawer, this->jointMatrixBuffer);
		}
	}
}

void AnimationHandler::Restart()
{
	currentTime = 0;

	animations[currentRunningAnimation]->Restart();
}

/******************************************************************************/
/*!
	Updates the current running animation with dt.
	If the currentTime passes duration -- it gets reset back to 0, and 
	restarts the animations
*/
/******************************************************************************/
void AnimationHandler::Update(float dt)
{
	if(currentRunningAnimation != -1)
	{
		float duration = animations[currentRunningAnimation]->GetDuration();

		//Update the current time
		currentTime += dt * duration * animationCyclesPerSecond;

		if(currentTime > duration)
		{
			currentTime = fmod(duration - currentTime, duration);
			animations[currentRunningAnimation]->Restart();
		}

		Animation &anim =  *(animations[currentRunningAnimation]);

		anim.Update(currentTime);
		skel->ComputeJointMatrixBuffer(anim, jointMatrixBuffer, currentTime);
	}
}

bool AnimationHandler::UpdateUntilRestart(float dt)
{
	if(currentRunningAnimation != -1)
	{
		float duration = animations[currentRunningAnimation]->GetDuration();

		if(currentTime != 0)
		{
			//Update the current time
			currentTime += dt * duration * animationCyclesPerSecond;

			if(currentTime > duration)
			{
				currentTime = 0;
				animations[currentRunningAnimation]->Restart();
			}

			Animation &anim =  *(animations[currentRunningAnimation]);

			anim.Update(currentTime);
			skel->ComputeJointMatrixBuffer(anim, jointMatrixBuffer, currentTime);
		}
	}

	return currentTime == 0;
}

bool AnimationHandler::UpdateUntilKeyFrame(float dt, vector<int> const &keyframesToStopAt)
{
	if(currentRunningAnimation != -1)
	{
		float duration = animations[currentRunningAnimation]->GetDuration();
		float nearestKeyFrameDis = 9999999;
		float keyFrameTime;

		//Determine what keyframe to stop at -- the one nearest to the currentTime
		for(auto it = keyframesToStopAt.begin(); it != keyframesToStopAt.end(); ++it)
		{
			float tempKeyTime = animations[currentRunningAnimation]->GetKeyFrameTime(*it);

			if(tempKeyTime > currentTime)
			{
				if(tempKeyTime - currentTime < nearestKeyFrameDis)
				{
					keyFrameTime = tempKeyTime;
					nearestKeyFrameDis = tempKeyTime - currentTime;
				}
			}
			else
			{
				if(duration - currentTime + tempKeyTime < nearestKeyFrameDis)
				{
					keyFrameTime = tempKeyTime;
					nearestKeyFrameDis = duration - currentTime + tempKeyTime;
				}
			}
		}

		if(currentTime != keyFrameTime)
		{
			bool before = currentTime < keyFrameTime;

			//Update the current time
			currentTime += dt * duration * animationCyclesPerSecond;

			if(currentTime > duration)
			{
				currentTime = 0;
				animations[currentRunningAnimation]->Restart();
			}

			if(before && currentTime > keyFrameTime)
			{
				currentTime = keyFrameTime;
			}

			Animation &anim =  *(animations[currentRunningAnimation]);

			anim.Update(currentTime);
			skel->ComputeJointMatrixBuffer(anim, jointMatrixBuffer, currentTime);
		}

		return currentTime == keyFrameTime;
	}

	return false;
}

void AnimationHandler::UpdateBlend(int animId1, int animId2, float blendFactor, float dt)
{
	Animation *anim1 = animations[animId1];
	Animation *anim2 = animations[animId2];

	assert(anim1->GetDuration() == 1.0f && anim2->GetDuration() == 1.0f);

	float duration = 1.0f;

	currentTime += dt * animationCyclesPerSecond;

	if(currentTime > duration)
	{
		currentTime = 0;
		anim1->Restart();
		anim2->Restart();
	}

	anim1->Update(currentTime);
	anim2->Update(currentTime);

	skel->ComputeBlendJointMatrixBuffer(*anim1, *anim2, blendFactor, jointMatrixBuffer, currentTime);
}

void AnimationHandler::UpdateIk(float k, std::map<int, JointPose> const & ikJointPoseMap)
{
	if(currentRunningAnimation != -1)
	{
		Animation &anim =  *(animations[currentRunningAnimation]);

		anim.Update(currentTime);
		skel->ComputeIkJointMatrixBuffer(anim, jointMatrixBuffer, currentTime, k, ikJointPoseMap);
	}	
}

/******************************************************************************/
/*!
	Adds an additional animation to the animation list -- and switches to that animation
*/
/******************************************************************************/
void AnimationHandler::AddAnimation(Animation* anim)
{
	animations.push_back(anim);
	
	anim->Initialize();
	
	currentRunningAnimation = animations.size() - 1;
}

/******************************************************************************/
/*!
	Sets the current running animation by given animation ID
*/
/******************************************************************************/
void AnimationHandler::SetCurrentAnimationId(int animation)
{
	assert(animation >= 0 && animation < (int)animations.size());

	currentRunningAnimation = animation;
}


/******************************************************************************/
/*!
	Gets the current animation ID
*/
/******************************************************************************/
int AnimationHandler::GetCurrentAnimationId()
{
	return currentRunningAnimation;
}

bool AnimationHandler::GetJointChain(std::string const &handle, std::string const &root, vector<Joint*> &jointsOut)
{
	return skel->GetJointChain(handle, root, jointsOut);
}

JointPose AnimationHandler::GetJointPose(int joint)
{
	return skel->GetJointPose(joint, *(animations[currentRunningAnimation]), currentTime);
}

glm::vec3 AnimationHandler::GetJointModelPosition(int joint)
{
	glm::mat4 const& globalMatrix = jointMatrixBuffer[joint];

	return glm::vec3(globalMatrix[3][0], globalMatrix[3][1], globalMatrix[3][2]);
}

glm::mat4 AnimationHandler::GetJointMatrix(int joint)
{
	return jointMatrixBuffer[joint];
}