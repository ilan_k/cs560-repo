#pragma once

#include <string>
#include <vector>

class ISerializer
{
public:
						ISerializer() {}
	virtual				~ISerializer() {}

	virtual bool		OpenFile(std::string const & filename) = 0;
	virtual void		CloseFile() = 0;
	virtual int			GetBlockCount() = 0;

	virtual std::string		GetString(int blockIndex, std::string const & setting, std::string const & default) = 0;
	virtual int			GetInt(int blockIndex, std::string const & setting, int default = 0) = 0;
	virtual float		GetFloat(int blockIndex, std::string const & setting, float default = 0.0f) = 0;
	virtual bool		GetWordList(int blockIndex, std::string const & setting, std::vector<std::string> & words, char token = ',') = 0;
};
