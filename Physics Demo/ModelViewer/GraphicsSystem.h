/* Start Header -------------------------------------------------------
Copyright (C) 2014 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written consent of DigiPen Institute of Technology is prohibited.
File Name:		GraphicsSystem.h
Purpose:		Graphics System for Drawing the the model
				It also does all the heavy work under the hood as well
Language:		C++ and compiled in VS2012
Platform:		Visual Studio 2012, Windows 8
Project:		CS560
Author:			Ilan Keshet
Creation date:	16/9/2014
- End Header --------------------------------------------------------*/

#pragma once

#include "System.h"
#include "Texture.h"
#include "GLSLshader.h"
#include "LevelEditor.h"
#include "TextSerialization.h"
#include "LineDrawer.h"

class Model;
class Render;
class Connection;
class Transform;
struct Joint;
struct JointPose;

class GraphicsSystem : public System
{
public:
	GraphicsSystem();
	~GraphicsSystem();

	static GraphicsSystem* Get();

	void							Initialize();
	void							Update(float dt);
	string						    GetName() { return "Graphics"; }

	void							LoadBinModel(string filepath, Model *&modelToLoad);
	void							LoadTexture(string textureFile, Texture *&tex);

	void                            SendMsg(Message * mess);

	void							RegisterRender(Render * rend);
	void							UnregisterRender(int id);

	void							RegisterConnection(Connection * connect);
	void							UnregisterConnection(int id);

private:
	string							binFile;
	string							textureFile;
	LevelEditor						editor;

	Transform*						planeTransform;

	glm::mat4						modelMatrix;
	glm::mat4						viewMatrix;
	glm::mat4                       proj;

	void                            SetStaticVertexAttributes(GLuint, unsigned int size);
	void                            SetSkinnedVertexAttributes(GLuint, unsigned int size);

	void							LoadMultipleAnimation(int blockid, TextSerializer &ts);
	void                            InitializeDebugShader();
	void                            InitializeStaticShader();
	void                            InitializeSkinnedShader();

	void							UpdateModelMatrix(Transform const& modelTransform);

	void                            RenderModel(Model* model, Texture *tex, bool showModel);
	void							RenderSkeleton(Model *model);

	float							zoom;
	float							eyeXRotation;
	float							eyeYRotation;
	float							sceneCenterX;
	float							sceneCenterY;

	int                             mouseX;
	int                             mouseY;
	int                             mouseButton;
	bool                            mouseButtonDown;

	GLSLshader                      staticShader;
	GLSLshader                      skinnedShader;

	LineDrawer						lDrawer;

	std::unordered_map<int, Render*> renders;
	std::unordered_map<int, Connection*> connections;
};

