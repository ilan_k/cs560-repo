/* Start Header -------------------------------------------------------
Copyright (C) 2014 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written consent of DigiPen Institute of Technology is prohibited.
File Name:		Connection.cpp
Purpose:		Class that contains the link between two objects (rope)
Language:		C++ and compiled in VS2012
Platform:		Visual Studio 2012, Windows 8
Project:		CS560
Author:			Ilan Keshet
Creation date:	20/12/2014
- End Header --------------------------------------------------------*/

#include "Precompiled.h"
#include "Connection.h"
#include "Body.h"
#include "Transform.h"
#include "GraphicsSystem.h"
#include "PhysicsSystem.h"

void Connection::Initialize()
{
	GraphicsSystem::Get()->RegisterConnection(this);
	PhysicsSystem::Get()->RegisterConnection(this);
}

Connection::~Connection()
{
	GraphicsSystem::Get()->UnregisterConnection(this->owner->GetId());
	PhysicsSystem::Get()->UnregisterConnection(this->owner->GetId());
}

void Connection::Initialize(Body* fromBody, Body* toBody)
{
	this->fromBody = fromBody;
	this->toBody = toBody;

	this->fromTransform = fromBody->owner->has(Transform);
	this->toTransform = toBody->owner->has(Transform);
}