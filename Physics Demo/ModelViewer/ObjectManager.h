/* Start Header -------------------------------------------------------
Copyright (C) 2014 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written consent of DigiPen Institute of Technology is prohibited.
File Name:		ObjectManager.h
Purpose:	    class that managers (contains) all the objects and archetypes of the scene
Language:		C++ and compiled in VS2012
Platform:		Visual Studio 2012, Windows 8
Project:		CS560
Author:			Ilan Keshet
Creation date:	20/12/2014
- End Header --------------------------------------------------------*/

#pragma once

class Object;
class Model;
class Texture;

class ObjectManager
{
public:
	ObjectManager();

	~ObjectManager();

	static ObjectManager* Get();

	Object* CreateObject(std::string archetypeName);

	bool AddArchetype(Object* obj);

	bool LoadModel(std::string filepath);

	bool LoadTexture(std::string filepath);

	Model* GetModel(std::string modelName);

	Texture* GetTexture(std::string textureName);

	Object* FindObjectWithArchetypeName(std::string archetypeName);

private:
	//list of all objects in the scene
	std::unordered_map<int, Object*> objects;

	//list of all archetypes
	std::unordered_map<std::string, Object*> archetypes;

	//list of all models
	std::unordered_map<std::string, Model*> models;

	//list of all textures
	std::unordered_map<std::string, Texture*> textures;
};