#pragma once

class Texture{
public:
	Texture(string textureFile);

	~Texture();

	unsigned int getTexture();

	unsigned int getImageWidth();

	unsigned int getImageHeight();

private:
	Texture();

	unsigned int textureID;
	unsigned int width;
	unsigned int height;
};