/* Start Header -------------------------------------------------------
Copyright (C) 2014 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written consent of DigiPen Institute of Technology is prohibited.
File Name:		Model.h
Purpose:		Class for storing / loading, and animating a model
Language:		C++ and compiled in VS2012
Platform:		Visual Studio 2012, Windows 8
Project:		CS560
Author:			Ilan Keshet
Creation date:	16/9/2014
- End Header --------------------------------------------------------*/

#pragma once

#include "LineDrawer.h"
#include "Mesh.h"
#include "AnimationHandler.h"

enum ModelType
{
	Static,
	Skinned
};

class GLSLshader;

class Model
{
public:
	Model(std::string modelName);
	~Model();

	std::string GetName() { return modelName; }

	void SetAnimationCyclesPerSecond(float cycles);

	//Updates the animation
	void Update(float dt);

	bool UpdateUntilRestart(float dt);

	bool UpdateUntilKeyFrame(float dt, vector<int> const &keyframesToStopAt);

	void UpdateBlend(int animId1, int animId2, float blendFactor, float dt);

	void UpdateIk(float k, std::map<int, JointPose> const & ikJointPoseMap);

	//Draws the actual model -- after interpolation
	void Draw(GLSLshader* shader) const;

	//Draws the model's mesh in Bind pose
	void DrawBindPose(GLSLshader* shader) const;

	//Assumes that the correct Debug Shader for lines
	//has been set up
	void DrawBindPoseSkeleton(LineDrawer *lDrawer) const; //Draws just the Skeleton in BindPose
	void DrawSkeleton(LineDrawer *lDrawer) const; //Draws the skeleton 

	//Loads a bin file
	static Model* LoadFile(string filename);

	//Loads an animation from a file
	Animation* LoadAnimation(string filename);

	void RestartAnimation();

	Mesh const* GetMesh(){ return mesh; }
	
	bool IsSkinned() { return modelType == Skinned; }

	int TotalAnimations();

	void SetCurrentAnimationId(int animation);

	int GetCurrentAnimationId();

	bool GetJointChain(std::string const &handle, std::string const &root, vector<Joint*> &jointsOut);

	JointPose GetJointPose(int joint);

	glm::mat4 GetJointMatrix(int joint);

	glm::vec3 GetJointModelPosition(int joint);

	GLuint vao;
	GLuint vbo, ebo;

private:
	ModelType modelType;
	AnimationHandler* animationHandler;
	Mesh* mesh;
	std::string modelName;
};

