#version 330
in vec3 OutputNormal;
in vec3 OutputTangent;
in vec3 OutputTex;

out vec4 outColor;

uniform sampler2D tex;

uniform mat4 normalMatrix;

void main(){
	vec4 color = texture(tex, OutputTex.st).rgba;

	//if(color.a < 1.0)
	//{
	//	outColor = vec4(1.0, 0.0, 0.0, 1.0);
	//}
	//else
	//{
	//	outColor = vec4(0.0, 1.0, 0.0, 1.0);
	//}

    outColor = vec4(color);
}