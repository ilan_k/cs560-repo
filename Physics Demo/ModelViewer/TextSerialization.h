#pragma once

#include "Serialization.h"

class SettingsBlock;

class TextSerializer : public ISerializer
{
public:
							TextSerializer();
							~TextSerializer();

	virtual bool			OpenFile(std::string const & filename);
	virtual void			CloseFile();
	virtual int				GetBlockCount();
	virtual int				GetBlockCount(int blockIndex);

	void					Merge(TextSerializer const & other);

	virtual std::string		GetBlockName(int blockIndex);
	int						GetBlockId(std::string const & blockname, bool createIfNotFound = false);
	virtual std::string		GetString(int blockIndex, std::string const & setting, std::string const & default = "");
	virtual bool			GetBool(int blockIndex, std::string const & setting, bool default = false);
	virtual int				GetInt(int blockIndex, std::string const & setting, int default = 0);
	virtual float			GetFloat(int blockIndex, std::string const & setting, float default = 0.0f);
	virtual bool			GetWordList(int blockIndex, std::string const & setting, std::vector<std::string> & words, char token = ',');
	void					GetSettings(int blockIndex, std::vector<std::string> & settings);

	void					SetString(int blockIndex, std::string const & setting, std::string const & strValue);
	void					SetInt(int blockIndex, std::string const & setting, int nValue);
	void					SetFloat(int blockIndex, std::string const & setting, float fValue);
	bool					SetWordList(int blockIndex, std::string const & setting, std::vector<std::string> const & words, char token = ',');
	void					SaveFile();

	static void				GetOverriddenString(std::string const & original, std::string & header, std::vector<std::string> & overriddens, char token = '|');

	static void				TrimSpace(std::string & str);
	static void				ToLower(std::string & str);

private:

	std::string						m_filename;
	std::vector<SettingsBlock *>	m_settingBlocks;
};
