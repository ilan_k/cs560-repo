#include "Precompiled.h"
#include "Skeleton.h"
#include "LineDrawer.h"
#include "Animation.h"

/******************************************************************************/
/*!
	Reads all the skeleton information from the binary file. 
	Handles the 2 different input binary files -- mine and chris peters
*/
/******************************************************************************/
void Skeleton::Read(BinFileDeseralizer &deser, bool oldFileVersion)
{
	numberOfJoints = 0;

	deser.Read(numberOfJoints);

	joints.resize(numberOfJoints);

	Quaternion unusedBindRotation;

	glm::vec3 invBindPosition;
	Quaternion invBindRotation;
	glm::vec3 invBindScale(1.0, 1.0, 1.0);

	for(unsigned int i = 0; i < numberOfJoints; i++)
	{
		Joint &joint = joints[i];

		deser.Read(joint.name);
		deser.Read(joint.parentIndex);
		
		//The following line is only used when drawing the skeleton in bind pose
		//it is not needed anywhere else
		deser.Read(joint.BindPosition);

		if(oldFileVersion)
		{
			//The following line is READING IN AN UNNEEDED VARIABLE OF THE BINARY FILE
			deser.Read(unusedBindRotation);
		}

		deser.Read(invBindPosition);
		deser.Read(invBindRotation);

		if(!oldFileVersion)
		{
			//My new version includes scaling on joints
			deser.Read(invBindScale);
		}
		else
		{
			//Set invBindScale to 1
			invBindScale = glm::vec3(1.0, 1.0, 1.0);
		}

		//Build the inverse bind transform -- used for the skinning process
		joint.invBindTransform = BuildTransform(invBindPosition, invBindRotation, invBindScale);
	}
}

/******************************************************************************/
/*!
	Initializes the skeleton to make all parents reference all its children
*/
/******************************************************************************/
void Skeleton::Initialize()
{
	for(unsigned int i = 0; i < numberOfJoints; ++i)
	{
		Joint* joint = &joints[i];
		joint->index = i;

		if(joint->parentIndex != -1)
		{
			joints[joint->parentIndex].children.push_back(joint);
		}
		else
		{
			rootJoints.push_back(joint);
		}
	}
}

/******************************************************************************/
/*!
	Draws the skeleton in bind pose -- make sure that debug shader is being used
*/
/******************************************************************************/
void Skeleton::DrawBindPose(LineDrawer *lDrawer) const
{
	for(auto it = joints.begin(); it != joints.end(); ++it)
	{
		const Joint& joint = *it;

		for(auto it2 = joint.children.begin(); it2 != joint.children.end(); ++it2)
		{
			const Joint& joint2 = *(*it2);

			lDrawer->DrawLine(joint.BindPosition, joint2.BindPosition);
		}
	}
}

/******************************************************************************/
/*!
	Draws the skeleton -- make sure that debug shader is being used
*/
/******************************************************************************/
void Skeleton::Draw(LineDrawer *lDrawer, vector<glm::mat4> const &jointMatrixBuffer) const
{
	for(unsigned int parentIndex = 0; parentIndex < numberOfJoints; parentIndex++)
	{
		glm::mat4 const& parentMatrix = jointMatrixBuffer[parentIndex];

		glm::vec3 parentPosition(parentMatrix[3][0], parentMatrix[3][1], parentMatrix[3][2]);

		const Joint& parentJoint = joints[parentIndex];

		for(unsigned int c = 0; c < parentJoint.children.size(); c++)
		{
			int childIndex = parentJoint.children[c]->index;

			glm::mat4 const& childMatrix = jointMatrixBuffer[childIndex];

			glm::vec3 childPosition(childMatrix[3][0], childMatrix[3][1], childMatrix[3][2]);

			lDrawer->DrawLine(parentPosition, childPosition);
		}
	}
}

void Skeleton::InterpolateKeyFrame(Animation& anim, int boneId, float currentTime, Quaternion &interpolatedQuat, glm::vec3 &interpolatedTrans, glm::vec3 &interpolatedScale)
{
	JointPose const & currentJointPose = anim.GetCurrentJointPose(boneId);
	JointPose const * nextJointPose = anim.GetNextJointPose(boneId);

	if(nextJointPose && currentTime >= currentJointPose.time)
	{
		//t = 0 is mapped to CurrentJointPose
		//t = 1 is mapped to nextJointPose
		float t = (currentTime - currentJointPose.time) / (nextJointPose->time - currentJointPose.time);

		//Interpolation of all VQS
		interpolatedQuat = Slerp(currentJointPose.Rotation, nextJointPose->Rotation, t);
		interpolatedTrans = (1.0f - t) * currentJointPose.Translation + t * nextJointPose->Translation;
		interpolatedScale = (1.0f - t) * currentJointPose.Scale + t * nextJointPose->Scale;
	}
	else
	{
		interpolatedQuat  = currentJointPose.Rotation;
		interpolatedTrans = currentJointPose.Translation;
		interpolatedScale = currentJointPose.Scale;
	}
}

/******************************************************************************/
/*!
	Computes the World Transformation matrix for each joint 
	which is = parent * local

	the local transform is computed based on interpolated two joint poses,
	and then building a transformation
*/
/******************************************************************************/
void Skeleton::ComputeJointMatrixBuffer(Animation &animation, vector<glm::mat4> &jointMatrixbuffer, float currentTime)
{
	glm::mat4 identity;

	for(unsigned int i = 0; i < numberOfJoints; i++)
	{
		Quaternion interpolatedQuat;
		glm::vec3 interpolatedTrans;
		glm::vec3 interpolatedScale;

		InterpolateKeyFrame(animation, i, currentTime, interpolatedQuat, interpolatedTrans, interpolatedScale);

		glm::mat4 local = BuildTransform(interpolatedTrans, interpolatedQuat, interpolatedScale);

		glm::mat4 parent;

		if(joints[i].parentIndex != -1)
		{
			assert(joints[i].parentIndex >= 0 && joints[i].parentIndex < (int)jointMatrixbuffer.size());
			parent = jointMatrixbuffer[joints[i].parentIndex]; //Reuse the previously computed parent matrix
		}
		else
		{
			parent = identity;
		}

		jointMatrixbuffer[i] = parent * local;
	}
}

JointPose Skeleton::GetJointPose(int joint, Animation &animation, float currentTime)
{
	JointPose pose;

	InterpolateKeyFrame(animation, joint, currentTime, pose.Rotation, pose.Translation, pose.Scale);

	return pose;
}

void Skeleton::ComputeBlendJointMatrixBuffer(Animation &anim1, Animation &anim2, float b, vector<glm::mat4> &jointMatrixbuffer, float currentTime)
{
	glm::mat4 identity;

	for(unsigned int i = 0; i < numberOfJoints; i++)
	{
		Quaternion interpolatedQuat1;
		glm::vec3 interpolatedTrans1;
		glm::vec3 interpolatedScale1;

		InterpolateKeyFrame(anim1, i, currentTime, interpolatedQuat1, interpolatedTrans1, interpolatedScale1);

		Quaternion interpolatedQuat2;
		glm::vec3 interpolatedTrans2;
		glm::vec3 interpolatedScale2;

		InterpolateKeyFrame(anim2, i, currentTime, interpolatedQuat2, interpolatedTrans2, interpolatedScale2);

		//Funny bug below
		//Quaternion blendedQuat = (1.0f - b) * interpolatedQuat1.Get() + b * interpolatedQuat2.Get();

		Quaternion blendedQuat = Slerp(interpolatedQuat1, interpolatedQuat2, b);
		glm::vec3 blendedTrans = (1.0f - b) * interpolatedTrans1 + b * interpolatedTrans2;
		glm::vec3 blendedScale = (1.0f - b) * interpolatedScale1 + b * interpolatedScale2;

		glm::mat4 local = BuildTransform(blendedTrans, blendedQuat, blendedScale);

		glm::mat4 parent;

		if(joints[i].parentIndex != -1)
		{
			assert(joints[i].parentIndex >= 0 && joints[i].parentIndex < (int)jointMatrixbuffer.size());
			parent = jointMatrixbuffer[joints[i].parentIndex]; //Reuse the previously computed parent matrix
		}
		else
		{
			parent = identity;
		}

		jointMatrixbuffer[i] = parent * local;
	}	
}

void Skeleton::ComputeIkJointMatrixBuffer(Animation &animation, vector<glm::mat4> &jointMatrixbuffer, float currentTime, 
										  float k, std::map<int, JointPose> const & ikJointPoseMap)
{
	glm::mat4 identity;

	for(unsigned int i = 0; i < numberOfJoints; i++)
	{
		Quaternion interpolatedQuat;
		glm::vec3 interpolatedTrans;
		glm::vec3 interpolatedScale;

		InterpolateKeyFrame(animation, i, currentTime, interpolatedQuat, interpolatedTrans, interpolatedScale);

		auto pose = ikJointPoseMap.find(i);

		if(pose != ikJointPoseMap.end())
		{
			JointPose const &jPose = pose->second;

			interpolatedQuat = Slerp(interpolatedQuat, jPose.Rotation, k);
			interpolatedTrans = (1.0f - k) * interpolatedTrans + k * jPose.Translation;
			interpolatedScale = (1.0f - k) * interpolatedScale + k * jPose.Scale;
		}

		glm::mat4 local = BuildTransform(interpolatedTrans, interpolatedQuat, interpolatedScale);

		glm::mat4 parent;

		if(joints[i].parentIndex != -1)
		{
			assert(joints[i].parentIndex >= 0 && joints[i].parentIndex < (int)jointMatrixbuffer.size());
			parent = jointMatrixbuffer[joints[i].parentIndex]; //Reuse the previously computed parent matrix
		}
		else
		{
			parent = identity;
		}

		jointMatrixbuffer[i] = parent * local;
	}
}

bool Skeleton::GetJointChain(std::string const &handle, std::string const &root, vector<Joint*> &jointsOut)
{
	jointsOut.clear();
	Joint* joint = NULL;

	for(auto it = joints.begin(); it != joints.end(); ++it)
	{
		if(it->name == handle)
		{
			joint = &(*it);
			jointsOut.push_back(joint);
			break;
		}
	}

	if(!joint)
	{
		return false;
	}

	while(joint->name != root)
	{
		int parentId = joint->parentIndex;

		if(parentId == -1)
		{
			//Node with Root name was never found
			return false;
		}
		else
		{
			joint = &joints[parentId];
			jointsOut.push_back(joint);
		}
	}

	return true;
}