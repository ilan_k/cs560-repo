/* Start Header -------------------------------------------------------
Copyright (C) 2014 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written consent of DigiPen Institute of Technology is prohibited.
File Name:		Body.cpp
Purpose:		Body component which adds Physics to an object
Language:		C++ and compiled in VS2012
Platform:		Visual Studio 2012, Windows 8
Project:		CS560
Author:			Ilan Keshet
Creation date:	20/12/2014
- End Header --------------------------------------------------------*/

#include "Precompiled.h"
#include "Transform.h"
#include "Body.h"
#include "PhysicsSystem.h"

void Body::Initialize()
{
	trans = owner->has(Transform);

	PhysicsSystem::Get()->RegisterBody(this);
}

Body::~Body()
{
	PhysicsSystem::Get()->UnregisterBody(this->owner->GetId());
}