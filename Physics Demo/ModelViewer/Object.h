/* Start Header -------------------------------------------------------
Copyright (C) 2014 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written consent of DigiPen Institute of Technology is prohibited.
File Name:		Object.h
Purpose:		Container class that contains a list of components
Language:		C++ and compiled in VS2012
Platform:		Visual Studio 2012, Windows 8
Project:		CS560
Author:			Ilan Keshet
Creation date:	20/12/2014
- End Header --------------------------------------------------------*/

#pragma once

class Component;

enum COMPID
{
	CID_Transform,
	CID_Render,
	CID_Body,
	CID_Connection
};

class Object
{
public:
	Object(std::string archName) : archetypeName(archName)
	{
		static int uniqueId = 0;
		objectId = uniqueId++;
	}

	~Object();

	void AddComponent(Component* comp);

	template<typename T>
	T* GetComponentType(COMPID id)
	{
		return static_cast<T*>(components[id]);
	}

	std::string GetName()
	{
		return archetypeName;
	}

	int GetId()
	{
		return objectId;
	}

	void Initialize();

	Object* Clone();

private:
	friend class ObjectManager;
	std::unordered_map<COMPID, Component*> components;

	int objectId;
	std::string archetypeName;
};

#define has(x) GetComponentType<x>(CID_##x)