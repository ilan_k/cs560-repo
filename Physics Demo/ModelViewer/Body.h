/* Start Header -------------------------------------------------------
Copyright (C) 2014 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written consent of DigiPen Institute of Technology is prohibited.
File Name:		Body.h
Purpose:		Body component which adds Physics to an object
Language:		C++ and compiled in VS2012
Platform:		Visual Studio 2012, Windows 8
Project:		CS560
Author:			Ilan Keshet
Creation date:	20/12/2014
- End Header --------------------------------------------------------*/

#pragma once

#include "Component.h"

class Transform;

class Body : public Component
{
public:
	Body() : Component(CID_Body), trans(0), isStatic(false), body1Connection(0),
		body2Connection(0), mass(0.5f), velocity(0), netForce(0){}

	~Body();

	virtual std::string GetName() { return "Body"; }

	virtual void Initialize();

	virtual Component* Clone()
	{
		return new Body();
	}

	Transform * trans;
	bool isStatic;

	float mass;

	glm::vec3 netForce;

	glm::vec3 velocity;

	Body * body1Connection;
	Body * body2Connection;
};