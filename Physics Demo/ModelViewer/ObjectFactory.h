/* Start Header -------------------------------------------------------
Copyright (C) 2014 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written consent of DigiPen Institute of Technology is prohibited.
File Name:		ObjectFactory.h
Purpose:		Class that creates all the objects in the scene
Language:		C++ and compiled in VS2012
Platform:		Visual Studio 2012, Windows 8
Project:		CS560
Author:			Ilan Keshet
Creation date:	20/12/2014
- End Header --------------------------------------------------------*/

#pragma once

class ObjectManager;

class ObjectFactory
{
public:
	//Initializes the scene
	static void Initialize(ObjectManager* objManager);

private:
	ObjectFactory();


	//Creates 1 row of the bridge
	static void CreateBridgeRow(ObjectManager* objManager, glm::vec3 position, std::vector<Object*> &nodes, 
									std::string &anchor1Model, std::string &anchor2Model);

	//Creates a Sphere to the list of archetpyes in ObjectManager
	static void CreateSphereToArchetypeList(ObjectManager* objManager, std::string &textureName, std::string &objName);
};