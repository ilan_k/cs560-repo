/* Start Header -------------------------------------------------------
Copyright (C) 2014 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written consent of DigiPen Institute of Technology is prohibited.
File Name:		Mesh.h
Purpose:		Class for storing the Mesh data
Language:		C++ and compiled in VS2012
Platform:		Visual Studio 2012, Windows 8
Project:		CS560
Author:			Ilan Keshet
Creation date:	16/9/2014
- End Header --------------------------------------------------------*/

#pragma once

#include "BinAsset.h"

//Vertex for Static Model
struct StaticModelVertex
{
	glm::vec3		pos;
	glm::vec3		norm;
	glm::vec2		tex;

	//This is used for Normal Mapping
	glm::vec3		tangent;
};

//Vertex for Skinned Models
struct SkinnedModelVertex
{
	glm::vec3		pos;
	glm::vec3		norm;
	glm::vec2		tex;

	float			boneWeights[4];
	unsigned char	boneIndices[4];

	//This is used for Normal Mapping
	glm::vec3		tangent;
};

//Underlying Mesh holding all the vertices in format that is easy to
//send to the shader
class Mesh : public BinAsset
{
public:
	Mesh() : vertexType(0), indices(0), numIndices(0), numVertices(0), vertexBufferData(NULL){}

	void Read(BinFileDeseralizer& file, bool oldFileVersion);

	unsigned int		vertexType;
	unsigned int *		indices;
	unsigned int		numIndices;
	unsigned int		numVertices;
	void *				vertexBufferData;

	unsigned int		numTriangles;
};