#version 330
in layout(location=0) vec3 position;
in layout(location=1) vec3 normal;
in layout(location=2) vec3 tex;
in layout(location=3) vec4 boneWeights;
in layout(location=4) ivec4 boneIndices;
in layout(location=5) vec3 tangent;

out vec3 OutputTangent;
out vec3 OutputNormal;
out vec3 OutputTex;

uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform mat4 projMatrix;
uniform mat4 normalMatrix;
uniform bool bindPose;

#define MAX_JOINT_COUNT 100

uniform mat4 skinningMatrices[MAX_JOINT_COUNT];

void main(){
  OutputTangent = tangent;
  OutputNormal = normal;
  OutputTex = tex;

  vec4 mulPosition = vec4(position.xyz, 1.0);
  vec4 mulNormal = vec4(normal.xyz, 1.0);

  if(bindPose)
  {
	   gl_Position = projMatrix * viewMatrix * modelMatrix * mulPosition;
  }
  else
  {
	  vec4 animatedVertex = vec4(0.0, 0.0, 0.0, 1.0);
	  vec4 animatedNormal = vec4(0.0, 0.0, 0.0, 1.0);

	  animatedVertex += skinningMatrices[boneIndices.x] * mulPosition * boneWeights.x;
	  animatedVertex += skinningMatrices[boneIndices.y] * mulPosition * boneWeights.y;
	  animatedVertex += skinningMatrices[boneIndices.z] * mulPosition * boneWeights.z;
	  animatedVertex += skinningMatrices[boneIndices.w] * mulPosition * boneWeights.w;

	  mat4 normalMatrixFix = transpose(inverse(skinningMatrices[boneIndices.x]));
	  mat4 normalMatrixFix2 = transpose(inverse(skinningMatrices[boneIndices.y]));
	  mat4 normalMatrixFix3 = transpose(inverse(skinningMatrices[boneIndices.z]));
	  mat4 normalMatrixFix4 = transpose(inverse(skinningMatrices[boneIndices.w]));

	  animatedNormal += normalMatrixFix  * mulNormal * boneWeights.x;
	  animatedNormal += normalMatrixFix2 * mulNormal * boneWeights.y;
	  animatedNormal += normalMatrixFix3 * mulNormal * boneWeights.z;
	  animatedNormal += normalMatrixFix4 * mulNormal * boneWeights.w;

	  OutputNormal = normalize(animatedNormal.xyz);
	  gl_Position = projMatrix * viewMatrix * modelMatrix * vec4(animatedVertex.xyz,1.0);
  }
}