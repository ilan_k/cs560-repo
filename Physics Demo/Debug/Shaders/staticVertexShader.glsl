#version 330
in layout(location = 0) vec3 position;
in layout(location = 1) vec3 tex;
in layout(location = 2) vec3 normal;
in layout(location = 3) vec3 tangent;

out vec3 OutputTangent;
out vec3 OutputNormal;
out vec3 OutputTex;

uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform mat4 projMatrix;
uniform mat4 normalMatrix;

void main(){
  OutputTangent = tangent;
  OutputNormal = normal;
  OutputTex = tex;
  gl_Position = projMatrix * viewMatrix * modelMatrix * vec4(position,1.0);
}